﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GDLECore;
using GDLECore.Data;

namespace WeenieFlush9000
{
    public class Flusher
    {
        readonly string serverIP = string.Empty;
        readonly string serverPort = string.Empty;
        readonly string serverUser = string.Empty;
        readonly string serverPass = string.Empty;
        readonly string serverdbname = string.Empty;
        Dictionary<uint, string> characters;
        List<uint> goodIds;
        List<uint> mappedIds;
        List<uint> badIds;

        const int timeDiff = 86400 * 10;

        public Flusher(string dbServerIp, string dbServerPort, string dbServerUser, string dbServerPass, string dbname)
        {
            serverIP = dbServerIp;
            serverPort = dbServerPort;
            serverUser = dbServerUser;
            serverPass = dbServerPass;
            serverdbname = dbname;
            characters = new Dictionary<uint, string>();
            badIds = new List<uint>();
            goodIds = new List<uint>();

            if (!IsDatabaseConnectionValid())
                throw new ApplicationException("Unable to access database");
        }

        public bool LoadCharacterList()
        {
            try
            {
                characters = Database.DBUtils.GetCharIdsAndNames();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
            return true;
        }

        public void CheckCharacterSavePattern(bool fix = false)
        {
            uint totalRows = 0;
            foreach (KeyValuePair<uint, string> character in characters)
            {
                // Get Char row
                DataTable CharacterInfo = Database.DBUtils.GetCharacterData(character.Key);
                if (CharacterInfo.Rows.Count == 0)
                    continue;
                Byte[] charData = (byte[])CharacterInfo.Rows[0][1];
                // Get date of char row
                Weenie CharWeenie;
                Console.Write($"Parsing character: {character.Value} with ID: {character.Key}");
                using (BinaryReader br = new BinaryReader(new MemoryStream(charData)))
                {
                    CharWeenie = new Weenie(character.Key).Unpack(br);
                }
                if (CharWeenie == null)
                    continue;
                // build list of real items
                foreach (uint ii in CharWeenie.inventoryIds)
                {
                    goodIds.Add(ii);
                }
                foreach (uint ei in CharWeenie.equipmentIds)
                {
                    goodIds.Add(ei);
                }
                foreach (uint pi in CharWeenie.packIds)
                {
                    goodIds.Add(pi);
                }

                // Get mapped rows
                mappedIds = Database.DBUtils.GetCharInventoryIds(character.Key);
                badIds = mappedIds.Except(goodIds).ToList();

                totalRows += (uint)badIds.Count;
                Console.WriteLine($"- Delete {badIds.Count} rows - Total {totalRows}");

                if (fix)
                    CleanDatabaseOfBadRows();
                //else
                //    Console.WriteLine($"Char: {CharWeenie.Qualities.StringStats[PropertyString.NAME_STRING]}  Expected:{goodIds.Count}  Mapped:{mappedIds.Count} Bad:{badIds.Count}");
                Thread.Sleep(250);
            }

            
        }

        public void CleanDatabaseOfBadRows()
        {
            for (int i = 0; i < badIds.Count; i += 50)
            {
                var itemsToDelete = badIds.Skip(i).Take(50);
                string idsAsList = string.Join(",", itemsToDelete);
                Database.DBUtils.DeleteWeeniesByRange(idsAsList);
                Thread.Sleep(10000);
            }
        }

        private bool IsDatabaseConnectionValid()
        {
            try
            {
                Database.DBUtils.SetupConnection(serverIP, serverPort, serverUser, serverPass, serverdbname);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
            finally
            {
                Database.DBUtils.CloseDB();
            }

            return true;
        }


    }
}
