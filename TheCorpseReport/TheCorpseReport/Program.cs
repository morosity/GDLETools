﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GDLECore;
using GDLECore.Data;

namespace TheCorpseReport
{
    class Program
    {
        public static DateTime FromUnixTime(long unixTime)
        {
            return epoch.AddSeconds(unixTime);
        }
        private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        static void Main(string[] args)
        {



            Database.DBUtils.SetupConnection(ConfigurationManager.AppSettings["server"], ConfigurationManager.AppSettings["port"],
                ConfigurationManager.AppSettings["user"], ConfigurationManager.AppSettings["pass"], ConfigurationManager.AppSettings["dbname"]);

            Dictionary<uint, List<uint>> listOfCurrentCorpses = Database.DBUtils.GetCurrentCorpses();

            if (listOfCurrentCorpses.Count == 0)
            {
                Console.WriteLine("No Corpses found.");
            }
            else
            {
                using (var corpseFile = new StreamWriter(ConfigurationManager.AppSettings["world"] + "_" + DateTime.Now.ToString("yyyyMMddHHmm") + "_TheCorpseReport.txt"))
                {
                    foreach (KeyValuePair<uint, List<uint>> corpseList in listOfCurrentCorpses)
                    {

                        // In Block X, Char was killed on DATE by Player/Mob with Z remaining to rot
                        foreach (uint toon in corpseList.Value)
                        {
                            // Get Weenie
                            Weenie weenie = Database.DBUtils.GetWeenieData(toon);
                            // Get name of killer
                            string killerName = string.Empty;
                            if (weenie != null)
                            {
                                uint killerId = weenie.Qualities.IIDStats[PropertyIID.KILLER_IID];
                                killerName = Database.DBUtils.GetPlayerName(killerId) ?? "mob";
                            }

                            //string deathDate = FromUnixTime(Convert.ToUInt32(weenie.Qualities.FloatStats[PropertyFloat.CREATION_TIMESTAMP_FLOAT])).ToLongDateString();
                            double rotTimer = weenie.Qualities.FloatStats[PropertyFloat.TIME_TO_ROT_FLOAT];
                            // Print out details
                            string printLine = "Block: {0} - {1} killed by {2} with {3} time remaining.";
                            //Console.WriteLine(string.Format(printLine, corpseList.Key.ToString("X"), weenie.Qualities.StringStats[PropertyString.NAME_STRING],
                            //    killerName, deathDate, rotTimer));
                            corpseFile.WriteLine(string.Format(printLine, corpseList.Key.ToString("X"), weenie.Qualities.StringStats[PropertyString.NAME_STRING],
                                killerName, rotTimer));
                            corpseFile.Flush();

                        }
                    }
                }
            }
        }
    }
}
