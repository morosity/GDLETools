﻿using GDLECore;
using GDLECore.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHA
{
    class Program
    {
        static void Main(string[] args)
        {
            Database.DBUtils.SetupConnection(ConfigurationManager.AppSettings["server"], ConfigurationManager.AppSettings["port"],
            ConfigurationManager.AppSettings["user"], ConfigurationManager.AppSettings["pass"], ConfigurationManager.AppSettings["dbname"]);

            List<UInt32> houses = Database.DBUtils.GetHouses();

            string csvTemplate = "{0},{1},{2},{3},{4},{5},{6},{7}";

            using (var houseFile = new StreamWriter("Houses_" + DateTime.Now.ToString("yyyyMMddHHmm") + ".txt"))
            {
                houseFile.WriteLine("houseid,owner,account,access,character,characcout,characctIP,PositionLoc");
                foreach (UInt32 house in houses)
                {
                    HouseData hd = Database.DBUtils.GetHouseData(house);

                    if (hd == null)
                        continue;

                    //string ownerName = Database.DBUtils.GetPlayerName(hd.OwnerId);
                    //if (string.IsNullOrEmpty(ownerName))
                    //    ownerName = "DELETED";

                    //string accountName = Database.DBUtils.GetAccountNameById(hd.OwnerAccountId);
                    //if (string.IsNullOrEmpty(accountName))
                    //    accountName = "UNKNOWN";

                    //List<string> charsWithHouseAccess = new List<string>();
                    //for (int x = 0; x < hd.HouseAccessList.Count; x++)
                    //{
                    //    charsWithHouseAccess.Add(Database.DBUtils.GetPlayerName(hd.HouseAccessList[x]));
                    //}

                    //List<string> charswithChestAccess = new List<string>();
                    //for (int x = 0; x < hd.StorageAccessList.Count; x++)
                    //{
                    //    charswithChestAccess.Add(Database.DBUtils.GetPlayerName(hd.StorageAccessList[x]));
                    //}

                    uint owned = hd.OwnerAccountId > 0 ? hd.OwnerAccountId : 0;

                    uint lastLogin = 0;
                    string loginDate = string.Empty;
                    if (owned > 0)
                    {
                        lastLogin = Database.DBUtils.GetLatestLogin(owned);
                        loginDate = FromUnixTime(lastLogin).ToShortDateString();
                    }

                    houseFile.WriteLine($"House: {house} HouseType:{hd.HouseType} Owner:{owned} Last login:{loginDate} Location: {hd.Location.ToString()}");

                    //foreach (string haChar in charsWithHouseAccess)
                    //{
                    //    if (string.IsNullOrEmpty(haChar))
                    //        continue;
                    //    string haCharAcctName = Database.DBUtils.GetAccountNameNyCharName(haChar.Replace("'", "''"));
                    //    string haCharIP = Database.DBUtils.GetAccountIPByCharName(haChar.Replace("'", "''"));
                    //    houseFile.WriteLine(string.Format(csvTemplate, house, ownerName, accountName, "house", haChar, haCharAcctName, haCharIP, hd.Location.ToString()));
                    //    houseFile.Flush();
                    //}

                    //foreach (string stChar in charswithChestAccess)
                    //{
                    //    if (string.IsNullOrEmpty(stChar))
                    //        continue;
                    //    string stCharAcctName = Database.DBUtils.GetAccountNameNyCharName(stChar.Replace("'", "''"));
                    //    string stCharIP = Database.DBUtils.GetAccountIPByCharName(stChar.Replace("'", "''"));
                    //    houseFile.WriteLine(string.Format(csvTemplate, house, ownerName, accountName, "chest", stChar, stCharAcctName, stCharIP, hd.Location.ToString()));
                    //    houseFile.Flush();
                    //}

                }
            }

        }

        public static DateTime FromUnixTime(long unixTime)
        {
            return epoch.AddSeconds(unixTime);
        }
        private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
    }
}
