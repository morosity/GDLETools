﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAdminTool.WindowObjects
{
    public class StatDetail
    {
        public string EnumName { get; set; }
        public string Usage { get; set; }
        public string Value { get; set; }

    }
}
