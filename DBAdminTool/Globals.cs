﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAdminTool
{
    public static class Globals
    {
        public static string GetCharacterList = "SELECT name FROM characters ORDER BY name;";
        public static string GetCharacterData = "SELECT id, data FROM weenies WHERE top_level_object_id = (SELECT weenie_id FROM characters WHERE name = '{0}') and id = top_level_object_id;";
        public static string GetCharInventory = "SELECT id, data FROM weenies WHERE top_level_object_id = (SELECT weenie_id FROM characters WHERE name = '{0}') and id <> top_level_object_id;";
        public static string GetCharDataLength = "SELECT length(data) FROM weenies WHERE top_level_object_id = (SELECT weenie_id FROM characters WHERE name = '{0}') and id = top_level_object_id;";
        public static string SaveCharacterData = "UPDATE weenies SET data = @data WHERE id = {0}";
        public static string SaveInventoryData = "UPDATE weenies SET data = @data WHERE top_level_object_id = {0} AND id = {1}";
        public static string GetNumberOfCorpses = "SELECT COUNT(*) FROM weenies WHERE id >= 2147483648 and id = top_level_object_id;";
        public static string GetNumberOfCorpseRows = "SELECT COUNT(*) FROM weenies WHERE id >= 2147483648 and id > top_level_object_id and top_level_object_id in " +
            "(SELECT id FROM weenies WHERE id >= 2147483648 and id = top_level_object_id);";
        public static string GetNonCharacterRows = "SELECT COUNT(*) FROM weenies WHERE id >= 2147483648;";
        public static string GetCharacterWeenies = "SELECT * FROM weenies WHERE id < 2147483648;";
        public static string VerifyRemappingTableExists = "SELECT * FROM information_schema.tables WHERE table_schema = 'gdle' AND table_name = 'idremapping' LIMIT 1";
        public static string VerifyTempWeenieTableExists = "SELECT * FROM information_schema.tables WHERE table_schema = 'gdle' AND table_name = 'weeniestemp' LIMIT 1";
        public static string VerifyRemappingTableFields = "SELECT newid, oldid FROM idremapping LIMIT 1;";
        public static string VerifyTempWeenieTableFields = "SELECT id, top_level_object_id, block_id, data FROM weeniestemp LIMIT 1;";
        public static string CreateWeenieTempTable = "CREATE TABLE `weeniestemp` (`id` INT(11) UNSIGNED NOT NULL,`top_level_object_id` INT(11) UNSIGNED NOT NULL,`block_id` INT(11) UNSIGNED NOT NULL," +
            "`data` MEDIUMBLOB NOT NULL) COLLATE='utf8_general_ci' ENGINE=InnoDB;";
        public static string CreateRemappingTable = "CREATE TABLE `idremapping` ( `NewId` INT(11) UNSIGNED NOT NULL, `OldId` INT(11) UNSIGNED NOT NULL ) COLLATE='utf8_general_ci' ENGINE=InnoDB;";
        public static string DropRemappingTable = "DROP TABLE IF EXISTS `idremapping`;";
        public static string DropWeenieTempTable = "DROP TABLE IF EXISTS `weeniestemp`;";
        public static string InsertNewMapping = "INSERT INTO idremapping Values({0}, {1});";
        public static string GetMappingValue = "SELECT NewId FROM idremapping WHERE oldid = {0};";
        public static string SaveWeenieTemp = "insert into weeniestemp select {0}, top_level_object_id, block_id, data from weenies where id = {1};";
        public static string MoveWeenieNew = "INSERT INTO weenies (id, top_level_object_id, block_id, data) SELECT id, top_level_object_id, block_id, data FROM weenietemp;";
        public static string DeleteForDefrag = "DELETE FROM weenies WHERE id >= 2147483648;";
        public static string DeleteCorpseRows = "DELETE FROM weenies WHERE id >= 2147483648 and top_level_object_id > 2147483648;";
        public static string DeleteCorpses = "DELETE FROM weenies WHERE id >= 2147483648 and id = top_level_object_id;";
        public static string GetIdAndNamesOfChars = "SELECT weenie_id, name FROM characters ORDER BY name;";
        public static string GetGearIds = "SELECT id FROM weenies WHERE id >= 2147483648;";
        public static string DeleteCharacterInventory = "DELETE FROM weenies WHERE top_level_object_id = {0};";
        public static string DeleteCharacterWeenei = "DELETE FROM weenies WHERE id = {0};";
        public static string DeleteCharacter = "DELETE FROM chracters WHERE weenie_id = {0};";
        public static string GetAccountForCharacter = "SELECT account_id FROM characters WHERE weenie_id = {0};";
        public static string UpdateAccountPassword = "UPDATE accounts SET password = 'a953d45cfd9680887dc6c6f1a00e5372bc93a32a358a1f6106f3db134b9a988b', password_salt = 'dc5079d09a1f4681' WHERE id = {0}; ";

        public static int ByteAlign(int len)
        {
            int extra = len % 4;
            if (extra == 1)
                return 3;
            if (extra == 2)
                return 2;
            if (extra == 3)
                return 1;
            return extra;
        }
    }
}
