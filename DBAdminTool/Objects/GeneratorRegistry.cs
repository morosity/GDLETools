﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAdminTool.Objects
{
    public class GeneratorRegistry : IPackable<GeneratorRegistry>
    {
        public List<GeneratorRegistryNode> Nodes { get; private set; }

        public GeneratorRegistry()
        {
            Nodes = new List<GeneratorRegistryNode>();
        }

        public GeneratorRegistry Unpack(BinaryReader reader)
        {
            ushort numGenRegistry = reader.ReadUInt16();
            ushort sizeGenRegistry = reader.ReadUInt16();

            for (int gr = 0; gr < numGenRegistry; gr++)
            {
                Nodes.Add(new GeneratorRegistryNode().Unpack(reader));
            }
            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write((ushort)Nodes.Count);
            writer.Write((ushort)16);

            foreach (GeneratorRegistryNode gnr in Nodes)
                gnr.Pack(writer);
        }
    }
}
