﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAdminTool.Objects
{
    public class Attribute : IPackable<Attribute>
    {
        public uint LevelFromCP { get; set; }
        public uint InitialLevel { get; set; }
        public uint CPSpent { get; set; }

        public Attribute()
        { }

        public Attribute Unpack(BinaryReader reader)
        {
            LevelFromCP = reader.ReadUInt32();
            InitialLevel = reader.ReadUInt32();
            CPSpent = reader.ReadUInt32();

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write(LevelFromCP);
            writer.Write(InitialLevel);
            writer.Write(CPSpent);
        }
    }
}
