﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAdminTool
{
    interface IPackable<T>
    {
        T Unpack(BinaryReader reader);

        void Pack(BinaryWriter writer);
    }
}
