﻿using DBAdminTool.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DBAdminTool
{
    /// <summary>
    /// Interaction logic for IDRemapper.xaml
    /// </summary>
    public partial class IDRemapper : Window
    {
        int nonCorpseRowCount = 0;
        int corpseRowCount = 0;
        int numNonCharRows = 0;
        int numChars = 0;

        public IDRemapper()
        {
            InitializeComponent();
            ShowDbConnection();
            //ShowNumberOfCharacters();
            //ShowNumberOfCorpses();
            //ShowNumberOfCorpseRows();
            //ShowNumberOfNonCharWeenies();
         }

        private void VerifyRemappingTables()
        {
            if (overwritemappings.IsChecked == true)
            {
                CreateMappingTables(true);
                return;
            }

            if (!DBUtils.VerifyMappingTables(Application.Current.Properties["dbconnectionstring"].ToString()))
            {
                if (createmappings.IsChecked == true)
                {
                    CreateMappingTables(false);
                    return;
                }
                else
                    MessageBox.Show("Mapping tables not found\nUse 'Create Mapping Tables' option");
            }

            if (!DBUtils.VerifyMappingFields(Application.Current.Properties["dbconnectionstring"].ToString()))
            {
                MessageBox.Show("Mapping tables missing fields\nUse 'Overwrite Exising' option");
            }
        }

        private void CreateMappingTables(bool recreate)
        {
            if (recreate)
            {
                DBUtils.DropMappingTables(Application.Current.Properties["dbconnectionstring"].ToString());
            }
            DBUtils.CreateMappingTables(Application.Current.Properties["dbconnectionstring"].ToString());
        }

        private void ShowDbConnection()
        {
            string[] connSplit = Application.Current.Properties["dbconnectionstring"].ToString().Split(';');
            dbinstance.Content = connSplit[0].ToLower().Replace("server=", "") + ":" + connSplit[1].ToLower().Replace("port=", "");
        }

        private void dechexconv_Click(object sender, RoutedEventArgs e)
        {
            if (dechexconv.Content.ToString() == "ToDecimal")
            {
                try
                {
                    uint asdec = UInt32.Parse(startidnum.Text.Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    startidnum.Text = asdec.ToString();
                    dechexconv.Content = "ToHex";
                }
                catch
                {
                    MessageBox.Show("Start number is not in Hex format: 0x0");
                }
            }
            else
            {
                try
                {
                    uint tohex = Convert.ToUInt32(startidnum.Text);
                    startidnum.Text = "0x" + tohex.ToString("X");
                    dechexconv.Content = "ToDecimal";
                }
                catch
                {
                    MessageBox.Show("Start number is not a valid unsigned int.");
                }
            }
        }

        private void close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void changedb_Click(object sender, RoutedEventArgs e)
        {
            DBConnections connDialog = new DBConnections
            {
                Owner = Application.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            connDialog.Show();
            ShowDbConnection();
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            VerifyRemappingTables();
            outputbox.Text = "Starting ID Remapping...\n";
            if (deletecorposes.IsChecked == true)
            {
                outputbox.Text += "Corpses will be deleted\n";
                //DBUtils.DeleteCorpsesAndData(Application.Current.Properties["dbconnectionstring"].ToString());
                numcharsrows.Content = "0";
                numcorpses.Content = "0";
            }
            else
                outputbox.Text += "Corpses will be remapped\n";
            outputbox.Text += "Starting with ID: " + startidnum.Text + "\n";

            Dictionary<uint, string> characterData = DBUtils.GetCharIdsAndNames(Application.Current.Properties["dbconnectionstring"].ToString());

            uint remapStartId = 0;
            
            if (dechexconv.Content.ToString() == "ToDecimal")
            {
                try
                {
                    remapStartId = UInt32.Parse(startidnum.Text.Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                }
                catch
                {
                    MessageBox.Show("Start number is not in Hex format: 0x0");
                    return;
                }
            }
            else
            {
                try
                {
                    remapStartId = Convert.ToUInt32(startidnum.Text);
                }
                catch
                {
                    MessageBox.Show("Start number is not a valid unsigned int.");
                    return;
                }
            }

            DBUtils.ConnectAndOpen(Application.Current.Properties["dbconnectionstring"].ToString());

            foreach (KeyValuePair<uint, string> cd in characterData)
            {
                outputbox.Text += "Processing " + cd.Value;
                uint charStartId = remapStartId;
                DataTable dataTable = DBUtils.GetCharacterData(cd.Value, Application.Current.Properties["dbconnectionstring"].ToString());
                uint weenieID = Convert.ToUInt32(dataTable.Rows[0][0]);
                Byte[] charData = (byte[])dataTable.Rows[0][1];
                Weenie character = new Weenie(0);
                bool testCharValid = true;
                try
                {
                    character = ParseWeenieData(weenieID, charData);
                }
                catch
                {
                    if (MessageBox.Show("Test Char?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                    {
                        testCharValid = false;
                        DBUtils.UpdateAccountPassAndSalt(DBUtils.GetAccountIdForCharacter(weenieID, Application.Current.Properties["dbconnectionstring"].ToString()),
                             Application.Current.Properties["dbconnectionstring"].ToString());
                    }

                }

                if (testCharValid == false)
                {
                    if (MessageBox.Show("Delete Char?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                    {
                        DBUtils.DeleteCharacterInventory(weenieID, Application.Current.Properties["dbconnectionstring"].ToString());
                        DBUtils.DeleteCharacterWeenie(weenieID, Application.Current.Properties["dbconnectionstring"].ToString());
                        DBUtils.DeleteCharacter(weenieID, Application.Current.Properties["dbconnectionstring"].ToString());
                    }
                }
                else
                {
                    List<uint> newInv = new List<uint>();
                    foreach (var w in character.inventoryIds)
                    {
                        uint newId = remapStartId++;
                        DBUtils.AddNewIdMapping(w, newId, Application.Current.Properties["dbconnectionstring"].ToString());
                        newInv.Add(newId);
                    }
                    character.inventoryIds.Clear();
                    character.inventoryIds.AddRange(newInv);

                    List<uint> newEquip = new List<uint>();
                    foreach (var w in character.equipmentIds)
                    {
                        uint newId = remapStartId++;
                        DBUtils.AddNewIdMapping(w, newId, Application.Current.Properties["dbconnectionstring"].ToString());
                        newEquip.Add(newId);
                    }
                    character.equipmentIds.Clear();
                    character.equipmentIds.AddRange(newEquip);

                    List<uint> newPack = new List<uint>();
                    foreach (var w in character.packIds)
                    {
                        uint newId = remapStartId++;
                        DBUtils.AddNewIdMapping(w, newId, Application.Current.Properties["dbconnectionstring"].ToString());
                        newPack.Add(newId);
                    }
                    character.packIds.Clear();
                    character.packIds.AddRange(newPack);

                    MemoryStream writeMemStream = new MemoryStream();
                    BinaryWriter writeBinStream = new BinaryWriter(writeMemStream);

                    character.Pack(writeBinStream);

                    byte[] toSave = writeMemStream.ToArray();

                    bool saved = DBUtils.SaveCharacterData(character.WeenieID, toSave, Application.Current.Properties["dbconnectionstring"].ToString());
                    uint charEndId = remapStartId;
                }
            }

            DBUtils.CloseDB();

            outputbox.Text += "Creating Temp Weenies";

            List<uint> remapgear = DBUtils.GetRangeEightRowId(Application.Current.Properties["dbconnectionstring"].ToString());

            foreach (uint dr in remapgear)
            {
                uint newid = DBUtils.GetNewId(dr, Application.Current.Properties["dbconnectionstring"].ToString());
                DBUtils.SaveRemapData(newid, dr, Application.Current.Properties["dbconnectionstring"].ToString());
            }

            outputbox.Text += "Deleting old Range 8 weenies";
            DBUtils.DeleteOldWeenies(Application.Current.Properties["dbconnectionstring"].ToString());

            outputbox.Text += "Copying remapped weenies to Weenies table";
            DBUtils.InsertRemappedWeenies(Application.Current.Properties["dbconnectionstring"].ToString());


            outputbox.Text += "Done";
        }

        private Weenie ParseWeenieData(uint weenieID, Byte[] charData)
        {
            Weenie character;
            using (BinaryReader br = new BinaryReader(new MemoryStream(charData)))
            {
                character = new Weenie(weenieID).Unpack(br);
            }
            return character;
        }


    }
}
