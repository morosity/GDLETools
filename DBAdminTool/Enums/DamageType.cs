﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAdminTool.Enums
{
    public enum DAMAGE_TYPE
    {
        UNDEF_DAMAGE_TYPE = 0,
        SLASH_DAMAGE_TYPE = 1,
        PIERCE_DAMAGE_TYPE = 2,
        BLUDGEON_DAMAGE_TYPE = 4,
        COLD_DAMAGE_TYPE = 8,
        FIRE_DAMAGE_TYPE = 0x10,
        ACID_DAMAGE_TYPE = 0x20,
        ELECTRIC_DAMAGE_TYPE = 0x40,
        HEALTH_DAMAGE_TYPE = 0x80,
        STAMINA_DAMAGE_TYPE = 0x100,
        MANA_DAMAGE_TYPE = 0x200,
        NETHER_DAMAGE_TYPE = 0x400,
        BASE_DAMAGE_TYPE = 0x10000000
    };
}
