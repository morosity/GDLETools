﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WorldBuilder
{
    /// <summary>
    /// Interaction logic for RecipeManager.xaml
    /// </summary>
    public partial class RecipeManager : Page
    {
        public RecipeManager()
        {
            InitializeComponent();
        }

        private void Expander_Expanded(object sender, RoutedEventArgs e)
        {

        }

        private void RecipeExpander_Expanded(object sender, RoutedEventArgs e)
        {

        }

        private void RequirementsExpander_Expanded(object sender, RoutedEventArgs e)
        {

        }

        private void RequirementsExpander_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }

        private void RecipeToolButton_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
