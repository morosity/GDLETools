﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WorldBuilder
{
    /// <summary>
    /// Interaction logic for TreasureManager.xaml
    /// </summary>
    public partial class TreasureManager : Page
    {
        TreasureSystem treasureSystem;

        public TreasureManager()
        {
            InitializeComponent();
            treasureSystem = new TreasureSystem();
        }

        private void NewTreasureButton_Opened(object sender, RoutedEventArgs e)
        {

        }

        private void NewTreasureButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void NewTreasureButton_Closed(object sender, RoutedEventArgs e)
        {

        }

        private void NewDeathTreasureButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void NewWieldedTreasureButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("New Wielded Treasure");
        }

        private void RollDeathTreasureButton_Click(object sender, RoutedEventArgs e)
        {
            DeathTreasureChoice deathTreasureChoice = new DeathTreasureChoice();
            deathTreasureChoice.ShowDialog();
            treasureSystem.DoDeathTreasure(DeathTreasureChoice.DeathTreasureValue);
        }

        private void RollWieldedTreasureButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Roll Wielded Treasure");
        }

        private void RollRandomTierTreasureButton_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
