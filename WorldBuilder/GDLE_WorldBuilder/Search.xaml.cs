﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace WorldBuilder
{
    /// <summary>
    /// Interaction logic for Search.xaml
    /// </summary>
    public partial class Search : Window
    {
        public long SelectedRecipe = 0;

        public Search(string criteria)
        {
            InitializeComponent();
            SearchWindowTitle.Title = "Find " + criteria;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            RecipeSearch();

        }

        private void RecipeSearch()
        {
            //if (WeenieLoader.WeenieData.Weenies.Count() == 0)
            //{
            //    MessageBox.Show("Weenie data not loaded.\nPlease verify in Json Settings.");
            //    return;
            //}


            int tryWcid = 0;
            if (!string.IsNullOrEmpty(SearchCriteriaWCID.Text))
            {
                if (!Int32.TryParse(SearchCriteriaWCID.Text, out tryWcid))
                {
                    MessageBox.Show("Weenie Class ID must be a valid positive integer");
                    SearchCriteriaWCID.Text = "";
                    return;
                }

                //var weenieid = (from d in WeenieLoader.WeenieData.Weenies
                //                where d.Key == tryWcid
                //                select d.Value).FirstOrDefault();

                //if (weenieid == null)
                //{
                //    MessageBox.Show("Unable to locate selected weenie by wcid");
                //    return;
                //}

                //SearchCriteriaName.Text = (from d in weenieid.StringStats
                //                           where d.Key == 1
                //                           select d.Value).FirstOrDefault();

                SearchCriteriaWCID.Text = tryWcid.ToString();

            }
            else if (!string.IsNullOrEmpty(SearchCriteriaName.Text))
            {
                List<SearchResult> recipes = new List<SearchResult>();
                //foreach (KeyValuePair<long, Weenie> w in WeenieLoader.WeenieData.Weenies)
                //{
                //    foreach (StringStat ss in w.Value.StringStats)
                //    {
                //        if (ss.Key == 1 && ss.Value.ToLower().Trim().Contains(SearchCriteriaName.Text.ToLower().Trim()))
                //        {
                //            recipes.Add(new SearchResult() { WCID = w.Value.Wcid, WeenieName =  ss.Value });
                //        }
                //    }
                //}
                if (recipes.Count > 0)
                {
                    SearchResults.ItemsSource = recipes;
                    SearchResults.Visibility = Visibility.Visible;
                    SearchResults.Height = 100;
                }
                else
                    MessageBox.Show("No Results found");
            }

        }

        private void ByName_Checked(object sender, RoutedEventArgs e)
        {
        }

        private void SearchResults_Selected(object sender, RoutedEventArgs e)
        {
            DataGrid dg = sender as DataGrid;
            DataGridRow row = ItemsControl.ContainerFromElement((DataGrid)sender, e.OriginalSource as DependencyObject) as DataGridRow;
            if (row == null) return;

            SelectedRecipe = ((SearchResult)row.Item).WCID;
            Close();
        }

        private void UseButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void CloseSettings_Click(object sender, RoutedEventArgs e)
        { }
    }

    public class SearchResult
    {
        public long WCID { get; set; }
        public string WeenieName { get; set; }
    }

}
