﻿using System;
using System.Windows;
using System.Windows.Input;

namespace WorldBuilder
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Logger.Initialize();
        }

        private void MenuSettings_Click(object sender, RoutedEventArgs e)
        {
            Settings settings = new Settings();
            settings.Show();
        }

        private void RecipeToolbar_Click(object sender, RoutedEventArgs e)
        {
            SetContentMenuHighlight("recipe");
            ContentFrame.Navigate(new System.Uri("RecipeManager.xaml", System.UriKind.RelativeOrAbsolute));
        }


        private void searchbutton_Click(object sender, RoutedEventArgs e)
        {
            Search search = new Search("");
            search.Show();
            //LoadRecipe(search.SelectedRecipe);
        }

        private void RecipeToolButton_Click(object sender, RoutedEventArgs e)
        {
            Search search = new Search("Tool");
            search.Show();
        }

        private void JsonLocationSettings_Click(object sender, RoutedEventArgs e)
        {
            JsonSettings jsonSettings = new JsonSettings();
            jsonSettings.ShowDialog();
            WeenieManager.Initialize(JsonSettings.WeenieJsonPath);
            if (WeenieManager.IsInitialized)
            {
                IsWeenieLoaded.Text = "Weenies Loaded: True";
            }
        }

        private void TreasureToolbar_Click(object sender, RoutedEventArgs e)
        {
            SetContentMenuHighlight("treasure");
            ContentFrame.Navigate(new System.Uri("TreasureManager.xaml", System.UriKind.RelativeOrAbsolute));
        }

        private void WeenieToolbar_Click(object sender, RoutedEventArgs e)
        {
            SetContentMenuHighlight("weenie");
            ContentFrame.Navigate(new System.Uri("WeenieManager.xaml", UriKind.RelativeOrAbsolute));
        }

        private void LandblockToolbar_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SpellToolbar_Click(object sender, RoutedEventArgs e)
        {

        }


        private void SetContentMenuHighlight(string contentPage)
        {
            switch (contentPage)
            {
                case "recipe":
                    RecipeToolbar.FontSize = 14;
                    RecipeToolbar.FontWeight = FontWeights.Bold;
                    WeenieToolbar.FontSize = 10;
                    WeenieToolbar.FontWeight = FontWeights.Normal;
                    SpellToolbar.FontSize = 10;
                    SpellToolbar.FontWeight = FontWeights.Normal;
                    LandblockToolbar.FontSize = 10;
                    LandblockToolbar.FontWeight = FontWeights.Normal;
                    TreasureToolbar.FontSize = 10;
                    TreasureToolbar.FontWeight = FontWeights.Normal;
                    break;
                case "treasure":
                    WeenieToolbar.FontSize = 10;
                    WeenieToolbar.FontWeight = FontWeights.Normal;
                    SpellToolbar.FontSize = 10;
                    SpellToolbar.FontWeight = FontWeights.Normal;
                    LandblockToolbar.FontSize = 10;
                    LandblockToolbar.FontWeight = FontWeights.Normal;
                    RecipeToolbar.FontSize = 10;
                    RecipeToolbar.FontWeight = FontWeights.Normal;
                    TreasureToolbar.FontSize = 14;
                    TreasureToolbar.FontWeight = FontWeights.Bold;
                    break;
                case "spell":
                    WeenieToolbar.FontSize = 10;
                    WeenieToolbar.FontWeight = FontWeights.Normal;
                    RecipeToolbar.FontSize = 10;
                    RecipeToolbar.FontWeight = FontWeights.Normal;
                    LandblockToolbar.FontSize = 10;
                    LandblockToolbar.FontWeight = FontWeights.Normal;
                    TreasureToolbar.FontSize = 10;
                    TreasureToolbar.FontWeight = FontWeights.Normal;
                    SpellToolbar.FontSize = 14;
                    SpellToolbar.FontWeight = FontWeights.Bold;
                    break;
                case "landblock":
                    WeenieToolbar.FontSize = 10;
                    WeenieToolbar.FontWeight = FontWeights.Normal;
                    SpellToolbar.FontSize = 10;
                    SpellToolbar.FontWeight = FontWeights.Normal;
                    RecipeToolbar.FontSize = 10;
                    RecipeToolbar.FontWeight = FontWeights.Normal;
                    TreasureToolbar.FontSize = 10;
                    TreasureToolbar.FontWeight = FontWeights.Normal;
                    LandblockToolbar.FontSize = 14;
                    LandblockToolbar.FontWeight = FontWeights.Bold;
                    break;
                default:
                    RecipeToolbar.FontSize = 10;
                    RecipeToolbar.FontWeight = FontWeights.Normal;
                    SpellToolbar.FontSize = 10;
                    SpellToolbar.FontWeight = FontWeights.Normal;
                    LandblockToolbar.FontSize = 10;
                    LandblockToolbar.FontWeight = FontWeights.Normal;
                    TreasureToolbar.FontSize = 10;
                    TreasureToolbar.FontWeight = FontWeights.Normal;
                    WeenieToolbar.FontSize = 14;
                    WeenieToolbar.FontWeight = FontWeights.Bold;
                    break;
            }
        }

        private void DatLocationSettings_Click(object sender, RoutedEventArgs e)
        {

        }

        private void DatabaseConnectionSettings_Click(object sender, RoutedEventArgs e)
        {
            DBConnection dBConnection = new DBConnection();
            dBConnection.ShowDialog();
            if (Application.Current.Properties["dbconnectionvalid"] as string == "true")
            {
                IsDBSet.Text = "Database Connected: True";
            }
        }

        private void menuItemExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
