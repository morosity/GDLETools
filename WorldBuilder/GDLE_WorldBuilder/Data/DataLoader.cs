﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WorldBuilder
{
    public class DataLoader
    {
        private static readonly List<string> mutationFilterFiles = new List<string>() { "02", "03", "04", "05", "06", "07", "08", "09", "10", "14", "34", "48", "49", "50", "51", "68", "71", "72", "73", "74" };

        internal static Dictionary<int, List<MutationFilter>> GetJsonData()
        {
            Dictionary<int, List<MutationFilter>> mutations = new Dictionary<int, List<MutationFilter>>();
            for (int x = 0; x < mutationFilterFiles.Count; x++)
            {
                string fileName = AppDomain.CurrentDomain.BaseDirectory + $"json\\{mutationFilterFiles[x]}.json";
                using (StreamReader reader = new StreamReader(fileName))
                {
                    string readText = reader.ReadToEnd();
                    int mutationIdx = 939524096 + Convert.ToInt32(mutationFilterFiles[x]);
                    mutations.Add(mutationIdx, MutationFilter.FromJson(readText));
                }
            }
            return mutations;
        }

        internal static DataTable GetDataTable(string tableName)
        {
            DataTable data = null;

            using (MySqlConnection conn = new MySqlConnection(Application.Current.Properties["dbconnectionstring"] as string))
            {
                conn.Open();
                data = new DataTable();
                using (MySqlCommand command = new MySqlCommand($"SELECT * FROM {tableName};", conn))
                {
                    data.Load(command.ExecuteReader());
                }
            }
            return data;
        }
    }
}
