﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace WorldBuilder
{
    public static class WeenieManager
    {
        static public bool IsInitialized = false;
        static ConcurrentDictionary<int, Weenie> weenieData;
        

        static public bool Initialize(string pathToWeenies)
        {
            try
            {
                weenieData = new ConcurrentDictionary<int, Weenie>();
                var jsonFiles = GetWeenieJsonFileList(pathToWeenies);
                Parallel.ForEach(jsonFiles, (file) =>
                {
                    // parse numerical file name
                    string[] name = file.ToString().Split(' ');
                    string[] wid = name[0].Split('\\');
                    int id = Convert.ToInt32(wid[wid.Length - 1]);
                    // read file into Weenie
                    string jsonString = File.ReadAllText(file);
                    var weenie = Weenie.FromJson(jsonString);
                    // add to weenieData
                    weenieData.TryAdd(id, weenie);
                });

                IsInitialized = true;
            }
            catch
            {
                return false;
            }

            return true;
        }


        static IEnumerable<string> GetWeenieJsonFileList(string path)
        {
            Queue<string> queue = new Queue<string>();
            queue.Enqueue(path);
            while (queue.Count > 0)
            {
                path = queue.Dequeue();
                try
                {
                    foreach (string subDir in Directory.GetDirectories(path))
                    {
                        queue.Enqueue(subDir);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(Severity.Error, ex.ToString());
                }
                string[] files = null;
                try
                {
                    files = Directory.GetFiles(path);
                }
                catch (Exception ex)
                {
                    Logger.Log(Severity.Error, ex.ToString());
                }
                if (files != null)
                {
                    for (int i = 0; i < files.Length; i++)
                    {
                        yield return files[i];
                    }
                }
            }
        }

        internal static Weenie GetWeenie(int wcid)
        {
            if (weenieData == null)
                return null;
            return Utils.DeepClone(weenieData[wcid]); 
        }

        internal static ACQualities GetWeeineQualities(int wcid)
        {
            ACQualities ac = new ACQualities();

            ac.BoolQualities = ConvertToBoolDictionary(weenieData[wcid].BoolStats);
            ac.IntQualities = ConvertToIntDictionary(weenieData[wcid].IntStats);
            ac.FloatQualities = ConvertToFloatDictionary(weenieData[wcid].FloatStats);
            ac.DidQualities = ConvertToDIDDictionary(weenieData[wcid].DidStats);
            ac.StringQualities = ConvertToStringDictionary(weenieData[wcid].StringStats);
            return ac;
        }

        internal static ACQualities GetWeeineQualities(Weenie weenie)
        {
            ACQualities ac = new ACQualities();

            ac.BoolQualities = ConvertToBoolDictionary(weenie.BoolStats);
            ac.IntQualities = ConvertToIntDictionary(weenie.IntStats);
            ac.FloatQualities = ConvertToFloatDictionary(weenie.FloatStats);
            ac.DidQualities = ConvertToDIDDictionary(weenie.DidStats);
            ac.StringQualities = ConvertToStringDictionary(weenie.StringStats);
            return ac;
        }


        private static Dictionary<StringQualities, string> ConvertToStringDictionary(List<StringStat> stringStats)
        {
            Dictionary<StringQualities, string> retVal = new Dictionary<StringQualities, string>();
            for (int x = 0; x < stringStats.Count; x++)
            {
                retVal.Add((StringQualities)stringStats[x].Key, stringStats[x].Value);
            }
            return retVal;
        }

        private static Dictionary<DIDQualities, int> ConvertToDIDDictionary(List<Stat> didStats)
        {
            Dictionary<DIDQualities, int> retVal = new Dictionary<DIDQualities, int>();
            for (int x = 0; x < didStats.Count; x++)
            {
                retVal.Add((DIDQualities)didStats[x].Key, Convert.ToInt32(didStats[x].Value));
            }
            return retVal;
        }

        private static Dictionary<FLOATQualities, double> ConvertToFloatDictionary(List<Stat> floatStats)
        {
            Dictionary<FLOATQualities, double> retVal = new Dictionary<FLOATQualities, double>();
            for (int x = 0; x < floatStats.Count; x++)
            {
                retVal.Add((FLOATQualities)floatStats[x].Key, Convert.ToDouble(floatStats[x].Value));
            }
            return retVal;
        }

        private static Dictionary<INTQualities, int> ConvertToIntDictionary(List<Stat> intStats)
        {
            Dictionary<INTQualities, int> retVal = new Dictionary<INTQualities, int>();
            for (int x = 0; x < intStats.Count; x++)
            {
                retVal.Add((INTQualities)intStats[x].Key, Convert.ToInt32(intStats[x].Value));
            }
            return retVal;
        }

        private static Dictionary<BoolQualities, bool> ConvertToBoolDictionary(List<Stat> boolStats)
        {
            Dictionary<BoolQualities, bool> retVal = new Dictionary<BoolQualities, bool>();
            for (int x = 0; x < boolStats.Count; x++)
            {
                retVal.Add((BoolQualities)boolStats[x].Key, Convert.ToBoolean(boolStats[x].Value));
            }
            return retVal;
        }
    }
}
