﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WorldBuilder
{
    /// <summary>
    /// Interaction logic for DBConnection.xaml
    /// </summary>
    public partial class DBConnection : Window
    {
        public DBConnection()
        {
            InitializeComponent();
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            connectionServerAddr.Focus();
        }

        private void ConnectionTest_Click(object sender, RoutedEventArgs e)
        {
            if (VerifyInputs())
            {
                string connectionString = CreateConnectionString();
                using (MySqlConnection conn = new MySqlConnection(connectionString))
                {
                    try
                    {
                        conn.Open();
                        Application.Current.Properties["dbconnectionvalid"] = "true";
                        Application.Current.Properties["dbconnectionstring"] = connectionString;
                        connectionTest.Background = new SolidColorBrush(Colors.Green);
                    }
                    catch
                    {
                        MessageBoxResult result = MessageBox.Show("Failed to connect to database.\nWould you like the database created?", "Connection Error", MessageBoxButton.YesNoCancel);
                        switch (result)
                        {
                            case MessageBoxResult.Yes:
                                CloseWindow();
                                CreateDatabase();
                                break;
                            case MessageBoxResult.No:
                                MessageBox.Show("Application exitting.", "Connection Error");
                                break;
                            case MessageBoxResult.Cancel:
                                break;
                        }
                    }
                }
            }
        }

        private void CreateDatabase()
        {
            MessageBox.Show("Making Waffles", "Mmmm", MessageBoxButton.OK, MessageBoxImage.Information);
            // Show "Loading" modal
            // Call DataLoader to build and populate DB
            // Close "Loading" modal
            MessageBox.Show("Database created and data loaded.", "", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private bool VerifyInputs()
        {
            if (string.IsNullOrEmpty(connectionServerAddr.Text))
            {
                ShowError("Server Address cannot be blank");
                return false;
            }

            if (string.IsNullOrEmpty(connectionServerPort.Text))
            {
                ShowError("Server Port cannot be blank");
                return false;
            }

            if (!Int32.TryParse(connectionServerPort.Text.Trim(), out int portcheck) && (portcheck < 1 || portcheck > 64000))
            {
                ShowError("Server Port must be between 1 & 64000");
            }

            if (string.IsNullOrEmpty(connectionServerUsername.Text))
            {
                ShowError("Username cannot be blank");
                return false;
            }

            if (string.IsNullOrEmpty(connectionServerPassword.Text))
            {
                ShowError("Password cannot be blank");
                return false;
            }

            if (string.IsNullOrEmpty(connectionServerDatabase.Text))
            {
                ShowError("Database name cannot be blank");
                return false;
            }

            return true;
        }

        private void ShowError(string errorMsg)
        {
            MessageBox.Show(errorMsg, "Connection Test Error", MessageBoxButton.OK);
        }

        private string CreateConnectionString()
        {
            return new MySqlConnectionStringBuilder()
            {
                Server = connectionServerAddr.Text.Trim(),
                Port = Convert.ToUInt32(connectionServerPort.Text.Trim()),
                UserID = connectionServerUsername.Text.Trim(),
                Password = connectionServerPassword.Text.Trim(),
                Database = connectionServerDatabase.Text.Trim(),
                IgnorePrepare = false,
                Pooling = true,
                AllowUserVariables = true,
                AllowZeroDateTime = true
            }.ToString();
        }

        private void ConnectionClose_Click(object sender, RoutedEventArgs e)
        {
            CloseWindow();
        }

        private void CloseWindow()
        {
            this.Close();
            Application.Current.MainWindow.Focus();
        }
    }
}
