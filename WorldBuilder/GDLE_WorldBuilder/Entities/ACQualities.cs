﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldBuilder
{
    public class ACQualities
    {
        public Dictionary<INTQualities, int> IntQualities = new Dictionary<INTQualities, int>();
        public Dictionary<INT64Qualities, long> Int64Qualities = new Dictionary<INT64Qualities, long>();
        public Dictionary<BoolQualities, bool> BoolQualities = new Dictionary<BoolQualities, bool>();
        public Dictionary<DIDQualities, int> DidQualities = new Dictionary<DIDQualities, int>();
        public Dictionary<IIDQualities, int> IidQualities = new Dictionary<IIDQualities, int>();
        public Dictionary<FLOATQualities, double> FloatQualities = new Dictionary<FLOATQualities, double>();
        public Dictionary<PositionQualities, Position> PositionQualities = new Dictionary<PositionQualities, Position>();
        public Dictionary<StringQualities, string> StringQualities = new Dictionary<StringQualities, string>();
        public List<int> Spells = new List<int>();

        #region IntMethods
        internal bool QueryInt(INTQualities intQual)
        {
            if (IntQualities.ContainsKey(intQual))
                return true;
            return false;
        }
        
        internal void SetInt(INTQualities intQual, int value)
        {
            if (QueryInt(intQual))
            {
                IntQualities[intQual] = value;
            }
            else
            {
                IntQualities.Add(intQual, value);
            }
        }

        public bool InqInt(INTQualities intQual, out int value)
        {
            value = 0;
            if (!QueryInt(intQual))
                return false;

            value = GetInt(intQual);
            return true;
        }

        internal int GetInt(INTQualities intQual)
        {
            return IntQualities[intQual];
        }
        #endregion

        #region FloatMethods
        internal bool QueryFloat(FLOATQualities floatQual)
        {
            if (FloatQualities.ContainsKey(floatQual))
                return true;
            return false;
        }

        internal bool InqFloat(FLOATQualities floatQual, out double value)
        {
            value = 0;
            if (!QueryFloat(floatQual))
                return false;

            value = GetFloat(floatQual);
            return true;
        }

        internal double GetFloat(FLOATQualities floatQual)
        {
            return FloatQualities[floatQual];
        }

        internal void SetFloat(FLOATQualities floatQual, double value)
        {
            FloatQualities[floatQual] = value;
        }

        #endregion

        #region DataIDMethods
        internal bool SetDataID(DIDQualities didQual, int value)
        {
            if (QueryDataID(didQual))
            {
                DidQualities[didQual] = value;
            }
            else
            {
                DidQualities.Add(didQual, value);
            }
            return true;
        }

        internal bool QueryDataID(DIDQualities didQual)
        {
            if (DidQualities.ContainsKey(didQual))
                return true;
            return false;
        }

        internal bool InqDataID(DIDQualities didQual, out int value)
        {
            value = 0;
            if (!QueryDataID(didQual))
                return false;

            value = GetDataID(didQual);
            return true;
        }

        internal int GetDataID(DIDQualities didQual)
        {
            return DidQualities[didQual];
        }

        #endregion

        #region SpellMethods

        internal bool InqSpell(int spellId)
        {
            if (Spells.Contains(spellId))
                return true;
            return false;
        }

        internal bool AddSpell(int spellId)
        {
            if (!InqSpell(spellId))
            {
                Spells.Add(spellId);
                return true;
            }
            return false;
        }

        internal List<int> GetSpellBook()
        {
            return Spells;
        }

        internal bool HasSpellBook()
        {
            return Spells.Count > 0;
        }
        #endregion

        #region StringMethods
        internal bool QueryString(StringQualities stringQual)
        {
            if (StringQualities.ContainsKey(stringQual))
                return true;
            return false;
        }

        internal bool InqString(StringQualities stringQual, out string basename)
        {
            basename = string.Empty;
            if (QueryString(stringQual))
            {
                basename = StringQualities[stringQual];
                return true;
            }
            return false;
        }

        internal void SetString(StringQualities stringQual, string value)
        {
            if (QueryString(stringQual))
            {
                StringQualities[stringQual] = value;
            }
            else
            {
                StringQualities.Add(stringQual, value);
            }
        }

        #endregion

        #region BoolMethods

        internal bool QueryBool(BoolQualities boolQual)
        {
            if (BoolQualities.ContainsKey(boolQual))
                return true;
            return false;
        }

        internal bool GetBool(BoolQualities boolQual)
        {
            return BoolQualities[boolQual];
        }

        internal void SetBool(BoolQualities boolQual, bool value)
        {
            if (QueryBool(boolQual))
            {
                BoolQualities[boolQual] = value;
            }
            else
            {
                BoolQualities.Add(boolQual, value);
            }
        }

        #endregion
    }
}
