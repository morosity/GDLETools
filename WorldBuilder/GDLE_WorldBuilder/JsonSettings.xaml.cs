﻿using System.IO;
using System.Windows;
using System.Windows.Forms;

namespace WorldBuilder
{
    /// <summary>
    /// Interaction logic for JsonSettings.xaml
    /// </summary>
    public partial class JsonSettings : Window
    {
        public static string WeenieJsonPath = string.Empty;
        public static string RecipeJsonPath = string.Empty;
        public static string PrecursorJsonPath = string.Empty;
        public static string LandblockJsonPath = string.Empty;
        public static string TreasureJsonPath = string.Empty;
        public static string SpellJsonPath = string.Empty;
        private static bool IsPathValid = false;

        public JsonSettings()
        {
            InitializeComponent();
            WeenieLocationPath.Content = Properties.Settings.Default["weenielocation"];
            WeenieJsonPath = Properties.Settings.Default["weenielocation"].ToString();
        }

        private bool ValidatePath(string path)
        {
            if (!string.IsNullOrWhiteSpace(path) && Directory.Exists(path))
            {
                Properties.Settings.Default["weenielocation"] = path;
                Properties.Settings.Default.Save();
                IsPathValid = true;
                return true;
            }
            return false;
        }


            private void WeenieLocationButton_Click(object sender, RoutedEventArgs e)
        {
            using (var weenieLocation = new FolderBrowserDialog())
            {
                if (weenieLocation.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    WeenieJsonPath = weenieLocation.SelectedPath;
                    WeenieLocationPath.Content = weenieLocation.SelectedPath;
                    if(!ValidatePath(weenieLocation.SelectedPath))
                        System.Windows.MessageBox.Show("Unable to validate path to weenies");
                }
                    
            }
        }

        private void LandblockLocationButton_Click(object sender, RoutedEventArgs e)
        {
            using (var landblockLocation = new FolderBrowserDialog())
            {
                if (landblockLocation.ShowDialog() == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(landblockLocation.SelectedPath) && Directory.Exists(landblockLocation.SelectedPath))
                {
                    LandblockJsonPath = landblockLocation.SelectedPath;
                    LandblockLocationPath.Content = landblockLocation.SelectedPath;
                }
                else
                    System.Windows.MessageBox.Show("Unable to validate path to landblocks");
            }
        }

        private void TreasureLocationButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog treasureLocation = new OpenFileDialog();
            if (treasureLocation.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (File.Exists(treasureLocation.FileName))
                {
                    PrecursorJsonPath = treasureLocation.FileName;
                    TreasureLocationPath.Content = treasureLocation.FileName;
                }
                else
                    System.Windows.MessageBox.Show("Unable to open treasure json file");
            }
        }

        private void RecipeLocationButton_Click(object sender, RoutedEventArgs e)
        {
            using (var recipeLocation = new FolderBrowserDialog())
            {
                if (recipeLocation.ShowDialog() == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(recipeLocation.SelectedPath) && Directory.Exists(recipeLocation.SelectedPath))
                {
                    RecipeJsonPath = recipeLocation.SelectedPath;
                    RecipeLocationPath.Content = recipeLocation.SelectedPath;
                }
                else
                    System.Windows.MessageBox.Show("Unable to validate path to recipes");
            }
        }

        private void PrecursorLocationButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog precursorLocation = new OpenFileDialog();
            if (precursorLocation.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (File.Exists(precursorLocation.FileName))
                {
                    PrecursorJsonPath = precursorLocation.FileName;
                }
                else
                    System.Windows.MessageBox.Show("Unable to open crafing precursor json file");
            }

        }

        private void SpellLocationButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog spellLocation = new OpenFileDialog();
            if (spellLocation.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (File.Exists(spellLocation.FileName))
                {
                    SpellJsonPath = spellLocation.FileName;
                    SpellLocationPath.Content = spellLocation.FileName;
                }
                else
                    System.Windows.MessageBox.Show("Unable to open spell json file");
            }

        }

        private void CloseSettings_Click(object sender, RoutedEventArgs e)
        {
            if (!IsPathValid)
            {
                if (!ValidatePath(WeenieLocationPath.Content.ToString()))
                {
                    System.Windows.MessageBox.Show("Unable to validate path to weenies");
                }
                WeenieJsonPath = WeenieLocationPath.Content.ToString();
            }
            Close();
        }
    }
}
