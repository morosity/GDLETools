﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldBuilder
{
    public class Vector
    {
        public float x;
        public float y;
        public float z;
    }

    public class Quaternion
    {
        public float w;
        public float x;
        public float y;
        public float z;
    }

    public class Frame
    {
        public Vector Vector;
        public Quaternion Quaternion;
    }


    public class Position
    {
        public Int32 ObjectCellId { get; set; }
        public Frame Frame { get; set; }
    }
}
