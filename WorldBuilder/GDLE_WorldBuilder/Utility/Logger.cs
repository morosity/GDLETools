﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WorldBuilder
{
    public static class Logger
    {
        static StreamWriter streamWriter;
        static FileStream fileStream;

        public static void Initialize()
        {
            string filename = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\Treasure.log";
            fileStream = new FileStream(filename, FileMode.Append, FileAccess.Write);
            streamWriter = new StreamWriter(fileStream);
        }

        public static void Shutdown()
        {
            streamWriter.Close();
            fileStream.Close();
        }

        public static void Log(Severity severity, string message)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(DateTime.Now.ToShortDateString());
            sb.Append("|");
            sb.Append(severity.ToString());
            sb.Append("|");
            sb.Append(message);
            streamWriter.WriteLine(sb.ToString());
            streamWriter.Flush();
        }
    }


    public enum Severity
    {
        Info,
        Warning,
        Error
    }

}
