﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WorldBuilder
{
    /// <summary>
    /// Interaction logic for DeathTreasureChoice.xaml
    /// </summary>
    public partial class DeathTreasureChoice : Window
    {
        public static int DeathTreasureValue = 0;

        public DeathTreasureChoice()
        {
            InitializeComponent();
            dtid.Focus();
        }

        private void dtGo_Click(object sender, RoutedEventArgs e)
        {
            if (!Int32.TryParse(dtid.Text, out DeathTreasureValue))
                MessageBox.Show("Please enter number between 1 & 500", "Invalid Choice", MessageBoxButton.OK, MessageBoxImage.Error);
            else if (!TreasureTables.EntryLookupByIntIsValid("deathtreasure", "id", DeathTreasureValue))
            {
                MessageBox.Show($"Death Treasure value: {DeathTreasureValue} does not exist", "Invalid Choice", MessageBoxButton.OK, MessageBoxImage.Error);
                DeathTreasureValue = 0;
            }
            else
            {
                Close();
            }
        }

        private void dtCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
