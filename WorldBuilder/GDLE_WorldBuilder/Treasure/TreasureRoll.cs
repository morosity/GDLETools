﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldBuilder
{
    public class TreasureRoll
    {
        public ACQualities Qualities { get; set; }
        public ACQualities MutateFilter;
        public TreasureData TreasureData;
        public int WeenieClassID; // TODO: Replace with WeenieClassID
        public double LuckBonus; // LQM from DeathTreasure
        public bool HasStandardMods;
        public bool HasMagic;
        public List<string> MutationLog = new List<string>();
    }




}
