﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldBuilder
{
    internal class DeathTreasure
    {
        public int Tier { get; set; }
        public double QualMod { get; set; }
        public int HeritageDist { get; set; }
        public TreasureChance Items { get; set; }
        public TreasureChance Magic { get; set; }
        public TreasureChance Mundane { get; set; }
    }

    internal class TreasureChance
    {
        public int Chance { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
        public int Group { get; set; }
    }
}
