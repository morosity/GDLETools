﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldBuilder
{
    public class TreasureFilter
    {
        private Dictionary<int, List<MutationFilter>> mutations = new Dictionary<int, List<MutationFilter>>();
        Random random = new Random(DateTime.Now.Millisecond);

        public TreasureFilter()
        {
            mutations = DataLoader.GetJsonData();
        }



        internal bool MutateTreasure(TreasureRoll tr)
        {
            List<MutationFilter> filters = GetFiltersForMutation(tr);
            if (filters == null || filters.Count == 0)
            {
                Logger.Log(Severity.Error, $"Failed TSYS Mutation for wcid: {tr.WeenieClassID}");
                return false;
            }

            for (int f = 0; f < filters.Count; f++)
            {
                if (RollTierChance((int)tr.TreasureData.Tier, filters[f].TierChance, tr.LuckBonus))
                {
                    DoOutcome(tr.Qualities, filters[f].Outcomes, tr.LuckBonus);
                }
            }

            return true;
        }

        private void DoOutcome(ACQualities qualities, List<Outcome> outcomes, double luckBonus)
        {
            double roll = Utils.Clamp(random.NextDouble() + luckBonus, 0.0, 1.0);
            for (int x = 0; x < outcomes.Count; x++)
            {
                bool isDone = false;
                for (int y = 0; y < outcomes[x].EffectList.Count; y++)
                {
                    if (roll <= outcomes[x].EffectList[y].Probability)
                    {
                        for (int z = 0; z < outcomes[x].EffectList[y].Effects.Count; z++)
                        {
                            DoEffect(qualities, outcomes[x].EffectList[y].Effects[z], luckBonus);
                            isDone = true;
                            break;
                        }
                    }
                    if (isDone)
                        break;
                }
            }
        }

        private void DoEffect(ACQualities qualities, Effect effect, double luckBonus)
        {
            Arg result = Utils.DeepClone(effect.ArgQuality);
            result.ResolveValue(qualities);
            effect.Arg1.ResolveValue(qualities);
            effect.Arg2.ResolveValue(qualities);

            switch ((MutationEffectType)effect.EffectType)
            {
                case MutationEffectType.Assign:
                    result = Utils.DeepClone(effect.Arg1); // TODO FIX
                    break;
                case MutationEffectType.Add:
                    result += effect.Arg1;
                    break;
                case MutationEffectType.Subtract:
                    result -= effect.Arg1;
                    break;
                case MutationEffectType.Multiply:
                    result *= effect.Arg1;
                    break;
                case MutationEffectType.Divide:
                    result /= effect.Arg1;
                    break;
                case MutationEffectType.AtLeastAdd:
                    result = (result < effect.Arg1) ? effect.Arg1 : result + effect.Arg2;
                    break;
                case MutationEffectType.AtMostSubtract:
                    result = (result > effect.Arg1) ? effect.Arg1 : result - effect.Arg2;
                    break;
                case MutationEffectType.AddMultiply:
                    result += (effect.Arg1 * effect.Arg2);
                    break;
                case MutationEffectType.AddDivide:
                    result += (effect.Arg1 / effect.Arg2);
                    break;
                case MutationEffectType.SubtractMultiply:
                    result -= (effect.Arg1 * effect.Arg2);
                    break;
                case MutationEffectType.SubtractDivide:
                    result -= (effect.Arg1 / effect.Arg2);
                    break;
                case MutationEffectType.AssignAdd:
                    result = (effect.Arg1 + effect.Arg2);
                    break;
                case MutationEffectType.AssignSubtract:
                    result = (effect.Arg1 - effect.Arg2);
                    break;
                case MutationEffectType.AssignMultiply:
                    result = (effect.Arg1 * effect.Arg2);
                    break;
                case MutationEffectType.AssignDivide:
                    result = (effect.Arg1 / effect.Arg2);
                    break;
            }

            StoreValue(qualities, result);
        }

        private void StoreValue(ACQualities qualities, Arg result)
        {
            switch ((EffectArgumentType)result.Type)
            {
                case EffectArgumentType.Quality:
                    switch ((StatType)result.AsStatType)
                    {
                        case StatType.Int_StatType:
                            qualities.SetInt((INTQualities)result.AsStatIdx, result.AsInt.HasValue ? result.AsInt.Value : 0);
                            break;
                        case StatType.Float_StatType:
                            qualities.SetFloat((FLOATQualities)result.AsStatIdx, result.AsDouble.HasValue ? result.AsDouble.Value : 0);
                            break;
                        case StatType.DataID_StatType:
                            qualities.SetDataID((DIDQualities)result.AsStatIdx, result.AsInt.HasValue ? result.AsInt.Value : 0);
                            break;
                        case StatType.Bool_StatType:
                            qualities.SetBool((BoolQualities)result.AsStatIdx, result.AsInt.HasValue && result.AsInt.Value > 0 ? true : false);
                            break;
                    }
                    break;
                case EffectArgumentType.Variable:
                    break;
            }
        }

        private bool RollTierChance(int tier, List<double> tierChance, double luckBonus)
        {
            double luck = Utils.Clamp(random.NextDouble() + luckBonus, 0.0, 1.0);
            if (tierChance == null)
                return false;
            if (luck <= tierChance[tier])
                return true;
            return false;
        }

        private List<MutationFilter> GetFiltersForMutation(TreasureRoll tr)
        {
            List<MutationFilter> retVal = new List<MutationFilter>();
            int filterIndex = 0;
            if (!tr.Qualities.InqDataID(DIDQualities.TSYS_MUTATION_FILTER_DID, out filterIndex))
            {
                Logger.Log(Severity.Warning, $"No DID46 for wcid: {tr.WeenieClassID}");
                return retVal;
            }

            return mutations[filterIndex];
        }
    }
}
