﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldBuilder
{
    internal class DeathTreasureRoll
    {
        public DeathTreasure DeathTreasure { get; set; }

        public int ItemCount { get; set; }
        public int MagicCount { get; set; }
        public int MundaneCount { get; set; }

        public Dictionary<int, Weenie> Items = new Dictionary<int, Weenie>();
        public Dictionary<int, Weenie> Magic = new Dictionary<int, Weenie>();
        public Dictionary<int, Weenie> Mundane = new Dictionary<int, Weenie>();
    }
}
