﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldBuilder
{
    [Serializable]
    public class MutationFilter
    {
        [JsonProperty("Chances")]
        public List<double> TierChance { get; set; }

        [JsonProperty("Outcomes")]
        public List<Outcome> Outcomes { get; set; }

        public static List<MutationFilter> FromJson(string json) => JsonConvert.DeserializeObject<List<MutationFilter>>(json, Converter.Settings);
    }

    [Serializable]
    public class Outcome
    {
        [JsonProperty("EffectList")]
        public List<EffectList> EffectList { get; set; }
    }

    [Serializable]
    public class EffectList
    {
        [JsonProperty("Probability")]
        public double Probability { get; set; }

        [JsonProperty("Effects")]
        public List<Effect> Effects { get; set; }
    }

    [Serializable]
    public class Effect
    {
        [JsonProperty("ArgQuality")]
        public Arg ArgQuality { get; set; }

        [JsonProperty("EffectType")]
        public long EffectType { get; set; }

        [JsonProperty("Arg1")]
        public Arg Arg1 { get; set; }

        [JsonProperty("Arg2")]
        public Arg Arg2 { get; set; }

    }

    [Serializable]
    public class Arg
    {
        Random random = new Random(DateTime.Now.Millisecond);

        [JsonProperty("Type")]
        public long Type { get; set; }

        [JsonProperty("Raw")]
        public string Raw
        {
            set
            {
                DecodeRaw(value);
            }
        }

        private void DecodeRaw(string value)
        {
            var rawDecode = base64_decode(value);

            switch (Type)
            {
                case 1:
                    byte[] dblType = rawDecode.GetRange(0, 8).ToArray();
                    AsDouble = BitConverter.ToDouble(dblType, 0);
                    break;
                case 2:
                    byte[] intType = rawDecode.GetRange(0, 4).ToArray();
                    AsInt = BitConverter.ToInt32(intType, 0);
                    break;
                case 3:
                    byte[] statType = rawDecode.GetRange(0, 4).ToArray();
                    AsStatType = BitConverter.ToInt16(statType, 0);
                    var statIdx = rawDecode.GetRange(4, 4).ToArray();
                    AsStatIdx = BitConverter.ToInt16(statIdx, 0);
                    break;
                case 4:
                    byte[] rangeMin = rawDecode.GetRange(0, 4).ToArray();
                    AsRangeMin = BitConverter.ToSingle(rangeMin, 0);
                    var rangeMax = rawDecode.GetRange(4, 4).ToArray();
                    AsRangeMax = BitConverter.ToSingle(rangeMax, 0);
                    break;
                case 5:
                    Variable = 0;
                    break;
            }
        }

        public double? AsDouble { get; private set; }

        public int? AsInt { get; private set; }

        public int? AsStatType { get; private set; }
        public int? AsStatIdx { get; private set; }

        public float? AsRangeMin { get; private set; }
        public float? AsRangeMax { get; private set; }

        public UInt32? Variable { get; private set; }


        private static List<byte> base64_decode(string input)
        {

            List<byte> outbound = new List<byte>();

            List<int> T = VectorHelper.InitializedList(256, -1);
            for (int i = 0; i < 64; i++)
            {
                T["ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"[i]] = i;
            }

            int val = 0;
            int valb = -8;
            foreach (byte c in input)
            {
                if (T[c] == -1)
                {
                    break;
                }
                val = (val << 6) + T[c];
                valb += 6;
                if (valb >= 0)
                {
                    outbound.Add((byte)((val >> valb) & 0xFF));
                    valb -= 8;
                }
            }
            return outbound;
        }

        public void ResolveValue(ACQualities qualites)
        {
            switch ((EffectArgumentType)Type)
            {
                case EffectArgumentType.Double:
                case EffectArgumentType.Int:
                    break;
                case EffectArgumentType.Quality:
                    if (AsStatType.HasValue)
                    {
                        switch ((StatType)AsStatType.Value)
                        {
                            case StatType.Int_StatType:
                                AsInt = qualites.GetInt((INTQualities)AsStatIdx);
                                Type = (int)EffectArgumentType.Int;
                                break;
                            case StatType.Float_StatType:
                                AsDouble = qualites.GetFloat((FLOATQualities)AsStatIdx);
                                Type = (int)EffectArgumentType.Double;
                                break;
                            case StatType.DataID_StatType:
                                AsInt = qualites.GetDataID((DIDQualities)AsStatIdx);
                                Type = (int)EffectArgumentType.Int;
                                break;
                            case StatType.Bool_StatType:
                                AsInt = Convert.ToInt32(qualites.GetBool((BoolQualities)AsStatIdx));
                                Type = (int)EffectArgumentType.Int;
                                break;
                        }
                    }
                    break;
                case EffectArgumentType.Random:
                    AsDouble = random.NextDouble() * (AsRangeMax - AsRangeMin) + AsRangeMin;
                    break;
                case EffectArgumentType.Variable:
                    if (Variable < 0)
                        break;
                    break;
            }
        }

        public static Arg operator +(Arg a, Arg b)
        {
            Arg retval = Utils.DeepClone(a);
            switch ((EffectArgumentType)a.Type)
            {
                case EffectArgumentType.Double:
                    switch ((EffectArgumentType)b.Type)
                    {
                        case EffectArgumentType.Double:
                            retval.AsDouble += b.AsDouble;
                            return retval;
                        case EffectArgumentType.Int:
                            retval.AsDouble += (double)b.AsInt;
                            return retval;
                    }
                    break;
                case EffectArgumentType.Int:
                    switch ((EffectArgumentType)b.Type)
                    {
                        case EffectArgumentType.Double:
                            retval.AsInt += (int)b.AsDouble;
                            return retval;
                        case EffectArgumentType.Int:
                            retval.AsInt += b.AsInt;
                            return retval;
                    }
                    break;
            }
            return retval;
        }

        public static Arg operator -(Arg a, Arg b)
        {
            Arg retval = Utils.DeepClone(a);
            switch ((EffectArgumentType)a.Type)
            {
                case EffectArgumentType.Double:
                    switch ((EffectArgumentType)b.Type)
                    {
                        case EffectArgumentType.Double:
                            retval.AsDouble -= b.AsDouble;
                            return retval;
                        case EffectArgumentType.Int:
                            retval.AsDouble -= (double)b.AsInt;
                            return retval;
                    }
                    break;
                case EffectArgumentType.Int:
                    switch ((EffectArgumentType)b.Type)
                    {
                        case EffectArgumentType.Double:
                            retval.AsInt -= (int)b.AsDouble;
                            return retval;
                        case EffectArgumentType.Int:
                            retval.AsInt -= b.AsInt;
                            return retval;
                    }
                    break;
            }
            return retval;
        }

        public static Arg operator *(Arg a, Arg b)
        {
            Arg retval = Utils.DeepClone(a);
            switch ((EffectArgumentType)a.Type)
            {
                case EffectArgumentType.Double:
                    switch ((EffectArgumentType)b.Type)
                    {
                        case EffectArgumentType.Double:
                            retval.AsDouble *= b.AsDouble;
                            return retval;
                        case EffectArgumentType.Int:
                            retval.AsDouble *= (double)b.AsInt;
                            return retval;
                    }
                    break;
                case EffectArgumentType.Int:
                    switch ((EffectArgumentType)b.Type)
                    {
                        case EffectArgumentType.Double:
                            retval.AsInt *= (int)b.AsDouble;
                            return retval;
                        case EffectArgumentType.Int:
                            retval.AsInt *= b.AsInt;
                            return retval;
                    }
                    break;
            }
            return retval;
        }

        public static Arg operator /(Arg a, Arg b)
        {
            Arg retval = Utils.DeepClone(a);
            switch ((EffectArgumentType)a.Type)
            {
                case EffectArgumentType.Double:
                    switch ((EffectArgumentType)b.Type)
                    {
                        case EffectArgumentType.Double:
                            retval.AsDouble /= b.AsDouble;
                            return retval;
                        case EffectArgumentType.Int:
                            retval.AsDouble /= (double)b.AsInt;
                            return retval;
                    }
                    break;
                case EffectArgumentType.Int:
                    switch ((EffectArgumentType)b.Type)
                    {
                        case EffectArgumentType.Double:
                            retval.AsInt /= (int)b.AsDouble;
                            return retval;
                        case EffectArgumentType.Int:
                            retval.AsInt /= b.AsInt;
                            return retval;
                    }
                    break;
            }
            return retval;
        }

        public static bool operator <(Arg a, Arg b)
        {
            switch ((EffectArgumentType)a.Type)
            {
                case EffectArgumentType.Double:
                    return a.AsDouble < b.AsDouble;
                case EffectArgumentType.Int:
                    return a.AsInt < b.AsInt;
            }
            return false;
        }

        public static bool operator >(Arg a, Arg b)
        {
            switch ((EffectArgumentType)a.Type)
            {
                case EffectArgumentType.Double:
                    return a.AsDouble > b.AsDouble;
                case EffectArgumentType.Int:
                    return a.AsInt > b.AsInt;
            }
            return false;
        }
    }

    public static class Serialize
    {
        public static string ToJson(this MutationFilter[] self) => JsonConvert.SerializeObject(self, WorldBuilder.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
