﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldBuilder
{
    public class TreasureData
    {
        public int TSysMutationData;
        public WealthRating Tier;
        public int Workmanship
        {
            get => workmanship;
            set 
            { 
                workmanship = value;
                WorkmanshipMod = GetWorkmanshipMod(value);
            }
        }

        public float WorkmanshipMod { get; private set; }
        public TreasureItemClass TreasureItemClass;
        public int Material;
        public int NumberOfGems;
        public int TypeOfGems;
        public int GemsValue;
        public double BulkMod;
        public double SizeMod;
        public int CastableSpellLevel;
        public int SpellBookMaxLevel;
        public int QLWeaponOffense;
        public int QLWeaponDamageInt;
        public int QLWeaponDamageMod;
        public int QLWeaponDefense;
        public int QLWeaponSpeed;
        public int QLArmorLevel;
        public int QLShieldLevel;
        public int QLArmorEncumbrance;
        public int QLArmorVsFire;
        public int QLArmorVsCold;
        public int QLArmorVsAcid;
        public int QLArmorVsLightning;
        public double QualityModifier;

        private int workmanship;
        private float GetWorkmanshipMod(int value)
        {
            return (1.0f + (value / 9.0f));
        }
    }
}
