﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldBuilder
{
    internal class TreasureSystem
    {
        TreasureMutator tm = new TreasureMutator();
        TreasureFilter fm = new TreasureFilter();
        public DeathTreasureRoll deathTreasureRoll;
        public TreasureSystem()
        {
            TreasureTables.Initialize();
        }

        public bool MutateWCID(int wcid, Weenie weenie, bool isMagic, int tier, double lqmLuck, TreasureItemClass treasureItemClass)
        {
            if (weenie.WeenieType == (int)WeenieType.Scroll_WeenieType)
                return true;

            TreasureRoll tr = new TreasureRoll();
            tr.WeenieClassID = wcid;
            tr.HasMagic = isMagic;
            tr.HasStandardMods = true;
            tr.LuckBonus = lqmLuck;
            tr.Qualities = WeenieManager.GetWeeineQualities(weenie);
            int mutationFilterId;
            tr.Qualities.InqDataID(DIDQualities.MUTATE_FILTER_DID, out mutationFilterId);
            if (mutationFilterId == 0)
            {
                Logger.Log(Severity.Warning, $"Missing DID36 on WCID: {wcid}");
                return false;
            }
            tr.MutateFilter = GetMutationQualities(mutationFilterId);
            tr.TreasureData = new TreasureData();
            tr.TreasureData.Tier = (WealthRating)tier;
            tr.TreasureData.TreasureItemClass = treasureItemClass;
            bool retVal = tm.MutateTreasure(tr);
            if (retVal)
                retVal |= fm.MutateTreasure(tr);
            else
                tr.MutationLog.Add("Aborted Treasure Mutation");
            return retVal;
        }

        

        internal bool DoDeathTreasure(int deathTreasureValue)
        {
            deathTreasureRoll = new DeathTreasureRoll();
            deathTreasureRoll.DeathTreasure = GetDeathTreasureInfo(deathTreasureValue);
            CalculateChances(deathTreasureRoll);

            for (int i = 0; i < deathTreasureRoll.ItemCount; i++)
            {
                TreasureItemClass treasureItemClass;
                KeyValuePair<int, Weenie> newItem = RollLootItem(deathTreasureRoll.DeathTreasure.Items.Group, deathTreasureRoll.DeathTreasure.Tier, deathTreasureRoll.DeathTreasure.HeritageDist,
                    deathTreasureRoll.DeathTreasure.QualMod, out treasureItemClass);
                if (!MutateWCID(newItem.Key, newItem.Value, false, deathTreasureRoll.DeathTreasure.Tier, deathTreasureRoll.DeathTreasure.QualMod, treasureItemClass))
                {
                    Logger.Log(Severity.Error, $"Failed to Mutate WCID: {newItem.Key}");
                    return false;
                }
                if(!deathTreasureRoll.Items.ContainsKey(newItem.Key))
                    deathTreasureRoll.Items.Add(newItem.Key, newItem.Value);    
            }

            for (int i = 0; i < deathTreasureRoll.MagicCount; i++)
            {
                TreasureItemClass treasureItemClass;
                KeyValuePair<int, Weenie> newItem = RollLootItem(deathTreasureRoll.DeathTreasure.Magic.Group, deathTreasureRoll.DeathTreasure.Tier, deathTreasureRoll.DeathTreasure.HeritageDist,
                    deathTreasureRoll.DeathTreasure.QualMod, out treasureItemClass);
                if (!MutateWCID(newItem.Key, newItem.Value, true, deathTreasureRoll.DeathTreasure.Tier, deathTreasureRoll.DeathTreasure.QualMod, treasureItemClass))
                {
                    Logger.Log(Severity.Error, $"Failed to Mutate WCID: {newItem.Key}");
                    return false;
                }
                if (!deathTreasureRoll.Magic.ContainsKey(newItem.Key))
                    deathTreasureRoll.Magic.Add(newItem.Key, newItem.Value);
            }

            for (int i = 0; i < deathTreasureRoll.MundaneCount; i++)
            {
                TreasureItemClass treasureItemClass;
                KeyValuePair<int, Weenie> newItem = RollLootItem(deathTreasureRoll.DeathTreasure.Mundane.Group, deathTreasureRoll.DeathTreasure.Tier, deathTreasureRoll.DeathTreasure.HeritageDist,
                    deathTreasureRoll.DeathTreasure.QualMod, out treasureItemClass);
                if (!MutateWCID(newItem.Key, newItem.Value, false, deathTreasureRoll.DeathTreasure.Tier, deathTreasureRoll.DeathTreasure.QualMod, treasureItemClass))
                {
                    Logger.Log(Severity.Error, $"Failed to Mutate WCID: {newItem.Key}");
                    return false;
                }
                if (!deathTreasureRoll.Mundane.ContainsKey(newItem.Key))
                    deathTreasureRoll.Mundane.Add(newItem.Key, newItem.Value);
            }

            return true;
        }

        private ACQualities GetMutationQualities(int mutationFilterId)
        {
            var rows = TreasureTables.GetDataRowsByIntLookup("mutatefilter", "id", mutationFilterId);
            if (rows.Count == 0)
                return null;

            ACQualities retVal = new ACQualities();
            for (int x = 0; x < rows.Count; x++)
            {
                int qType = Convert.ToInt32(rows[x].ItemArray[1]);
                int qId = Convert.ToInt32(rows[x].ItemArray[2]);
                switch (qType)
                {
                    case 1:
                        retVal.SetInt((INTQualities)qId, 0);
                        break;
                    case 2:
                        retVal.SetFloat((FLOATQualities)qId, 0);
                        break;
                    case 3:
                        retVal.SetDataID((DIDQualities)qId, 0);
                        break;
                    case 4:
                        retVal.SetString((StringQualities)qId, "");
                        break;
                    default:
                        Logger.Log(Severity.Warning, $"Invalid Quality Type for Mutation filter: {mutationFilterId}");
                        break;
                }
            }

            return retVal;
        }

        private KeyValuePair<int, Weenie> RollLootItem(int group, int tier, int heritageDist, double qualMod, out TreasureItemClass treasureClass)
        {
            int treasureTableType = GetItemTreasureType(group);
            
            string treasureSubTable = GetItemTreasureTable(treasureTableType);
            int wcid = 0;
            int heritageRollType = 0;
            if (treasureSubTable == "gemtreasuretable")
            {
                wcid = TreasureTables.GetIntValueByChance(treasureSubTable, "class", tier, "wcid", 0);
            }
            else
            {
                if (treasureSubTable == "armortreasuretable")
                {
                    treasureSubTable = GetArmorSubtable(tier, qualMod, out treasureTableType);
                }
                else if (treasureSubTable == "clothingtreasuretable")
                {
                    heritageRollType = GetTableRollType(treasureSubTable);
                }
                else if (treasureSubTable == "weapontreasuretable")
                {
                    treasureSubTable = GetWeaponSubtable(tier, out treasureTableType);
                }
                heritageRollType = GetTableRollType(treasureSubTable);
                if (heritageRollType == 2)
                {
                    int heritageId = GetHeritageId(heritageDist);
                    treasureSubTable = treasureSubTable.Replace("treasuretable", $"{heritageId}treasuretable");
                }

                wcid = GetWCIDForTier(treasureSubTable, tier);
            }
            treasureClass = (TreasureItemClass)treasureTableType;
            Weenie weenie = WeenieManager.GetWeenie(wcid);

            return new KeyValuePair<int, Weenie>(wcid, weenie);

        }

        private int GetWCIDForTier(string treasureSubTable, int tier)
        {
            return TreasureTables.GetIntValueByChance(treasureSubTable, "tier", tier, "wcid", 0);
        }

        private int GetHeritageId(int heritageDist)
        {
            return TreasureTables.GetIntValueByChance("heritagetable", "dist", heritageDist, "heritage", 0);
        }

        private int GetTableRollType(string treasureSubTable)
        {
            return TreasureTables.GetIntByLookup("treasureitemtable", "treasureitemtablename", treasureSubTable, "rolltype");
        }

        private string GetWeaponSubtable(int tier, out int weaponTypeTableId)
        {
            weaponTypeTableId = TreasureTables.GetIntValueByChance("weapontreasuretable", "tier", tier, "weaponsubtable", 0);
            return TreasureTables.GetStringByLookup("treasureitemtable", "id", weaponTypeTableId, "treasureitemtablename");
        }

        private string GetArmorSubtable(int tier, double qualMod, out int armorTypeTableId)
        {
            armorTypeTableId = TreasureTables.GetIntValueByChance("armortreasuretable", "tier", tier, "armorsubtable", qualMod);
            return TreasureTables.GetStringByLookup("treasureitemtable", "id", armorTypeTableId, "treasureitemtablename");
        }

        private string GetItemTreasureTable(int treasureTableType)
        {
            return TreasureTables.GetStringByLookup("treasureitemtable", "id", treasureTableType, "treasureitemtablename");
        }

        private int GetItemTreasureType(int treasureGroupId)
        {
            int retVal = 0;
            retVal = TreasureTables.GetIntValueByChance("itemtreasuretable", "itemtable", treasureGroupId, "treasureitemtable", 0);
            return retVal;
        }

        private DeathTreasure GetDeathTreasureInfo(int deathTreasureValue)
        {
            var dr = TreasureTables.GetDataRowsByIntLookup("deathtreasure", "id", deathTreasureValue);
            if (dr.Count == 0)
            {
                Logger.Log(Severity.Warning, $"No death treasure found with ID: {deathTreasureValue}");
                return null;
            }
            DeathTreasure retVal;
            retVal = new DeathTreasure()
            {
                Tier = Convert.ToInt32(dr[0].ItemArray[1]),
                QualMod = Convert.ToDouble(dr[0].ItemArray[2]),
                HeritageDist = Convert.ToInt32(dr[0].ItemArray[3]),
                Items = new TreasureChance()
                {
                    Chance = Convert.ToInt32(dr[0].ItemArray[4]),
                    Min = Convert.ToInt32(dr[0].ItemArray[5]),
                    Max = Convert.ToInt32(dr[0].ItemArray[6]),
                    Group = Convert.ToInt32(dr[0].ItemArray[7])
                },
                Magic = new TreasureChance()
                {
                    Chance = Convert.ToInt32(dr[0].ItemArray[8]),
                    Min = Convert.ToInt32(dr[0].ItemArray[9]),
                    Max = Convert.ToInt32(dr[0].ItemArray[10]),
                    Group = Convert.ToInt32(dr[0].ItemArray[11])
                },
                Mundane = new TreasureChance()
                {
                    Chance = Convert.ToInt32(dr[0].ItemArray[12]),
                    Min = Convert.ToInt32(dr[0].ItemArray[13]),
                    Max = Convert.ToInt32(dr[0].ItemArray[14]),
                    Group = Convert.ToInt32(dr[0].ItemArray[15])
                }
            };

            return retVal;
        }

        private void CalculateChances(DeathTreasureRoll deathTreasureRoll)
        {
            Random random = new Random();
            int luck = random.Next(1, 100);

            //Item
            if (luck <= deathTreasureRoll.DeathTreasure.Items.Chance)
            {
                deathTreasureRoll.ItemCount = random.Next(deathTreasureRoll.DeathTreasure.Items.Min, deathTreasureRoll.DeathTreasure.Items.Max);
            }
            luck = random.Next(1, 100);

            //Magic
            if (luck <= deathTreasureRoll.DeathTreasure.Magic.Chance)
            {
                deathTreasureRoll.MagicCount = random.Next(deathTreasureRoll.DeathTreasure.Magic.Min, deathTreasureRoll.DeathTreasure.Magic.Max);
            }
            luck = random.Next(1, 100);

            //Mundane
            if (luck <= deathTreasureRoll.DeathTreasure.Mundane.Chance)
            {
                deathTreasureRoll.MundaneCount = random.Next(deathTreasureRoll.DeathTreasure.Mundane.Min, deathTreasureRoll.DeathTreasure.Mundane.Max);
            }
        }
    }
}
