﻿using System;
using System.Collections.Generic;
using System.Data;

namespace WorldBuilder
{
    public class TreasureMutator
    {
        Random random = new Random(DateTime.Now.Millisecond);

        public TreasureMutator()
        {
            
        }

        public bool MutateTreasure(TreasureRoll treasureRoll)
        {
            if (treasureRoll.Qualities == null)
            {
                Logger.Log(Severity.Error, "No qualities passed for mutating");
                return false;
            }

            if (treasureRoll.MutateFilter == null)
            {
                Logger.Log(Severity.Error, "No mutate filter passed for mutating");
                return false;
            }

            treasureRoll.TreasureData.TSysMutationData = GetTSysMutationInt(treasureRoll);

            if (treasureRoll.MutateFilter.QueryInt(INTQualities.ITEM_WORKMANSHIP_INT))
            {
                if (!SetWorkmanshipLevel(treasureRoll))
                {
                    Logger.Log(Severity.Error, "Unable to set workmanship");
                    return false;
                }
                treasureRoll.Qualities.SetInt(INTQualities.ITEM_WORKMANSHIP_INT, treasureRoll.TreasureData.Workmanship);
                treasureRoll.MutationLog.Add($"Workmanship Roll:{treasureRoll.LuckBonus} WS:{treasureRoll.TreasureData.Workmanship}");
            }

            if (treasureRoll.HasStandardMods)
            {
                if (treasureRoll.TreasureData.TreasureItemClass != TreasureItemClass.Gem_TreasureItemClass)
                {
                    if (SetMaterialType(treasureRoll.TreasureData))
                    {
                        treasureRoll.Qualities.SetInt(INTQualities.MATERIAL_TYPE_INT, treasureRoll.TreasureData.Material);
                        treasureRoll.MutationLog.Add($"Material:{treasureRoll.TreasureData.Material}");
                    }

                    if (SetNumGems(treasureRoll.TreasureData) && SetGemMaterial(treasureRoll.TreasureData))
                    {
                        treasureRoll.Qualities.SetInt(INTQualities.GEM_COUNT_INT, treasureRoll.TreasureData.NumberOfGems);
                        treasureRoll.Qualities.SetInt(INTQualities.GEM_TYPE_INT, treasureRoll.TreasureData.TypeOfGems);
                        treasureRoll.MutationLog.Add($"Gems Type:{treasureRoll.TreasureData.TypeOfGems} Count:{treasureRoll.TreasureData.NumberOfGems}");
                    }
                }
            }

            if (treasureRoll.MutateFilter.QueryFloat(FLOATQualities.WEAPON_OFFENSE_FLOAT))
            {
                if (!SetQualityLevel(ModQuality.Attack_ModQuality, treasureRoll.TreasureData, treasureRoll.LuckBonus, out treasureRoll.TreasureData.QLWeaponOffense))
                {
                    Logger.Log(Severity.Warning, "Unable to set weapon attack quality.  Defaulting to 0");
                }
                treasureRoll.MutationLog.Add($"Weapon Attack Qual Level:{treasureRoll.TreasureData.QLWeaponOffense}");
            }

            if (treasureRoll.MutateFilter.QueryInt(INTQualities.DAMAGE_INT))
            {
                if (!SetQualityLevel(ModQuality.DamageInt_ModQuality, treasureRoll.TreasureData, treasureRoll.LuckBonus, out treasureRoll.TreasureData.QLWeaponDamageInt))
                {
                    Logger.Log(Severity.Warning, "Unable to set damage int quality.  Defaulting to 0");
                }
                treasureRoll.MutationLog.Add($"Weapon DamageInt Qual Level:{treasureRoll.TreasureData.QLWeaponDamageInt}");
            }

            if (treasureRoll.MutateFilter.QueryFloat(FLOATQualities.DAMAGE_MOD_FLOAT))
            {
                if (!SetQualityLevel(ModQuality.DamageMod_ModQuality, treasureRoll.TreasureData, treasureRoll.LuckBonus, out treasureRoll.TreasureData.QLWeaponDamageMod))
                {
                    Logger.Log(Severity.Warning, "Unable to set damage mod quality.  Defaulting to 0");
                }
                treasureRoll.MutationLog.Add($"Weapon DamageMod Qual Level:{treasureRoll.TreasureData.QLWeaponDamageMod}");
            }

            if (treasureRoll.MutateFilter.QueryFloat(FLOATQualities.WEAPON_DEFENSE_FLOAT))
            {
                if (!SetQualityLevel(ModQuality.Defense_ModQuality, treasureRoll.TreasureData, treasureRoll.LuckBonus, out treasureRoll.TreasureData.QLWeaponDefense))
                {
                    Logger.Log(Severity.Warning, "Unable to set weapon defense quality.  Defaulting to 0");
                }
                treasureRoll.MutationLog.Add($"Weapon Defense Qual Level:{treasureRoll.TreasureData.QLWeaponDefense}");
            }

            if (treasureRoll.MutateFilter.QueryInt(INTQualities.WEAPON_TIME_INT))
            {
                if (!SetQualityLevel(ModQuality.Speed_ModQuality, treasureRoll.TreasureData, treasureRoll.LuckBonus, out treasureRoll.TreasureData.QLWeaponSpeed))
                {
                    Logger.Log(Severity.Warning, "Unable to set weapon speed quality.  Defaulting to 0");
                }
                treasureRoll.MutationLog.Add($"Weapon Speed Qual Level:{treasureRoll.TreasureData.QLWeaponSpeed}");
            }

            if (treasureRoll.MutateFilter.QueryInt(INTQualities.ARMOR_LEVEL_INT))
            {
                if (!SetQualityLevel(ModQuality.Protection_ModQuality, treasureRoll.TreasureData, treasureRoll.LuckBonus, out treasureRoll.TreasureData.QLArmorLevel))
                {
                    Logger.Log(Severity.Warning, "Unable to set armor level quality.  Defaulting to 0");
                }
                treasureRoll.MutationLog.Add($"Armor Level Qual Level:{treasureRoll.TreasureData.QLArmorLevel}");
            }

            if (treasureRoll.MutateFilter.QueryInt(INTQualities.SHIELD_VALUE_INT))
            {
                if (!SetQualityLevel(ModQuality.Protection_ModQuality, treasureRoll.TreasureData, treasureRoll.LuckBonus, out treasureRoll.TreasureData.QLShieldLevel))
                {
                    Logger.Log(Severity.Warning, "Unable to set shield value quality.  Defaulting to 0");
                }
                treasureRoll.MutationLog.Add($"Shield Value Qual Level:{treasureRoll.TreasureData.QLShieldLevel}");
            }

            if (treasureRoll.MutateFilter.QueryInt(INTQualities.ENCUMB_VAL_INT))
            {
                if (!SetQualityLevel(ModQuality.Encumbrance_ModQuality, treasureRoll.TreasureData, treasureRoll.LuckBonus, out treasureRoll.TreasureData.QLArmorEncumbrance))
                {
                    Logger.Log(Severity.Warning, "Unable to set armor encumbrance quality.  Defaulting to 0");
                }
                treasureRoll.MutationLog.Add($"Armor Encumbrance Qual Level:{treasureRoll.TreasureData.QLArmorEncumbrance}");
            }

            if (treasureRoll.MutateFilter.QueryFloat(FLOATQualities.ARMOR_MOD_VS_FIRE_FLOAT))
            {
                if (!SetQualityLevel(ModQuality.FireResistance_ModQuality, treasureRoll.TreasureData, -treasureRoll.LuckBonus, out treasureRoll.TreasureData.QLArmorVsFire))
                {
                    Logger.Log(Severity.Warning, "Unable to set armor mod vs fire quality.  Defaulting to 0");
                }
                treasureRoll.MutationLog.Add($"Armor Mod Vs Fire Qual Level:{treasureRoll.TreasureData.QLArmorVsFire}");
            }

            if (treasureRoll.MutateFilter.QueryFloat(FLOATQualities.ARMOR_MOD_VS_COLD_FLOAT))
            {
                if (!SetQualityLevel(ModQuality.ColdResistance_ModQuality, treasureRoll.TreasureData, -treasureRoll.LuckBonus, out treasureRoll.TreasureData.QLArmorVsCold))
                {
                    Logger.Log(Severity.Warning, "Unable to set armor mod vs cold quality.  Defaulting to 0");
                }
                treasureRoll.MutationLog.Add($"Armor Mod Vs Cold Qual Level:{treasureRoll.TreasureData.QLArmorVsCold}");
            }

            if (treasureRoll.MutateFilter.QueryFloat(FLOATQualities.ARMOR_MOD_VS_ACID_FLOAT))
            {
                if (!SetQualityLevel(ModQuality.AcidResistance_ModQuality, treasureRoll.TreasureData, -treasureRoll.LuckBonus, out treasureRoll.TreasureData.QLArmorVsAcid))
                {
                    Logger.Log(Severity.Warning, "Unable to set armor mod vs acid quality.  Defaulting to 0");
                }
                treasureRoll.MutationLog.Add($"Armor Mod Vs Acid Qual Level:{treasureRoll.TreasureData.QLArmorVsAcid}");
            }

            if (treasureRoll.MutateFilter.QueryFloat(FLOATQualities.ARMOR_MOD_VS_ELECTRIC_FLOAT))
            {
                if (!SetQualityLevel(ModQuality.LightningResistance_ModQuality, treasureRoll.TreasureData, -treasureRoll.LuckBonus, out treasureRoll.TreasureData.QLArmorVsLightning))
                {
                    Logger.Log(Severity.Warning, "Unable to set armor mod vs lightning quality.  Defaulting to 0");
                }
                treasureRoll.MutationLog.Add($"Armor Mod Vs Lightning Qual Level:{treasureRoll.TreasureData.QLArmorVsLightning}");
            }

            if (!SetQualityMods(treasureRoll))
            {
                Logger.Log(Severity.Error, "Error while setting new values");
                return false;
            }

            if (treasureRoll.Qualities.QueryDataID(DIDQualities.SETUP_DID) && !Modify3DObj(treasureRoll))
            {
                Logger.Log(Severity.Error, "Failed to update Palette and Shade");
                return false;
            }

            if (treasureRoll.HasMagic && !AssignMagicSpells(treasureRoll))
            {
                Logger.Log(Severity.Error, "Failed to add magic to item");
                return false;
            }

            if (treasureRoll.Qualities.QueryInt(INTQualities.VALUE_INT) && !SetNewValue(treasureRoll))
            {
                Logger.Log(Severity.Error, "Failed to set value of item");
                return false;
            }

            if (!SetItemString(treasureRoll))
            {
                Logger.Log(Severity.Error, "Failed to set item string");
                return false;
            }
            
            return true;
        }

        private bool SetItemString(TreasureRoll treasureRoll)
        {
            bool doname = treasureRoll.MutateFilter.QueryString(StringQualities.NAME_STRING);
            bool dolongd = treasureRoll.MutateFilter.QueryString(StringQualities.LONG_DESC_STRING);

            if (!doname && !dolongd) return true;

            string basename = string.Empty;
            int decoration = 0;
            string itemNameWithSpell = string.Empty;
            if (!treasureRoll.Qualities.InqString(StringQualities.NAME_STRING, out basename))
            {
                Logger.Log(Severity.Warning, $"No NAME_STRING found for wcid: {treasureRoll.WeenieClassID}");
            }

            if (doname)
            {
                if (!(treasureRoll.TreasureData.TreasureItemClass == TreasureItemClass.Gem_TreasureItemClass))
                {
                    if((treasureRoll.TreasureData.Material != (int)MaterialType.Leather) ||
                        ((treasureRoll.TreasureData.TreasureItemClass != TreasureItemClass.LeatherArmor_TreasureItemClass) && 
                        (treasureRoll.TreasureData.TreasureItemClass != TreasureItemClass.StuddedLeatherArmor_TreasureItemClass)))
                    {
                        string materialName = string.Empty;
                        materialName = TreasureTables.GetStringByLookup("materialmod", "material", treasureRoll.TreasureData.Material, "name");
                        if (!string.IsNullOrEmpty(materialName))
                        {
                            decoration |= (int)AppraisalLongDescDecorations.LDDecoration_PrependMaterial;
                            string newName = materialName + " " + basename;
                            treasureRoll.Qualities.SetString(StringQualities.NAME_STRING, newName);
                        }
                    }
                }
            }

            if (dolongd)
            {
                decoration |= (int)AppraisalLongDescDecorations.LDDecoration_PrependWorkmanship;

                int spellId = 0;
                
                if (treasureRoll.Qualities.InqDataID(DIDQualities.SPELL_DID, out spellId))
                {
                    itemNameWithSpell = GetItemCastableDescriptionString(spellId);
                }
                else
                {
                    List<int> itemSpells = treasureRoll.Qualities.GetSpellBook();
                    for (int x = 0; x < itemSpells.Count; x++)
                    {
                        if (TreasureTables.GetIntByLookup("spelldata", "spellid", x, "targetType") == 16)
                        {
                            if (TreasureTables.GetIntByLookup("spelldata", "spellid", x, "nodescentry") == 0)
                            {
                                itemNameWithSpell = GetItemCastableDescriptionString(spellId);
                            }
                        }
                    }
                }

                if (treasureRoll.TreasureData.NumberOfGems > 0)
                {
                    decoration |= (int)AppraisalLongDescDecorations.LDDecoration_AppendGemInfo;
                }

                string finalName = basename;
                finalName += itemNameWithSpell.Length > 0 ? " " + itemNameWithSpell : "";
                treasureRoll.Qualities.SetString(StringQualities.LONG_DESC_STRING, finalName);
            }

            if (decoration > 0)
            {
                treasureRoll.Qualities.SetInt(INTQualities.APPRAISAL_LONG_DESC_DECORATION_INT, decoration);
            }
            return true;
        }

        private string GetItemCastableDescriptionString(int spellId)
        {
            return TreasureTables.GetStringByLookup("spelldata", "spellid", spellId, "descriptor");
        }

        private bool SetNewValue(TreasureRoll treasureRoll)
        {
            int value = 0;
            if (!treasureRoll.Qualities.InqInt(INTQualities.VALUE_INT, out value))
            {
                Logger.Log(Severity.Warning, $"No base value. Starting with base of 0 for WCID: {treasureRoll.WeenieClassID}");
            }

            if (treasureRoll.TreasureData.TreasureItemClass == TreasureItemClass.Gem_TreasureItemClass)
            {
                value = (int)(treasureRoll.TreasureData.Workmanship * GetGemModifiedBaseValue(value));
            }
            else if (IsArmor(treasureRoll.TreasureData.TreasureItemClass))
            {
                treasureRoll.TreasureData.BulkMod = 1.0;
                treasureRoll.Qualities.InqFloat(FLOATQualities.BULK_MOD_FLOAT, out treasureRoll.TreasureData.BulkMod);
                treasureRoll.TreasureData.SizeMod = 1.0;
                treasureRoll.Qualities.InqFloat(FLOATQualities.SIZE_MOD_FLOAT, out treasureRoll.TreasureData.SizeMod);

                int armorLevel = 0;
                if (!treasureRoll.Qualities.InqInt(INTQualities.ARMOR_LEVEL_INT, out armorLevel))
                {
                    Logger.Log(Severity.Error, "Armo tem has no armor level");
                    return false;
                }

                value = GetNewArmorValue(armorLevel, treasureRoll.TreasureData);
            }
            else
            {
                value = GetNewItemValue(value, treasureRoll.TreasureData);
            }

            int itemMana = 0;
            if (treasureRoll.Qualities.InqInt(INTQualities.ITEM_MAX_MANA_INT, out itemMana))
            {
                value += 2 * itemMana;
            }

            int spellSum = treasureRoll.TreasureData.CastableSpellLevel + treasureRoll.TreasureData.SpellBookMaxLevel;
            value += 10 * spellSum;

            treasureRoll.Qualities.SetInt(INTQualities.VALUE_INT, value);

            if (treasureRoll.TreasureData.TreasureItemClass == TreasureItemClass.Gem_TreasureItemClass)
            {
                int stackSize = 0;
                treasureRoll.Qualities.InqInt(INTQualities.MAX_STACK_SIZE_INT, out stackSize);
                if (stackSize != 1)
                {
                    Logger.Log(Severity.Error, "Gem has no stack size");
                    return false;
                }
                treasureRoll.Qualities.SetInt(INTQualities.STACK_UNIT_VALUE_INT, value);
            }

            return true;
        }

        private int GetNewItemValue(int value, TreasureData treasureData)
        {
            double matMod = 1 + TreasureTables.GetFloatByLookup("materialmod", "material", treasureData.Material, "multiplier");
            if (matMod == 0)
            {
                Logger.Log(Severity.Error, $"Missing material value modifier for Material: {treasureData.Material}");
            }

            double newVal = value / 3 + matMod * GetTreasureValueFromWR(treasureData.Tier) + treasureData.GemsValue;
            double roll = random.NextDouble() * (1.25 - .7) + .7;
            newVal *= (treasureData.WorkmanshipMod + treasureData.QualityModifier) * roll;
            newVal += 2 * (double)value / 3;
            newVal = (double)Math.Ceiling(newVal);

            newVal = newVal < value ? value : newVal;
            return (int)newVal;
        }

        private double GetTreasureValueFromWR(WealthRating tier)
        {
            if (tier ==  WealthRating.Shoddy_WealthRating) return 25;
            else if (tier == WealthRating.Poor_WealthRating) return 50;
            else if (tier == WealthRating.Medium_WealthRating) return 100;
            else if (tier == WealthRating.Good_WealthRating) return 250;
            else if (tier == WealthRating.Rich_WealthRating) return 500;
            else if (tier == WealthRating.Incomparable_WealthRating) return 1000;
            else if (tier == WealthRating.Exceptional_WealthRating) return 1500;
            else if (tier == WealthRating.Phenomenal_WealthRating) return 2500;
            else
            {
                Logger.Log(Severity.Error, $"Invalid tier: {tier}");
            }

            return 0;
        }

        private int GetNewArmorValue(int armorLevel, TreasureData treasureData)
        {
            double modVal = (double)(Math.Pow((double)armorLevel, 2.0) / 10 - 20);
            modVal = modVal < 10 ? 10 : modVal;
            modVal *= (double)(treasureData.BulkMod * treasureData.SizeMod);

            modVal /= 10;
            modVal += .5;
            modVal = (double)((int)modVal);
            modVal *= 10;

            double matMod = TreasureTables.GetFloatByLookup("materialmod", "material", treasureData.Material, "multiplier");
            if (matMod == 0)
            {
                Logger.Log(Severity.Error, $"Missing material value modifier for Material: {treasureData.Material}");
            }

            double newVal = (double)((modVal / 10) * matMod * 0.016 + treasureData.GemsValue);
            newVal *= (treasureData.WorkmanshipMod + treasureData.QualityModifier) / 2;
            newVal += modVal;
            newVal = (double)Math.Ceiling(newVal);

            return (int)newVal;
        }

        private int GetGemModifiedBaseValue(int value)
        {
            return (int)((value / 2) * (random.NextDouble() * (20 - 1) + 1));
        }

        private bool AssignMagicSpells(TreasureRoll treasureRoll)
        {
            bool spellsAdded = false;
            int maxSpellPower = 0;
            double diffAdjustment = 0.0f;

            if (SetItemSpells(treasureRoll, ref maxSpellPower))
            {
                spellsAdded = true;
            }

            if (SetEnchantmentSpells(treasureRoll, ref maxSpellPower, ref diffAdjustment))
            {
                spellsAdded = true;
            }

            if (SetCastableSpell(treasureRoll, ref maxSpellPower))
            {
                spellsAdded = true;
            }

            if (SetCantrips(treasureRoll, ref diffAdjustment))
            {
                spellsAdded = true;
            }

            if (!spellsAdded)
            {
                Logger.Log(Severity.Error, "Failed to add spells");
                return false;
            }

            bool retVal = true;
            if (!FinalizeMagicItemQualities(treasureRoll, maxSpellPower, diffAdjustment))
            {
                Logger.Log(Severity.Error, "Failed to finalize magic item");
                retVal = false;
            }

            int spellDid = 0;
            if (treasureRoll.Qualities.InqDataID(DIDQualities.SPELL_DID, out spellDid) || treasureRoll.Qualities.HasSpellBook())
            {
                int mask = 0;
                treasureRoll.Qualities.InqInt(INTQualities.UI_EFFECTS_INT, out mask);
                mask |= (int)UI_EFFECT_TYPE.UI_EFFECT_MAGICAL;
                treasureRoll.Qualities.SetInt(INTQualities.UI_EFFECTS_INT, mask);
            }

            return retVal;
        }

        private bool FinalizeMagicItemQualities(TreasureRoll treasureRoll, int maxSpellPower, double diffAdjustment)
        {
            int spellcraft = GetFinalSpellcraft(maxSpellPower, treasureRoll.TreasureData.TreasureItemClass, treasureRoll.WeenieClassID);
            treasureRoll.Qualities.SetInt(INTQualities.ITEM_SPELLCRAFT_INT, spellcraft);
            treasureRoll.MutationLog.Add($"Spellcraft: {spellcraft}");

            int castableSpellMana = 0;
            double rate = 0;

            int maxSpellMana = GetMaxSpellMana(treasureRoll.Qualities, ref castableSpellMana);

            if (castableSpellMana > 0)
            {
                double costMult = 1;
                if ((treasureRoll.TreasureData.TreasureItemClass == TreasureItemClass.Caster_TreasureItemClass) && (treasureRoll.WeenieClassID != 2366))
                {
                    costMult = .5;
                }
                treasureRoll.Qualities.SetInt(INTQualities.ITEM_MANA_COST_INT, (int)(castableSpellMana * costMult));
            }

            if (maxSpellMana > 0)
            {
                rate = GetManaRate(maxSpellMana);
                treasureRoll.Qualities.SetFloat(FLOATQualities.MANA_RATE_FLOAT, rate);
            }

            if (maxSpellMana < castableSpellMana)
                maxSpellMana = castableSpellMana;

            int itemMana = GetItemMana(maxSpellMana, treasureRoll.TreasureData.Workmanship, treasureRoll.TreasureData.TreasureItemClass, treasureRoll.WeenieClassID);

            treasureRoll.Qualities.SetInt(INTQualities.ITEM_MAX_MANA_INT, itemMana);
            treasureRoll.Qualities.SetInt(INTQualities.ITEM_CUR_MANA_INT, itemMana);

            int skillLim = SetSkillLimit(treasureRoll, spellcraft);

            int diff = 0;
            switch (treasureRoll.TreasureData.TreasureItemClass)
            {
                case TreasureItemClass.Gem_TreasureItemClass:
                    diff = 0;
                    break;
                default:
                    diff = GetItemDiff(spellcraft, skillLim);
                    break;
            }

            diff += (int)diffAdjustment;
            treasureRoll.Qualities.SetInt(INTQualities.ITEM_DIFFICULTY_INT, diff);
            return true;
        }

        private int GetItemDiff(int spellcraft, int skillLim)
        {
            double diff = 2 * spellcraft;
            diff -= (skillLim / 2);
            return (int)(diff < 0 ? 0 : Math.Floor(diff));
        }

        private int SetSkillLimit(TreasureRoll treasureRoll, int spellcraft)
        {
            int skillLimit = GetItemSkillLimit(spellcraft, treasureRoll.TreasureData.TreasureItemClass);
            treasureRoll.Qualities.SetInt(INTQualities.ITEM_SKILL_LEVEL_LIMIT_INT, skillLimit);

            if (skillLimit == 0)
                return 0;

            int limitSkill = 0;
            if (IsPhysicalWeapon(treasureRoll.TreasureData.TreasureItemClass))
            {
                treasureRoll.Qualities.InqInt(INTQualities.WEAPON_SKILL_INT, out limitSkill);
            }
            else if (IsArmor(treasureRoll.TreasureData.TreasureItemClass))
            {
                double roll = random.NextDouble();
                if (roll <= .5)
                {
                    limitSkill = 6; // Melee Defense
                }
                else
                {
                    limitSkill = 7; // Missile Defense
                }
                skillLimit = (int)(skillLimit * .7);
                treasureRoll.Qualities.SetInt(INTQualities.ITEM_SKILL_LEVEL_LIMIT_INT, skillLimit);
            }
            else
            {
                Logger.Log(Severity.Error, $"Unexpected treasure type: {treasureRoll.TreasureData.TreasureItemClass}");
                return 0;
            }

            treasureRoll.Qualities.SetDataID(DIDQualities.ITEM_SKILL_LIMIT_DID, limitSkill);
            return skillLimit;
        }

        private int GetItemSkillLimit(int spellcraft, TreasureItemClass treasureItemClass)
        {
            double roll = random.NextDouble();
            int retVal = 0;
            if (roll < GetItemSkillLimitProb(treasureItemClass))
            {
                retVal = spellcraft + 20;
            }
            return retVal;
        }

        private double GetItemSkillLimitProb(TreasureItemClass treasureItemClass)
        {
            if (IsPhysicalWeapon(treasureItemClass))
            {
                return 1.0;
            }
            else if (IsArmor(treasureItemClass))
            {
                return .55;
            }
            return 0;
        }

        private int GetItemMana(int maxSpellMana, int workmanship, TreasureItemClass treasureItemClass, int weenieClassID)
        {
            double wsManaMod = GetWorkmanshipManaMod(workmanship);
            double manaChargeMod = GetItemManaChargeMod(treasureItemClass, weenieClassID);
            return (int)Math.Ceiling(maxSpellMana * wsManaMod * manaChargeMod);
        }

        private double GetItemManaChargeMod(TreasureItemClass treasureItemClass, int weenieClassID)
        {
            int min, max = 0;

            if (IsArmor(treasureItemClass) || treasureItemClass == TreasureItemClass.Clothing_TreasureItemClass)
            {
                min = 6;
                max = 15;
            }
            else if (treasureItemClass == TreasureItemClass.Gem_TreasureItemClass)
            {
                min = 1;
                max = 1;
            }
            else if (weenieClassID == 112414) // TODO get crown wcid
            {
                min = 6;
                max = 15;
            }
            else if (treasureItemClass == TreasureItemClass.Jewelry_TreasureItemClass)
            {
                min = 12;
                max = 20;
            }
            else if (IsWeapon(treasureItemClass) || treasureItemClass == TreasureItemClass.ArtObject_TreasureItemClass)
            {
                min = 6;
                max = 15;
            }
            else
            {
                Logger.Log(Severity.Warning, $"Unknown TreasureClass: {treasureItemClass} to WCID: {weenieClassID} combination found");
                return 1;
            }

            return (int)random.Next(min, max);
        }

        private double GetWorkmanshipManaMod(int workmanship)
        {
            return (1 + ((double)workmanship - 1) / 9);
        }

        private double GetManaRate(int maxSpellMana)
        {
            maxSpellMana = maxSpellMana < 1 ? 1 : maxSpellMana;

            double retVal = (double)Math.Ceiling(1200.0f / maxSpellMana);
            retVal = -1 / retVal;

            return retVal;
        }

        private int GetMaxSpellMana(ACQualities qualities, ref int castableSpellMana)
        {
            int spellId = 0;
            if (qualities.InqDataID(DIDQualities.SPELL_DID, out spellId))
            {
                int spellMana = TreasureTables.GetIntByLookup("spelldata", "spellid", spellId, "mana");
                castableSpellMana = 5 * spellMana;
            }

            int tempMana = 0;
            int maxMana = 0;
            List<int> spells = qualities.GetSpellBook();
            for (int x = 0; x < spells.Count; x++)
            {
                tempMana = TreasureTables.GetIntByLookup("spelldata", "spellid", spells[x], "mana");
                if (tempMana > maxMana)
                    maxMana = tempMana;
            }

            return maxMana;
        }

        private int GetFinalSpellcraft(int maxSpellPower, TreasureItemClass treasureItemClass, int weenieClassID)
        {
            double mod = GetSpellCraftMod(treasureItemClass);
            int retVal = (int)Math.Ceiling(maxSpellPower * mod);
            return retVal;
        }

        private double GetSpellCraftMod(TreasureItemClass treasureItemClass)
        {
            double min, max = 0;

            if (IsArmor(treasureItemClass) || IsWeapon(treasureItemClass) || treasureItemClass == TreasureItemClass.Clothing_TreasureItemClass ||
                treasureItemClass == TreasureItemClass.ArtObject_TreasureItemClass || treasureItemClass == TreasureItemClass.Jewelry_TreasureItemClass)
            {
                min = 0.9;
                max = 1.1;
            }
            else if (treasureItemClass == TreasureItemClass.Gem_TreasureItemClass)
            {
                min = 1;
                max = 1;
            }
            else
            {
                Logger.Log(Severity.Warning, $"Unknown Treasure type: {treasureItemClass}");
                return 1.0;
            }

            return random.NextDouble() * (max - min) + min;
        }

        private bool SetCantrips(TreasureRoll treasureRoll, ref double diffAdjustment)
        {
            string cantripTable = GetCantripSpellTable(treasureRoll);
            if (cantripTable == string.Empty)
            {
                return false;
            }

            int legoCantrips = 0;
            int epicCantrips = 0;
            int majorCantrips = 0;
            int totalCantrips = GetCantripCount(treasureRoll, ref majorCantrips, ref epicCantrips, ref legoCantrips);

            int maxAttempts = totalCantrips * 3;

            List<int> cantripIdList = new List<int>();
            while ((cantripIdList.Count <= totalCantrips) && (maxAttempts > 0))
            {
                double cantripLuck = random.NextDouble();
                int cantripId = TreasureTables.GetIntValueByChance(cantripTable, "tier", (int)treasureRoll.TreasureData.Tier,
                    "spellid", cantripLuck);

                cantripId = AdjustForWeaponMastery(treasureRoll, cantripId);

                if (!cantripIdList.Contains(cantripId))
                {
                    cantripIdList.Add(cantripId);
                }
                --maxAttempts;
            }

            bool retVal = false;
            for (int i = 0; i < cantripIdList.Count; i++)
            {
                if (legoCantrips > 0)
                {
                    cantripIdList[i] = GetCantripByLevel(cantripIdList[i], 4);
                    double diffRoll = random.NextDouble() * (30 - 15) + 15;
                    diffAdjustment += diffRoll;
                    --legoCantrips;
                }
                else if (epicCantrips > 0)
                {
                    cantripIdList[i] = GetCantripByLevel(cantripIdList[i], 3);
                    double diffRoll = random.NextDouble() * (25 - 15) + 15;
                    diffAdjustment += diffRoll;
                    --epicCantrips;
                }
                else if (majorCantrips > 0)
                {
                    cantripIdList[i] = GetCantripByLevel(cantripIdList[i], 2);
                    double diffRoll = random.NextDouble() * (20 - 10) + 10;
                    diffAdjustment += diffRoll;
                    --majorCantrips;
                }
                else
                {
                    double diffRoll = random.NextDouble() * (10 - 5) + 5;
                    diffAdjustment += diffRoll;
                }

                retVal |= treasureRoll.Qualities.AddSpell(cantripIdList[i]);
            }

            return retVal;
        }

        private int GetCantripByLevel(int cantripId, int cantripLevel)
        {
            string lookupFieldName = string.Empty;
            switch (cantripLevel)
            {
                case 2:
                    lookupFieldName = "major";
                    break;
                case 3:
                    lookupFieldName = "epic";
                    break;
                case 4:
                    lookupFieldName = "lego";
                    break;
                default:
                    Logger.Log(Severity.Error, "Invalid cantrip level");
                    return cantripId;
            }
            return TreasureTables.GetIntByLookup("cantriplevelprogression", "minor", cantripId, lookupFieldName);
        }

        private int AdjustForWeaponMastery(TreasureRoll treasureRoll, int cantripId)
        {
            if (cantripId == 2539) // Only weapon based cantrip in weapon table used as flag
            {
                int weaponSkillId;
                if (treasureRoll.Qualities.InqInt(INTQualities.WEAPON_SKILL_INT, out weaponSkillId))
                {
                    cantripId = CantripToMasterySpell(weaponSkillId);
                }
            }
            return cantripId;
        }

        private int CantripToMasterySpell(int weaponSkillId)
        {
            switch (weaponSkillId)
            {
                case 45:
                    return 2557;
                case 44:
                    return 2566;
                case 46:
                    return 2544;
                case 47:
                    return 2540;
                case 41:
                    return 5072;
            }
            return weaponSkillId;
        }

        private int GetCantripCount(TreasureRoll treasureRoll, ref int majorCAntrips, ref int epicCantrips, ref int legoCantrips)
        {
            int chances = 0;
            float fChanceOfCantrip = 0.0f;
            float fMajorUpgradeChance = 0.0f;
            float fEpicUpgradeChance = 0.0f;
            float fLegendaryUpgradeChance = 0.0f;

            switch (treasureRoll.TreasureData.Tier)
            {
                case WealthRating.Shoddy_WealthRating:
                case WealthRating.Poor_WealthRating:
                    break;

                case WealthRating.Medium_WealthRating:
                    chances = 1;
                    fChanceOfCantrip = 0.01f;
                    break;

                case WealthRating.Good_WealthRating:
                case WealthRating.Rich_WealthRating:
                    chances = 3;
                    fChanceOfCantrip = 0.03f;
                    fMajorUpgradeChance = 0.01f;
                    break;

                case WealthRating.Incomparable_WealthRating:
                    chances = 5;
                    fChanceOfCantrip = 0.05f;
                    fMajorUpgradeChance = 0.03f;
                    break;

                case WealthRating.Exceptional_WealthRating:
                    chances = 5;
                    fChanceOfCantrip = 0.05f;
                    fMajorUpgradeChance = 0.04f;
                    fEpicUpgradeChance = 0.03f;
                    break;

                case WealthRating.Phenomenal_WealthRating:
                    chances = 5;
                    fChanceOfCantrip = 0.05f;
                    fMajorUpgradeChance = 0.04f;
                    fEpicUpgradeChance = 0.03f;
                    fLegendaryUpgradeChance = 0.02f;
                    break;
            }

            int cantrips = 0;
            for (int i = 0; i < chances; i++)
            {
                if ((random.NextDouble() + treasureRoll.LuckBonus) / 10.0f < fChanceOfCantrip)
                {
                    ++cantrips;
                    if ((random.NextDouble() + treasureRoll.LuckBonus) / 10.0f < fMajorUpgradeChance)
                    {
                        ++majorCAntrips;
                    }
                    else if ((random.NextDouble() + treasureRoll.LuckBonus) / 10.0f < fEpicUpgradeChance)
                    {
                        ++epicCantrips;
                    }
                    else if ((random.NextDouble() + treasureRoll.LuckBonus) / 10.0f < fLegendaryUpgradeChance)
                    {
                        ++legoCantrips;
                    }
                }
            }
            return cantrips;
        }

        private string GetCantripSpellTable(TreasureRoll treasureRoll)
        {
            string retVal = string.Empty;
            if (IsArmor(treasureRoll.TreasureData.TreasureItemClass))
            {
                int shieldLevel = 0;
                if (treasureRoll.Qualities.InqInt(INTQualities.SHIELD_VALUE_INT, out shieldLevel))
                {
                    retVal = "cantripshielddist";
                }
                else
                {
                    retVal = "cantriparmordist";
                }
            }
            else if (treasureRoll.TreasureData.TreasureItemClass == TreasureItemClass.Caster_TreasureItemClass)
            {
                retVal = "cantripcasterdist";
            }
            else if (IsMissileWeapon(treasureRoll.TreasureData.TreasureItemClass))
            {
                retVal = "catripnmissiledist";
            }
            else if (IsMeleeWeapon(treasureRoll.TreasureData.TreasureItemClass))
            {
                retVal = "cantripmeleedist";
            }
            return retVal;
        }

        private bool SetCastableSpell(TreasureRoll treasureRoll, ref int maxSpellPower)
        {
            if (!GetsCastableSpell(treasureRoll.TreasureData.TreasureItemClass, treasureRoll.TreasureData.Tier))
            {
                return false;
            }

            string spellTable = "wandstaffspells";
            if (treasureRoll.WeenieClassID == 2366)
            {
                spellTable = "orbspells";
            }

            double roll = random.NextDouble();
            int spellId = TreasureTables.GetIntValueByChance(spellTable, "tier", (int)treasureRoll.TreasureData.Tier, "spell", roll);
            int spellLevel = 0;
            spellId = SelectSpellLevel(spellId, (int)treasureRoll.TreasureData.Tier, out spellLevel, treasureRoll.LuckBonus);

            bool retVal = treasureRoll.Qualities.SetDataID(DIDQualities.SPELL_DID, spellId);
            treasureRoll.TreasureData.CastableSpellLevel = spellLevel;

            int bitField = 0;
            treasureRoll.Qualities.InqInt(INTQualities.ITEM_USEABLE_INT, out bitField);
            uint uses = (uint)bitField;
            uses &= (uint)ITEM_USEABLE.USEABLE_WIELDED;
            uses &= (uint)ITEM_USEABLE.USEABLE_REMOTE;
            uses &= (uint)ITEM_USEABLE.USEABLE_NEVER_WALK;
            treasureRoll.Qualities.SetInt(INTQualities.ITEM_USEABLE_INT, (int)uses);

            int spellPower = GetSpellPower(spellId);
            if (spellPower > maxSpellPower)
            {
                maxSpellPower = spellPower;
            }
            return retVal;
        }

        private bool GetsCastableSpell(TreasureItemClass treasureItemClass, WealthRating tier)
        {
            if (treasureItemClass != TreasureItemClass.Caster_TreasureItemClass)
                return false;
            return true;
        }

        private bool SetEnchantmentSpells(TreasureRoll treasureRoll, ref int maxSpellPower, ref double diffAdjustment)
        {
            int cEndowments = GetNumEndowments(treasureRoll.TreasureData.TreasureItemClass, treasureRoll.TreasureData.Tier, treasureRoll.LuckBonus);
            if (cEndowments <= 0)
            {
                return false;
            }
            if (cEndowments > 3)
            {
                cEndowments = 3;
            }

            List<int> spellIdList = new List<int>();
            List<int> spellLevels = new List<int>();
            int maxAttempts = cEndowments * 3;
            int spellCode = GetSpellCodeIndex(treasureRoll.TreasureData.TSysMutationData);
            while ((spellIdList.Count <= cEndowments) && (maxAttempts > 0))
            {
                double spellRoll = random.NextDouble();
                int newSpellId = TreasureTables.GetIntValueByChance("spellcodedisttable", "group", spellCode, "spell", spellRoll);

                if (!spellIdList.Contains(newSpellId))
                {
                    spellIdList.Add(newSpellId);
                    spellLevels.Add(1);
                }
                --maxAttempts;
            }

            int maxSpellLevel = 0;
            int indexOfHighestLevelSpell = 0;
            for (int i = 0; i < spellIdList.Count; i++)
            {
                int spellLevel = 0;
                spellIdList[i] = SelectSpellLevel(spellIdList[i], (int)treasureRoll.TreasureData.Tier, out spellLevel, treasureRoll.LuckBonus);
                spellLevels[i] = spellLevel;

                int spellPower = GetSpellPower(spellIdList[i]);
                if (spellPower > maxSpellPower)
                    maxSpellPower = spellPower;

                if (spellLevel > maxSpellLevel)
                {
                    maxSpellLevel = spellLevel;
                    indexOfHighestLevelSpell = i;
                }
            }

            bool retVal = false;

            for (int i = 0; i < spellIdList.Count; i++)
            {
                if (i != indexOfHighestLevelSpell)
                {
                    double roll = random.NextDouble() * (1.5 - .5) + .5;
                    roll *= (5 * spellLevels[i]);
                    diffAdjustment += roll;
                }

                if (treasureRoll.TreasureData.TreasureItemClass == TreasureItemClass.Gem_TreasureItemClass)
                {
                    retVal |= treasureRoll.Qualities.SetDataID(DIDQualities.SPELL_DID, spellIdList[i]);
                    treasureRoll.Qualities.SetInt(INTQualities.ITEM_USEABLE_INT, (int)ITEM_USEABLE.USEABLE_CONTAINED);
                }
                else
                {
                    retVal |= treasureRoll.Qualities.AddSpell(spellIdList[i]);
                }

            }

            treasureRoll.TreasureData.SpellBookMaxLevel = maxSpellLevel;
            return retVal;
        }

        private int GetNumEndowments(TreasureItemClass treasureItemClass, WealthRating tier, double luckBonus)
        {
            double chance = 0;

            if (treasureItemClass == TreasureItemClass.Caster_TreasureItemClass)
            {
                switch (tier)
                {
                    case WealthRating.Shoddy_WealthRating:
                    case WealthRating.Poor_WealthRating:
                    case WealthRating.Medium_WealthRating:
                    case WealthRating.Good_WealthRating:
                    case WealthRating.Rich_WealthRating:
                        chance = 0.60;
                        break;
                    case WealthRating.Incomparable_WealthRating:
                        chance = 0.75;
                        break;
                    case WealthRating.Exceptional_WealthRating:
                        chance = 0.80;
                        break;
                    case WealthRating.Phenomenal_WealthRating:
                        chance = 0.85;
                        break;
                    default:
                        Logger.Log(Severity.Error, "Invalid WealthRating");
                        break;
                }

                double casterLuckBonus = random.NextDouble() + luckBonus;
                if (casterLuckBonus < chance)
                    return 1;
                else
                    return 0;
            }

            if (IsArmor(treasureItemClass) || IsPhysicalWeapon(treasureItemClass))
            {
                switch (tier)
                {
                    case WealthRating.Shoddy_WealthRating:
                        chance = 0;
                        break;
                    case WealthRating.Poor_WealthRating:
                        chance = 0.05;
                        break;
                    case WealthRating.Medium_WealthRating:
                        chance = .1;
                        break;
                    case WealthRating.Good_WealthRating:
                        chance = .2;
                        break;
                    case WealthRating.Rich_WealthRating:
                        chance = 04;
                        break;
                    case WealthRating.Incomparable_WealthRating:
                        chance = 0.6;
                        break;
                    case WealthRating.Exceptional_WealthRating:
                        chance = 0.7;
                        break;
                    case WealthRating.Phenomenal_WealthRating:
                        chance = 0.8;
                        break;
                    default:
                        Logger.Log(Severity.Error, "Invalid WealthRating");
                        break;
                }
                double casterLuckBonus = random.NextDouble() + luckBonus;
                if (casterLuckBonus < chance)
                    return 1;
                else
                    return 0;
            }

            if ((treasureItemClass == TreasureItemClass.Jewelry_TreasureItemClass) || (treasureItemClass == TreasureItemClass.Clothing_TreasureItemClass) ||
                treasureItemClass == TreasureItemClass.ArtObject_TreasureItemClass)
            {
                int retVal = 1;
                switch (tier)
                {
                    case WealthRating.Shoddy_WealthRating:
                    case WealthRating.Poor_WealthRating:
                    case WealthRating.Medium_WealthRating:
                    case WealthRating.Good_WealthRating:
                    case WealthRating.Rich_WealthRating:
                        double bonusSpells = random.NextDouble();
                        if (bonusSpells < (.10 + luckBonus))
                        {
                            retVal++;
                        }
                        break;
                    case WealthRating.Incomparable_WealthRating:
                        bonusSpells = random.NextDouble();
                        if (bonusSpells < (.10 + luckBonus))
                        {
                            retVal++;
                            if (random.NextDouble() < (.05 + luckBonus))
                            {
                                retVal++;
                            }
                        }
                        break;
                    case WealthRating.Exceptional_WealthRating:
                        bonusSpells = random.NextDouble();
                        if (bonusSpells < (.12 + luckBonus))
                        {
                            retVal++;
                            if (random.NextDouble() < (.06 + luckBonus))
                            {
                                retVal++;
                            }
                        }
                        break;
                    case WealthRating.Phenomenal_WealthRating:
                        bonusSpells = random.NextDouble();
                        if (bonusSpells < (.15 + luckBonus))
                        {
                            retVal++;
                            if (random.NextDouble() < (.044 + luckBonus))
                            {
                                retVal++;
                            }
                        }
                        break;
                    default:
                        Logger.Log(Severity.Error, "Invalid WealthRating");
                        break;
                }
            }

            return 1;
        }

        private bool SetItemSpells(TreasureRoll treasureRoll, ref int maxSpellPower)
        {
            if ((!IsWeapon(treasureRoll.TreasureData.TreasureItemClass) && !(IsArmor(treasureRoll.TreasureData.TreasureItemClass))))
            {
                return false;
            }

            string spellTable = string.Empty;
            if (IsMeleeWeapon(treasureRoll.TreasureData.TreasureItemClass))
            {
                spellTable = "meleeweaponspells";
            }
            else if (IsMissileWeapon(treasureRoll.TreasureData.TreasureItemClass))
            {
                spellTable = "missileweaponspells";
            }
            else if (treasureRoll.TreasureData.TreasureItemClass == TreasureItemClass.Caster_TreasureItemClass)
            {
                spellTable = "casterweaponspells";
            }
            else
            {
                spellTable = "itembanespells";
            }

            bool retVal = false;

            double probaility = 0.0;

            var possibleSpells = TreasureTables.GetSpellDataByTier(spellTable, (int)treasureRoll.TreasureData.Tier);
            for (int s = 0; s < possibleSpells.Count; s++)
            {
                probaility = Convert.ToDouble(possibleSpells[s].ItemArray[2].ToString());
                if (random.NextDouble() + treasureRoll.LuckBonus > probaility)
                {
                    int spellLevel = 0;
                    int spellId = SelectSpellLevel(Convert.ToInt32(possibleSpells[s].ItemArray[1].ToString()), (int)treasureRoll.TreasureData.Tier, out spellLevel, treasureRoll.LuckBonus);
                    if (spellId == 0)
                    {
                        Logger.Log(Severity.Error, "Invalid spellId returned");
                        return false;
                    }

                    int currentSpellPower = GetSpellPower(spellId);
                    if (currentSpellPower > maxSpellPower)
                    {
                        maxSpellPower = currentSpellPower;
                    }

                    retVal |= treasureRoll.Qualities.AddSpell(spellId);
                }
            }

            return retVal;
        }

        private int GetSpellPower(int spellId)
        {
            return TreasureTables.GetIntByLookup("spelldata", "spellid", spellId, "diff");
        }

        private int SelectSpellLevel(int spellId, int tier, out int spellLevel, double luckBonus)
        {
            spellLevel = TreasureTables.GetIntValueByChance("spellleveltable", "tier", tier, "level", luckBonus);
            return GetSpellByLevel(spellId, spellLevel);
        }

        private int GetSpellByLevel(int spellId, int finalSpellLevel)
        {
            DataRow spellRow = TreasureTables.GetRowByIntLookup("spelllevelprogression", "lvl1", spellId);
            if (spellRow != null && spellRow.ItemArray.Length > 0)
            {
                return Convert.ToInt32(spellRow.ItemArray[finalSpellLevel - 1].ToString());
            }
            return 0;
        }

        private bool IsArmor(TreasureItemClass treasureItemClass)
        {
            return ((treasureItemClass == TreasureItemClass.Armor_TreasureItemClass) ||
                    (treasureItemClass == TreasureItemClass.LeatherArmor_TreasureItemClass) ||
                    (treasureItemClass == TreasureItemClass.StuddedLeatherArmor_TreasureItemClass) ||
                    (treasureItemClass == TreasureItemClass.ChainMailArmor_TreasureItemClass) ||
                    (treasureItemClass == TreasureItemClass.CovenantArmor_TreasureItemClass) ||
                    (treasureItemClass == TreasureItemClass.PlateMailArmor_TreasureItemClass) ||
                    (treasureItemClass == TreasureItemClass.HeritageLowArmor_TreasureItemClass) ||
                    (treasureItemClass == TreasureItemClass.HeritageHighArmor_TreasureItemClass));
        }

        private bool IsWeapon(TreasureItemClass treasureItemClass)
        {
            return ((treasureItemClass == TreasureItemClass.Weapon_TreasureItemClass) ||
                    (treasureItemClass == TreasureItemClass.Caster_TreasureItemClass) ||
                    IsMeleeWeapon(treasureItemClass) ||
                    IsMissileWeapon(treasureItemClass));
        }

        private bool IsMissileWeapon(TreasureItemClass treasureItemClass)
        {
            return ((treasureItemClass == TreasureItemClass.BowWeapon_TreasureItemClass) ||
                    (treasureItemClass == TreasureItemClass.CrossbowWeapon_TreasureItemClass) ||
                    (treasureItemClass == TreasureItemClass.AtlatlWeapon_TreasureItemClass));
        }

        private bool IsMeleeWeapon(TreasureItemClass treasureItemClass)
        {
            return ((treasureItemClass == TreasureItemClass.SwordWeapon_TreasureItemClass) ||
                    (treasureItemClass == TreasureItemClass.MaceWeapon_TreasureItemClass) ||
                    (treasureItemClass == TreasureItemClass.AxeWeapon_TreasureItemClass) ||
                    (treasureItemClass == TreasureItemClass.SpearWeapon_TreasureItemClass) ||
                    (treasureItemClass == TreasureItemClass.UnarmedWeapon_TreasureItemClass) ||
                    (treasureItemClass == TreasureItemClass.StaffWeapon_TreasureItemClass) ||
                    (treasureItemClass == TreasureItemClass.DaggerWeapon_TreasureItemClass));
        }

        private bool IsPhysicalWeapon(TreasureItemClass treasureItemClass)
        {
            return (IsMeleeWeapon(treasureItemClass) || IsMissileWeapon(treasureItemClass));
        }

        private bool Modify3DObj(TreasureRoll treasureRoll)
        {
            PALETTE_TEMPLATE_ID palette = PALETTE_TEMPLATE_ID.UNDEF_PALETTE_TEMPLATE;
            double betterColorProb = ((treasureRoll.TreasureData.Workmanship * treasureRoll.TreasureData.Workmanship) / 100);
            double luck = random.NextDouble();

            switch (treasureRoll.TreasureData.TreasureItemClass)
            {
                case TreasureItemClass.Jewelry_TreasureItemClass:
                case TreasureItemClass.ArtObject_TreasureItemClass:
                case TreasureItemClass.Weapon_TreasureItemClass:
                case TreasureItemClass.Caster_TreasureItemClass:
                case TreasureItemClass.SwordWeapon_TreasureItemClass:
                case TreasureItemClass.MaceWeapon_TreasureItemClass:
                case TreasureItemClass.AxeWeapon_TreasureItemClass:
                case TreasureItemClass.SpearWeapon_TreasureItemClass:
                case TreasureItemClass.UnarmedWeapon_TreasureItemClass:
                case TreasureItemClass.StaffWeapon_TreasureItemClass:
                case TreasureItemClass.DaggerWeapon_TreasureItemClass:
                case TreasureItemClass.BowWeapon_TreasureItemClass:
                case TreasureItemClass.CrossbowWeapon_TreasureItemClass:
                case TreasureItemClass.AtlatlWeapon_TreasureItemClass:
                    palette = SelectPaletteTemplateID(treasureRoll.TreasureData.Material, GetColorCodeIndex(treasureRoll.TreasureData.TSysMutationData));
                    break;

                case TreasureItemClass.Gem_TreasureItemClass:
                    double gemluck = random.NextDouble();
                    palette = (PALETTE_TEMPLATE_ID)TreasureTables.GetIntValueByChanceTwoParam("materialcolordist", "material", "group", GetMaterialFromQualities(treasureRoll), 8, "color", gemluck);
                    break;

                case TreasureItemClass.Clothing_TreasureItemClass:
                    palette = (PALETTE_TEMPLATE_ID)TreasureTables.GetIntValueByChance("clothingpalette", "type", 1, "color", luck);
                    break;

                case TreasureItemClass.LeatherArmor_TreasureItemClass:
                case TreasureItemClass.StuddedLeatherArmor_TreasureItemClass:
                    if (luck > betterColorProb)
                    {
                        palette = SelectPaletteTemplateID(treasureRoll.TreasureData.Material, GetColorCodeIndex(treasureRoll.TreasureData.TSysMutationData));
                    }
                    else
                    {
                        palette = (PALETTE_TEMPLATE_ID)TreasureTables.GetIntValueByChance("clothingpalette", "type", 1, "color", luck);
                    }
                    break;

                case TreasureItemClass.ChainMailArmor_TreasureItemClass:
                case TreasureItemClass.CovenantArmor_TreasureItemClass:
                case TreasureItemClass.PlateMailArmor_TreasureItemClass:
                case TreasureItemClass.HeritageLowArmor_TreasureItemClass:
                case TreasureItemClass.HeritageHighArmor_TreasureItemClass:
                    if (luck > betterColorProb)
                    {
                        palette = SelectPaletteTemplateID(treasureRoll.TreasureData.Material, GetColorCodeIndex(treasureRoll.TreasureData.TSysMutationData));
                    }
                    else
                    {
                        if (BehavesLikeLeather(treasureRoll.WeenieClassID))
                        {
                            palette = (PALETTE_TEMPLATE_ID)TreasureTables.GetIntValueByChance("bootarmorpalette", "type", 2, "color", luck);
                        }
                        else
                        {
                            palette = (PALETTE_TEMPLATE_ID)TreasureTables.GetIntValueByChance("metalarmorpalette", "type", 3, "color", luck);
                        }
                    }
                    break;
                default:
                    Logger.Log(Severity.Error, "Invalid TreasureClass");
                    return false;
            }

            if (palette == PALETTE_TEMPLATE_ID.UNDEF_PALETTE_TEMPLATE)
            {
                Logger.Log(Severity.Error, $"Unable to find palette for wcid:{treasureRoll.WeenieClassID} with material:{treasureRoll.TreasureData.Material}");
            }
            else
            {
                int oldPalette = 0;
                treasureRoll.Qualities.InqInt(INTQualities.PALETTE_TEMPLATE_INT, out oldPalette);
                if (palette != (PALETTE_TEMPLATE_ID)oldPalette)
                {
                    treasureRoll.Qualities.SetInt(INTQualities.PALETTE_TEMPLATE_INT, (int)palette);
                }
            }

            double oldshade = 0.0;
            treasureRoll.Qualities.InqFloat(FLOATQualities.SHADE_FLOAT, out oldshade);
            double newshade = random.NextDouble();
            if (newshade != oldshade)
            {
                treasureRoll.Qualities.SetFloat(FLOATQualities.SHADE_FLOAT, newshade);
            }

            treasureRoll.MutationLog.Add($"New Palette:{palette.ToString()} with Shade:{newshade}");

            return true;
        }

        private int GetMaterialFromQualities(TreasureRoll treasureRoll)
        {
            int value = 0;
            if (treasureRoll.Qualities.InqInt(INTQualities.PALETTE_TEMPLATE_INT, out value))
                return value;

            Logger.Log(Severity.Error, $"WCID:{treasureRoll.WeenieClassID} missing PALETTE_TEMPLATE_INT");
            return 1;
        }

        private bool BehavesLikeLeather(int weenieClassID)
        {
            return ((weenieClassID == 7897) || (weenieClassID == 6047) || (weenieClassID == 6005));
        }

        private PALETTE_TEMPLATE_ID SelectPaletteTemplateID(int material, int colorcode)
        {
            double luck = random.NextDouble();
            return (PALETTE_TEMPLATE_ID)TreasureTables.GetIntValueByChanceTwoParam("materialcolordist", "group", "material", colorcode, material, "color", luck);
        }

        private bool SetQualityMods(TreasureRoll treasureRoll)
        {
            if (treasureRoll.TreasureData.QLWeaponOffense > 0)
            {
                double value = 0.0f;
                treasureRoll.Qualities.InqFloat(FLOATQualities.WEAPON_OFFENSE_FLOAT, out value);
                value += GetAttackMod(treasureRoll.TreasureData.QLWeaponOffense);
                treasureRoll.Qualities.SetFloat(FLOATQualities.WEAPON_OFFENSE_FLOAT, value);
                treasureRoll.MutationLog.Add($"Weapon Offense Value: {value}");
            }

            if (treasureRoll.TreasureData.QLWeaponDefense > 0)
            {
                double value = 0.0f;
                treasureRoll.Qualities.InqFloat(FLOATQualities.WEAPON_DEFENSE_FLOAT, out value);
                value += GetDefenseMod(treasureRoll.TreasureData.QLWeaponOffense);
                treasureRoll.Qualities.SetFloat(FLOATQualities.WEAPON_DEFENSE_FLOAT, value);
                treasureRoll.MutationLog.Add($"Weapon Defense Value: {value}");
            }

            if (treasureRoll.TreasureData.QLWeaponDamageInt > 0)
            {
                int value = 0;
                if (treasureRoll.Qualities.InqInt(INTQualities.DAMAGE_INT, out value))
                {
                    value = GetDamIntMod(treasureRoll.TreasureData.QLWeaponDamageInt, value);
                    treasureRoll.Qualities.SetInt(INTQualities.DAMAGE_INT, value);
                    treasureRoll.MutationLog.Add($"Weapon DamageInt Value: {value}");
                }
                else
                {
                    Logger.Log(Severity.Warning, "Item does not have DAMAGE_INT to modify");
                }
            }

            if (treasureRoll.TreasureData.QLWeaponDamageMod > 0)
            {
                double value = 1.0f;
                if (!treasureRoll.Qualities.InqFloat(FLOATQualities.DAMAGE_MOD_FLOAT, out value))
                {
                    Logger.Log(Severity.Warning, "Missing DAMAGE_MOD_FLOAT. Using 1.0");
                }
                value *= GetQualDamMultiplier(treasureRoll.TreasureData.QLWeaponDamageMod);
                treasureRoll.Qualities.SetFloat(FLOATQualities.DAMAGE_MOD_FLOAT, value);
                treasureRoll.MutationLog.Add($"Weapon DamageMod Value: {value}");
            }

            if (treasureRoll.TreasureData.QLWeaponSpeed > 0)
            {
                int value = 0;
                if (treasureRoll.Qualities.InqInt(INTQualities.WEAPON_TIME_INT, out value))
                {
                    value = (int)(value * GetSpeedMod(treasureRoll.TreasureData.QLWeaponSpeed));
                    treasureRoll.Qualities.SetInt(INTQualities.WEAPON_TIME_INT, value);
                    treasureRoll.MutationLog.Add($"Weapon Speed Value: {value}");
                }
                else
                {
                    Logger.Log(Severity.Error, "Missing WEAPON_TIME_ONE. No action");
                }
            }

            if (treasureRoll.TreasureData.QLArmorLevel > 0)
            {
                int value = 0;
                if (treasureRoll.Qualities.InqInt(INTQualities.ARMOR_LEVEL_INT, out value))
                {
                    value = GetNewArmorLevel(value, treasureRoll.TreasureData.QLArmorLevel);
                    treasureRoll.Qualities.SetInt(INTQualities.ARMOR_LEVEL_INT, value);
                    treasureRoll.MutationLog.Add($"Armor Level Value: {value}");
                }
                else
                {
                    Logger.Log(Severity.Warning, "Missing ARMOR_LEVEL_INT. No action");
                }
            }

            if (treasureRoll.TreasureData.QLShieldLevel > 0)
            {
                int value = 0;
                if (treasureRoll.Qualities.InqInt(INTQualities.SHIELD_VALUE_INT, out value))
                {
                    value = GetNewArmorLevel(value, treasureRoll.TreasureData.QLShieldLevel);
                    treasureRoll.Qualities.SetInt(INTQualities.SHIELD_VALUE_INT, value);
                    treasureRoll.MutationLog.Add($"Shield Level Value: {value}");
                }
                else
                {
                    Logger.Log(Severity.Warning, "Missing SHIELD_VALUE_INT. No action");
                }
            }

            if (treasureRoll.TreasureData.QLArmorEncumbrance > 0)
            {
                int value = 0;
                if (treasureRoll.Qualities.InqInt(INTQualities.ENCUMB_VAL_INT, out value))
                {
                    float encMod = GetEncumbranceMod(treasureRoll.TreasureData.QLArmorEncumbrance);
                    value = (int)(value * encMod);
                    value = value < 1 ? 1 : value;
                    treasureRoll.Qualities.SetInt(INTQualities.ENCUMB_VAL_INT, value);
                    treasureRoll.MutationLog.Add($"Encumbrance Value: {value}");
                    double bulkMod = 1.0;
                    treasureRoll.Qualities.InqFloat(FLOATQualities.BULK_MOD_FLOAT, out bulkMod);
                    bulkMod /= encMod;
                    treasureRoll.Qualities.SetFloat(FLOATQualities.BULK_MOD_FLOAT, bulkMod);

                }
                else
                {
                    Logger.Log(Severity.Warning, "Missing ENCUMB_VAL_INT. No action");
                }
            }

            if (treasureRoll.TreasureData.QLArmorVsFire > 0)
            {
                double value = 0.0f;
                if (!treasureRoll.Qualities.InqFloat(FLOATQualities.ARMOR_MOD_VS_FIRE_FLOAT, out value))
                {
                    Logger.Log(Severity.Error, "Item missing ARMOR_MOD_VS_FIRE_FLOAT skipping remaind mods");
                    return false;
                }
                value += GetResistanceMod(treasureRoll.TreasureData.QLArmorVsFire);
                treasureRoll.Qualities.SetFloat(FLOATQualities.ARMOR_MOD_VS_FIRE_FLOAT, value);
                treasureRoll.MutationLog.Add($"Amor Vs Fire Value: {value}");
            }

            if (treasureRoll.TreasureData.QLArmorVsCold > 0)
            {
                double value = 0.0f;
                if (!treasureRoll.Qualities.InqFloat(FLOATQualities.ARMOR_MOD_VS_COLD_FLOAT, out value))
                {
                    Logger.Log(Severity.Error, "Item missing ARMOR_MOD_VS_COLD_FLOAT skipping remaind mods");
                    return false;
                }
                value += GetResistanceMod(treasureRoll.TreasureData.QLArmorVsCold);
                treasureRoll.Qualities.SetFloat(FLOATQualities.ARMOR_MOD_VS_COLD_FLOAT, value);
                treasureRoll.MutationLog.Add($"Amor Vs Cold Value: {value}");
            }

            if (treasureRoll.TreasureData.QLArmorVsAcid > 0)
            {
                double value = 0.0f;
                if (!treasureRoll.Qualities.InqFloat(FLOATQualities.ARMOR_MOD_VS_ACID_FLOAT, out value))
                {
                    Logger.Log(Severity.Error, "Item missing ARMOR_MOD_VS_ACID_FLOAT skipping remaind mods");
                    return false;
                }
                value += GetResistanceMod(treasureRoll.TreasureData.QLArmorVsAcid);
                treasureRoll.Qualities.SetFloat(FLOATQualities.ARMOR_MOD_VS_ACID_FLOAT, value);
                treasureRoll.MutationLog.Add($"Amor Vs Acid Value: {value}");
            }

            if (treasureRoll.TreasureData.QLArmorVsLightning > 0)
            {
                double value = 0.0f;
                if (!treasureRoll.Qualities.InqFloat(FLOATQualities.ARMOR_MOD_VS_ELECTRIC_FLOAT, out value))
                {
                    Logger.Log(Severity.Error, "Item missing ARMOR_MOD_VS_ELECTRIC_FLOAT skipping remaind mods");
                    return false;
                }
                value += GetResistanceMod(treasureRoll.TreasureData.QLArmorVsLightning);
                treasureRoll.Qualities.SetFloat(FLOATQualities.ARMOR_MOD_VS_ELECTRIC_FLOAT, value);
                treasureRoll.MutationLog.Add($"Amor Vs Lightning Value: {value}");
            }

            int qlSum = treasureRoll.TreasureData.QLArmorEncumbrance + treasureRoll.TreasureData.QLArmorLevel +
                treasureRoll.TreasureData.QLArmorVsAcid + treasureRoll.TreasureData.QLArmorVsCold + treasureRoll.TreasureData.QLArmorVsFire +
                treasureRoll.TreasureData.QLArmorVsLightning + treasureRoll.TreasureData.QLShieldLevel + treasureRoll.TreasureData.QLWeaponDamageInt +
                treasureRoll.TreasureData.QLWeaponDamageMod + treasureRoll.TreasureData.QLWeaponDefense + treasureRoll.TreasureData.QLWeaponOffense +
                treasureRoll.TreasureData.QLWeaponSpeed;
            if(qlSum > 0)
                treasureRoll.TreasureData.QualityModifier = 1.0 / (double)qlSum;

            return true;
        }

        private double GetResistanceMod(int qualLevel)
        {
            double min = TreasureTables.GetFloatByLookup("armorelementresistances", "quallevel", (int)qualLevel / 2, "min");
            double max = TreasureTables.GetFloatByLookup("armorelementresistances", "quallevel", (int)qualLevel / 2, "max");
            return (float)(random.NextDouble() * (max - min) + min);
        }

        private float GetEncumbranceMod(int qualLevel)
        {
            float retVal = 1.0f;
            double chance = TreasureTables.GetFloatByLookup("encumbrancemodchance", "quallevel", (int)qualLevel / 2, "chance");
            if (chance <= random.NextDouble())
            {
                double min = TreasureTables.GetFloatByLookup("encumbrancemodchance", "quallevel", (int)qualLevel / 2, "min");
                double max = TreasureTables.GetFloatByLookup("encumbrancemodchance", "quallevel", (int)qualLevel / 2, "max");
                retVal = (float)(random.NextDouble() * (max - min) + min);
            }
            return retVal;
        }

        private int GetNewArmorLevel(int value, int qualLevel)
        {
            int index = GetArmorLevelIndex(value);
            int alAdjust = random.Next(-5, 5);
            return GetNewArmorLevel(index + qualLevel) + alAdjust;
        }

        private int GetArmorLevelIndex(int value) // TODO: Adjust for more eor ratings
        {
            int index = 0;

            if (value < 20) index = 0;
            else if (value < 30) index = 1;
            else if (value < 40) index = 2;
            else if (value < 60) index = 3;
            else if (value < 80) index = 4;
            else if (value < 100) index = 5;
            else if (value < 120) index = 6;
            else if (value < 135) index = 7;
            else if (value < 150) index = 8;
            else if (value < 180) index = 9;
            else if (value < 220) index = 10;
            else if (value < 260) index = 11;
            else index = 12;

            return index;
        }

        private int GetNewArmorLevel(int newIndex)
        {
            int maxlevel = 12;
            int retVal = 0;
            if (newIndex > maxlevel)
            {
                Logger.Log(Severity.Info, $"New armor above max level. Adjusting to max:{maxlevel}");
                newIndex = maxlevel;
            }

            switch (newIndex)
            {
                case 0: retVal = 20; break;
                case 1: retVal = 30; break;
                case 2: retVal = 40; break;
                case 3: retVal = 60; break;
                case 4: retVal = 80; break;
                case 5: retVal = 100; break;
                case 6: retVal = 120; break;
                case 7: retVal = 135; break;
                case 8: retVal = 150; break;
                case 9: retVal = 180; break;
                case 10: retVal = 220; break;
                case 11: retVal = 260; break;
                case 12: retVal = 300; break;
                default:
                    Logger.Log(Severity.Error, $"New armor index invalid: {newIndex}");
                    break;
            }

            return retVal;


        }

        private double GetSpeedMod(int qualLevel)
        {
            double minMod = TreasureTables.GetFloatByLookup("weaponspeedlevel", "quallevel", (int)qualLevel / 2, "min");
            double maxMod = TreasureTables.GetFloatByLookup("weaponspeedlevel", "quallevel", (int)qualLevel / 2, "max");
            return (random.NextDouble() * (maxMod - minMod) + minMod);
        }

        private int GetDamIntMod(int qualLevel, int value)
        {
            float damMult = GetQualDamMultiplier(qualLevel);
            return ((int)Math.Ceiling(value * damMult));
        }

        private float GetQualDamMultiplier(int qualLevel)
        {
            switch (qualLevel)
            {
                case 1:
                    return 1.2f;
                case 2:
                    return 1.4f;
                case 3:
                    return 1.5f;
                case 4:
                case 5:
                    return 1.6f;
                case 6:
                case 7:
                case 8:
                    return 1.7f;
                case 9:
                case 10:
                case 11:
                case 12:
                    return 1.8f;
            }
            return 1.0f;
        }

        private double GetDefenseMod(int qualLevel)
        {
            return GetAttackMod(qualLevel);
        }

        private double GetAttackMod(int qualLevel)
        {
            double minMod = TreasureTables.GetFloatByLookup("attackdefensequalmod", "quallevel", qualLevel, "min");
            double maxMod = TreasureTables.GetFloatByLookup("attackdefensequalmod", "quallevel", qualLevel, "max");
            return (random.NextDouble() * (maxMod - minMod) + minMod);
        }

        private bool SetQualityLevel(ModQuality modQuality, TreasureData treasureData, double luck, out int qualLevel)
        {
            qualLevel = 0;
            double modLookupChance = 0.0;
            try
            {
                modLookupChance = TreasureTables.GetFloatByLookup("qualitymod", "qualmod", (int)modQuality, ((int)treasureData.Tier).ToString());
            }
            catch
            {
                Logger.Log(Severity.Error, "Unable to find ModQuality");
                return false;
            }

            double qlLuck = random.NextDouble() + luck;
            qlLuck = Utils.Clamp(qlLuck, 0.0, 1.0);
            if (modLookupChance >= qlLuck)
            {
                qualLevel = TreasureTables.GetIntValueByChance("qualitylevels", "tier", (int)treasureData.Tier, "quallevel", Math.Abs(luck));
            }
            return true;
        }

        private bool SetGemMaterial(TreasureData treasureData)
        {
            int gemClass = TreasureTables.GetIntValueByChance("gemclassbytier", "tier", (int)treasureData.Tier, "gemclass", 0);
            if (gemClass == 0)
            {
                Logger.Log(Severity.Error, "No gem class found");
                return false;
            }
            int gemValue = TreasureTables.GetIntByLookup("gemclassvalues", "class", gemClass, "value");
            if (gemValue == 0)
            {
                Logger.Log(Severity.Error, "Gem value not found");
                return false;
            }

            treasureData.GemsValue = (int)Math.Ceiling((treasureData.NumberOfGems * treasureData.WorkmanshipMod * gemValue) / 4.0f);

            int gemType = TreasureTables.GetIntValueByChance("gemtreasuretable", "class", gemClass, "material", 0);
            if (gemType == 0)
            {
                Logger.Log(Severity.Error, $"No gem found for class:{gemClass}");
                return false;
            }

            treasureData.TypeOfGems = gemType;
            return true;
        }

        private bool SetNumGems(TreasureData treasureData)
        {
            int index = GetGemCodeIndex(treasureData.TSysMutationData);
            if (index == 0)
            {
                Logger.Log(Severity.Error, "Gem selection not found");
                return false;
            }

            treasureData.NumberOfGems = TreasureTables.GetIntValueByChanceTwoParam("gemcodedisttable", "group", "tier", index, (int)treasureData.Tier, "count", 0);

            return (treasureData.NumberOfGems > 0);
        }

        private bool SetMaterialType(TreasureData treasureData)
        {
            int index = GetMaterialCodeIndex(treasureData.TSysMutationData);
            if (index == 0)
            {
                Logger.Log(Severity.Error, "No valid material selection");
                return false;
            }

            int baseMat = TreasureTables.GetIntValueByChanceTwoParam("materialcodedisttable", "group", "tier", index, (int)treasureData.Tier, "material", 0);

            string matTable = string.Empty;
            switch (baseMat)
            {
                case 1:
                    matTable = "materialceramic";
                    break;
                case 3:
                    matTable = "materialcloth";
                    break;
                case 9:
                    matTable = "materialgem";
                    break;
                case 51:
                    treasureData.Material = 51;
                    break;
                case 52:
                    matTable = "materialleather";
                    break;
                case 56:
                    matTable = "materialmetal";
                    break;
                case 65:
                    matTable = "materialstone";
                    break;
                case 72:
                    matTable = "materialwood";
                    break;
                default:
                    Logger.Log(Severity.Error, $"Invalid base material code: {baseMat}");
                    return false;
            }

            if (matTable != string.Empty)
            {
                
                int finalMat = TreasureTables.GetIntValueByChance(matTable, "tier", (int)treasureData.Tier, "material", 0);
                if (finalMat == 0)
                {
                    Logger.Log(Severity.Error, $"Invalid material: {baseMat} Table: {matTable}");
                    return false;
                }
                treasureData.Material = finalMat;
            }
            return true;
        }

        private int GetMaterialCodeIndex(int sysMutationData)
        {
            return (sysMutationData & 0x000000FF);
        }

        private int GetColorCodeIndex(int sysMutationData)
        {
            return (sysMutationData & 0x00FF0000) >> 16;
        }

        private int GetGemCodeIndex(int sysMutationData)
        {
            return (sysMutationData & 0x0000FF00) >> 8;
        }

        private int GetSpellCodeIndex(int sysMutationData)
        {
            return (int)((sysMutationData & 0xFF000000) >> 24);
        }

        private bool SetWorkmanshipLevel(TreasureRoll treasureRoll)
        {
            if (treasureRoll.WeenieClassID == 2348) //Ingot - this had mutationdid and mutationint (material/gem/color) retired?
            {
                treasureRoll.TreasureData.Workmanship = 1;
            }
            else
            {
                try
                {
                    treasureRoll.TreasureData.Workmanship = TreasureTables.GetIntValueByChance("workmanshipdist", "tier", (int)treasureRoll.TreasureData.Tier, "ws", treasureRoll.LuckBonus);
                }
                catch
                {
                    Logger.Log(Severity.Error, $"Unable to set Workmanship on WCID:{treasureRoll.WeenieClassID}");
                    return false;
                }
            }
            return true;
        }

        private int GetTSysMutationInt(TreasureRoll treasureRoll)
        {
            int retVal = 0;
            if (!treasureRoll.Qualities.InqInt(INTQualities.TSYS_MUTATION_DATA_INT, out retVal))
            {
                Logger.Log(Severity.Error, "No mutation INT found");
            }
            return retVal;
        }
    }
}
