﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldBuilder
{
    public static class TreasureTables
    {
        static DataSet treasureTables = null;
        static bool IsInitailized = false;
        static Random random = new Random(DateTime.Now.Millisecond);
        
        public static void Initialize()
        {
            if (IsInitailized && treasureTables != null)
                return;
            treasureTables = new DataSet();

            treasureTables.Tables.Add(DataLoader.GetDataTable("armortreasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("arttreasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("atlatltreasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("axe1treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("axe2treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("axe3treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("bow1treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("bow2treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("bow3treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("castertreasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("chainmailarmortreasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("clothing1treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("clothing2treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("clothing3treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("consumabletreasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("covenanttreasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("dagger1treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("dagger2treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("dagger3treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("deathtreasure"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("gemcodedisttable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("gemtreasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("healkittreasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("heritagehigharmor1treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("heritagehigharmor2treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("heritagehigharmor3treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("heritagelowarmor1treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("heritagelowarmor2treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("heritagelowarmor3treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("heritagetable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("itemtreasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("jewelrytreasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("leatherarmortreasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("lockpicktreasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("mace1treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("mace2treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("mace3treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("manastonetreasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("materialcodedisttable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("peatreasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("platemailarmor1treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("platemailarmor2treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("platemailarmor3treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("scrolltreasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("spear1treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("spear2treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("spear3treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("staff1treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("staff2treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("staff3treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("studdedleatherarmortreasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("sword1treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("sword2treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("sword3treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("treasureitemtable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("ua1treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("ua2treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("ua3treasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("wcidentries"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("weapontreasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("crossbowtreasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("spellleveltable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("magictreasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("mundanetreasuretable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("materialcolordist"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("gemclassbytier"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("spellcodedisttable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("masterspelllist"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("itembanespells"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("meleeweaponspells"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("missileweaponspells"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("casterweaponspells"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("cantripspertiertable"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("cantripmeleedist"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("cantripmissiledist"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("cantripcasterdist"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("cantriparmordist"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("cantripmagicaldist"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("spelllevelprogression"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("cantriplevelprogression"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("spelllevelchances"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("cantriplevelchances"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("materialceramic"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("materialcloth"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("materialgem"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("materialleather"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("materialmetal"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("materialstone"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("materialwood"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("workmanshipdist"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("gemclassvalues"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("qualitylevels"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("qualitymod"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("attackdefensequalmod"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("weaponspeedlevel"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("encumbrancemodchance"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("armorelementresistances"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("clothingpalette"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("bootarmorpalette"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("metalarmorpalette"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("spelldata"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("materialmod"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("mutatefilter"));
            treasureTables.Tables.Add(DataLoader.GetDataTable("wandstaffspells"));
            IsInitailized = true;
        }

        static internal double GetFloatByLookup(string tableName, string fieldName, int value, string returnField)
        {
            var dtRows = (from dRow in treasureTables.Tables[tableName].AsEnumerable()
                          where dRow.Field<int>(fieldName) == value
                          select dRow).First();

            return dtRows.Field<double>(returnField);
        }

        static internal int GetIntByLookup(string tableName, string fieldName, int value, string returnField)
        {
            var dtRows = (from dRow in treasureTables.Tables[tableName].AsEnumerable()
                          where dRow.Field<int>(fieldName) == value
                          select dRow).First();

            return dtRows.Field<int>(returnField);
        }

        static internal int GetIntByLookup(string tableName, string fieldName, string value, string returnField)
        {
            var dtRows = (from dRow in treasureTables.Tables[tableName].AsEnumerable()
                          where dRow.Field<string>(fieldName) == value
                          select dRow).First();

            return dtRows.Field<int>(returnField);
        }

        static internal int GetIntByLookupTwoParram(string tableName, string fieldOneName, string fieldTwoName, int valueOne, int valueTwo, string returnField)
        {
            var dtRows = (from dRow in treasureTables.Tables[tableName].AsEnumerable()
                          where dRow.Field<int>(fieldOneName) == valueOne && dRow.Field<int>(fieldTwoName) == valueTwo
                          select dRow).First();

            return dtRows.Field<int>(returnField);
        }

        static internal bool EntryLookupByIntIsValid(string tableName, string fieldName, int value)
        {
            try
            {
                int lookupRow = GetIntByLookup(tableName, fieldName, value, fieldName);
            }
            catch
            {
                return false;
            }
            return true;
        }

        static public DataTable GetDataTable(string tableName)
        {
            if(treasureTables.Tables.Contains(tableName))
                return treasureTables.Tables[tableName];
            return null;
        }

        static public List<DataRow> GetDataRowsByIntLookup(string tableName, string fieldName, int value)
        {
            List<DataRow> drs = (from trows in treasureTables.Tables[tableName].AsEnumerable()
                                 where trows.Field<int>(fieldName) == value
                                 select trows).ToList<DataRow>();

            return drs;
        }

        static public int GetIntValueByChance(string tableName, string fieldName, int value, string returnField, double luckValue)
        {
            int retVal = 0;
            var dtRows =  (from trows in treasureTables.Tables[tableName].AsEnumerable()
                            where trows.Field<int>(fieldName) == value
                            select trows).ToList<DataRow>();
            double luck = random.NextDouble() + luckValue;
            luck = Utils.Clamp(luck, 0.0, 1.0);
            double luckCounter = 0;
            for (int x = 0; x < dtRows.Count(); x++)
            {
                luckCounter += Convert.ToDouble(dtRows[x]["chance"].ToString());
                if (luck <= luckCounter)
                {
                    retVal = Convert.ToInt32(dtRows[x][returnField].ToString());
                    break;
                }
            }
            return retVal;
        }

        static public int GetIntValueByChanceTwoParam(string tableName, string fieldOneName, string fieldTwoName, int valueOne, int valueTwo, string returnField, double luckValue)
        {
            int retVal = 0;
            var dtRows = (from trows in treasureTables.Tables[tableName].AsEnumerable()
                          where trows.Field<int>(fieldOneName) == valueOne && trows.Field<int>(fieldTwoName) == valueTwo
                          select trows).ToList<DataRow>();
            double luck = random.NextDouble() + luckValue;
            luck = Utils.Clamp(luck, 0.0, 1.0);
            double luckCounter = 0;
            for (int x = 0; x < dtRows.Count(); x++)
            {
                luckCounter += Convert.ToDouble(dtRows[x]["chance"].ToString());
                if (luck <= luckCounter)
                {
                    retVal = Convert.ToInt32(dtRows[x][returnField].ToString());
                    break;
                }
            }
            return retVal;
        }

        static public List<DataRow> GetSpellDataByTier(string tableName, int tier)
        {
            return (from trows in treasureTables.Tables[tableName].AsEnumerable()
                    where trows.Field<int>("tier") == tier
                    select trows).ToList<DataRow>();
        }

        static public DataRow GetRowByIntLookup(string tableName, string lookupField, int lookupValue)
        {
            return (from trows in treasureTables.Tables[tableName].AsEnumerable()
                    where trows.Field<int>(lookupField) == lookupValue
                    select trows).FirstOrDefault();
        }

        static internal string GetStringByLookup(string tableName, string fieldName, int value, string returnField)
        {
            var dtRows = (from dRow in treasureTables.Tables[tableName].AsEnumerable()
                          where dRow.Field<int>(fieldName) == value
                          select dRow).First();

            return dtRows.Field<string>(returnField);
        }
    }
}
