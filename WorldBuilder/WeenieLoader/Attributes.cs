using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weenies.Types
{
    [Serializable]
    public class Attributes
    {
        [JsonProperty("coordination")]
        public StatBase coordination { get; set; }
        [JsonProperty("endurance")]
        public StatBase endurance { get; set; }
        [JsonProperty("focus")]
        public StatBase focus { get; set; }
        [JsonProperty("health")]
        public StatVolitile health { get; set; }
        [JsonProperty("mana")]
        public StatVolitile mana { get; set; }
        [JsonProperty("quickness")]
        public StatBase quickness { get; set; }
        [JsonProperty("self")]
        public StatBase self { get; set; }
        [JsonProperty("stamina")]
        public StatVolitile stamina { get; set; }
        [JsonProperty("strength")]
        public StatBase strength { get; set; }
    }
}
