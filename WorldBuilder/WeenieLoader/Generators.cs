﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Weenies.Types
{
    [Serializable]
    public partial class GeneratorTable
    {
        [JsonProperty("delay")]
        public long Delay { get; set; }

        [JsonProperty("frame")]
        public Frame Frame { get; set; }

        [JsonProperty("initCreate")]
        public long InitCreate { get; set; }

        [JsonProperty("maxNum")]
        public long MaxNum { get; set; }

        [JsonProperty("objcell_id")]
        public long ObjcellId { get; set; }

        [JsonProperty("probability")]
        public double Probability { get; set; }

        [JsonProperty("ptid")]
        public long Ptid { get; set; }

        [JsonProperty("shade")]
        public long Shade { get; set; }

        [JsonProperty("slot")]
        public long Slot { get; set; }

        [JsonProperty("stackSize")]
        public long StackSize { get; set; }

        [JsonProperty("type")]
        public long Type { get; set; }

        [JsonProperty("whenCreate")]
        public long WhenCreate { get; set; }

        [JsonProperty("whereCreate")]
        public long WhereCreate { get; set; }
    }

   
}
