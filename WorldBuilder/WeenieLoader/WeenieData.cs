﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weenies;

namespace WeenieLoader
{
    public class WeenieData
    {
        public static Dictionary<long, Weenie> Weenies = new Dictionary<long, Weenie>();
        public static List<string> ErrorWeenies = new List<string>();
 
        public WeenieData(string PathToWeenies)
        {
            if (string.IsNullOrEmpty(PathToWeenies) || !Directory.Exists(PathToWeenies))
                throw new NullReferenceException("Path to weenies is not valid");

            ParseWeenies(PathToWeenies);
        }

        private void ParseWeenies(string PathToWeenies)
        {
            var Files = Directory.EnumerateFiles(PathToWeenies, "*.json");
            foreach (string f in Files)
            {
                try
                {
                    string contents = File.ReadAllText(f);
                    var data = Weenie.FromJson(contents);
                    Weenies.Add(data.Wcid, data);
                }
                catch
                {
                    ErrorWeenies.Add(f);
                }
            }

        }
    }
}
