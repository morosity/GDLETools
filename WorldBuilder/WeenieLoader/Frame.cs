﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Weenies.Types
{
    [Serializable]
    public partial class Frame
    {
        [JsonProperty("angles")]
        public Angles Angles { get; set; }

        [JsonProperty("origin")]
        public Origin Origin { get; set; }
    }

    [Serializable]
    public partial class Angles
    {
        [JsonProperty("w")]
        public double W { get; set; }

        [JsonProperty("x")]
        public long X { get; set; }

        [JsonProperty("y")]
        public long Y { get; set; }

        [JsonProperty("z")]
        public double Z { get; set; }
    }

    [Serializable]
    public partial class Origin
    {
        [JsonProperty("x")]
        public long X { get; set; }

        [JsonProperty("y")]
        public long Y { get; set; }

        [JsonProperty("z")]
        public long Z { get; set; }
    }
}
