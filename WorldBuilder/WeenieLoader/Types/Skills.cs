using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weenies.Types
{
    public partial class Skill
    {
        [JsonProperty("key")]
        public long Key { get; set; }

        [JsonProperty("value")]
        public SkillValue Value { get; set; }
    }

    public partial class SkillValue
    {
        [JsonProperty("init_level")]
        public long InitLevel { get; set; }

        [JsonProperty("last_used_time")]
        public double LastUsedTime { get; set; }

        [JsonProperty("level_from_pp")]
        public long LevelFromPp { get; set; }

        [JsonProperty("pp")]
        public long Pp { get; set; }

        [JsonProperty("resistance_of_last_check")]
        public long ResistanceOfLastCheck { get; set; }

        [JsonProperty("sac")]
        public long Sac { get; set; }
    }
}
