using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weenies.Types
{
    public partial class EmoteTable
    {
        [JsonProperty("key")]
        public long Key { get; set; }

        [JsonProperty("value")]
        public List<EmoteTableValue> Value { get; set; }
    }

    public partial class EmoteTableValue
    {
        [JsonProperty("category")]
        public long Category { get; set; }

        [JsonProperty("emotes")]
        public List<Emote> Emotes { get; set; }

        [JsonProperty("quest")]
        public string Quest { get; set; }

        [JsonProperty("style")]
        public long? Style { get; set; }

        [JsonProperty("substyle")]
        public long? Substyle { get; set; }

        [JsonProperty("classID")]
        public long? ClassId { get; set; }

        [JsonProperty("probability")]
        public long Probability { get; set; }
    }

    public partial class Emote
    {
        [JsonProperty("delay")]
        public long Delay { get; set; }

        [JsonProperty("extent")]
        public long Extent { get; set; }

        [JsonProperty("msg")]
        public string Msg { get; set; }

        [JsonProperty("type")]
        public long Type { get; set; }

        [JsonProperty("spellid")]
        public long? Spellid { get; set; }

        [JsonProperty("motion")]
        public long? Motion { get; set; }

        [JsonProperty("amount")]
        public long? Amount { get; set; }

        [JsonProperty("cprof")]
        public CreateList Cprof { get; set; }

        [JsonProperty("frame")]
        public Frame Frame { get; set; }

        [JsonProperty("max")]
        public long? Max { get; set; }

        [JsonProperty("min")]
        public long? Min { get; set; }

        [JsonProperty("stat")]
        public long? Stat { get; set; }

        [JsonProperty("mPosition")]
        public MPosition MPosition { get; set; }

        [JsonProperty("fmax")]
        public double? Fmax { get; set; }

        [JsonProperty("fmin")]
        public double? Fmin { get; set; }

        [JsonProperty("teststring")]
        public string Teststring { get; set; }

        [JsonProperty("sound")]
        public long? Sound { get; set; }

        [JsonProperty("pscript")]
        public long? Pscript { get; set; }

    }
}
