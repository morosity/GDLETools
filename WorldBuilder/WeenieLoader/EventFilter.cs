using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weenies.Types
{
    [Serializable]
    public class EventFilter
    {
        [JsonProperty("events")]
        public List<long> Events { get; set; }
    }
}
