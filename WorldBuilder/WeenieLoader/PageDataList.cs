﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Weenies.Types
{
    [Serializable]
    public partial class PageDataList
    {
        [JsonProperty("maxNumCharsPerPage")]
        public long MaxNumCharsPerPage { get; set; }

        [JsonProperty("maxNumPages")]
        public long MaxNumPages { get; set; }

        [JsonProperty("pages")]
        public Page[] Pages { get; set; }
    }

    [Serializable]
    public partial class Page
    {
        [JsonProperty("authorAccount")]
        public string AuthorAccount { get; set; }

        [JsonProperty("authorID")]
        public long AuthorId { get; set; }

        [JsonProperty("authorName")]
        public string AuthorName { get; set; }

        [JsonProperty("ignoreAuthor")]
        public long IgnoreAuthor { get; set; }

        [JsonProperty("pageText")]
        public string PageText { get; set; }
    }
}
