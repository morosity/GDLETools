﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Weenies.Types
{
    [Serializable]
    public partial class PosStat
    {
        [JsonProperty("key")]
        public long Key { get; set; }

        [JsonProperty("value")]
        public MPosition Value { get; set; }
    }
}
