using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weenies.Types
{
    [Serializable]
    public class Acache
    {
        [JsonProperty("armor_vs_acid")]
        public long ArmorVsAcid { get; set; }

        [JsonProperty("armor_vs_bludgeon")]
        public long ArmorVsBludgeon { get; set; }

        [JsonProperty("armor_vs_cold")]
        public long ArmorVsCold { get; set; }

        [JsonProperty("armor_vs_electric")]
        public long ArmorVsElectric { get; set; }

        [JsonProperty("armor_vs_fire")]
        public long ArmorVsFire { get; set; }

        [JsonProperty("armor_vs_nether")]
        public long ArmorVsNether { get; set; }

        [JsonProperty("armor_vs_pierce")]
        public long ArmorVsPierce { get; set; }

        [JsonProperty("armor_vs_slash")]
        public long ArmorVsSlash { get; set; }

        [JsonProperty("base_armor")]
        public long BaseArmor { get; set; }
    }

    [Serializable]
    public class Bpsd
    {
        [JsonProperty("HLB")]
        public double Hlb { get; set; }

        [JsonProperty("HLF")]
        public double Hlf { get; set; }

        [JsonProperty("HRB")]
        public double Hrb { get; set; }

        [JsonProperty("HRF")]
        public double Hrf { get; set; }

        [JsonProperty("LLB")]
        public double Llb { get; set; }

        [JsonProperty("LLF")]
        public double Llf { get; set; }

        [JsonProperty("LRB")]
        public double Lrb { get; set; }

        [JsonProperty("LRF")]
        public double Lrf { get; set; }

        [JsonProperty("MLB")]
        public double Mlb { get; set; }

        [JsonProperty("MLF")]
        public double Mlf { get; set; }

        [JsonProperty("MRB")]
        public double Mrb { get; set; }

        [JsonProperty("MRF")]
        public double Mrf { get; set; }
    }

    [Serializable]
    public class BodyPartTable
    {
        [JsonProperty("key")]
        public long Key { get; set; }

        [JsonProperty("value")]
        public BodyPartTableValue Value { get; set; }
    }

    [Serializable]
    public class Body
    {
        [JsonProperty("body_part_table")]
        public List<BodyPartTable> BodyPartTable { get; set; }
    }

    [Serializable]
    public partial class BodyPartTableValue
    {
        [JsonProperty("acache")]
        public Acache Acache { get; set; }

        [JsonProperty("bh")]
        public long Bh { get; set; }

        [JsonProperty("bpsd")]
        public Bpsd Bpsd { get; set; }

        [JsonProperty("dtype")]
        public long Dtype { get; set; }

        [JsonProperty("dval")]
        public long Dval { get; set; }

        [JsonProperty("dvar")]
        public double Dvar { get; set; }
    }
}
