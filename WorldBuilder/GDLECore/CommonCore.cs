﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public static class CommonCore
    {
        public static int ByteAlign(int len)
        {
            int extra = len % 4;
            if (extra == 1)
                return 3;
            if (extra == 2)
                return 2;
            if (extra == 3)
                return 1;
            return extra;
        }
    }
}
