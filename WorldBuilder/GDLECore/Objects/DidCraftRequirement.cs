﻿using GDLECore.Enums;
using GDLECore.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore.Objects
{
    public class DidCraftRequirement : IPackable<DidCraftRequirement>
    {
        public PropertyDID Property;
        public int Value;
        public uint Operation;  // TODO: Change to enum
        public string Message;

        public void Pack(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public DidCraftRequirement Unpack(BinaryReader reader)
        {
            Property = (PropertyDID)reader.ReadUInt32();
            Value = reader.ReadInt32();
            Operation = reader.ReadUInt32();
            Message = reader.ReadGDLEString();

            return this;
        }
    }
}
