﻿using GDLECore.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore.Objects
{
    public class SpellBookPage : IPackable<SpellBookPage>
    {
        public float CastingLikelihood { get; private set; }

        public SpellBookPage()
        { }

        public SpellBookPage Unpack(BinaryReader reader)
        {
            CastingLikelihood = reader.ReadSingle() - 2.0f;
            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write(CastingLikelihood + 2.0f);
        }
    }
}
