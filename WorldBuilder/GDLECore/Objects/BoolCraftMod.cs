﻿using GDLECore.Enums;
using GDLECore.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore.Objects
{
    public class BoolCraftMod : IPackable<BoolCraftMod>
    {
        public int UnknownInt;
        public int Operation; // TODO: Make enum
        public PropertyBool Property;
        public bool Value;

        public void Pack(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public BoolCraftMod Unpack(BinaryReader reader)
        {
            UnknownInt = reader.ReadInt32();
            Operation = reader.ReadInt32();
            Property = (PropertyBool)reader.ReadInt32();
            Value = Convert.ToBoolean(reader.ReadInt32());
            return this;
        }
    }
}
