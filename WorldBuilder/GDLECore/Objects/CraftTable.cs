﻿using GDLECore.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore.Objects
{
    public class CraftTable : IPackable<CraftTable>
    {
        public Dictionary<uint, CraftOperation> Recipe = new Dictionary<uint, CraftOperation>();
        public List<Precursor> Precursors = new List<Precursor>();

        public void Pack(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public CraftTable Unpack(BinaryReader reader)
        {
            int recipeCount = reader.ReadUInt16();
            int dummyValue = reader.ReadUInt16();
            for (int x = 0; x < recipeCount; x++)
            {
                Recipe.Add(reader.ReadUInt32(), new CraftOperation().Unpack(reader));
            }
            int precursorCount = reader.ReadInt32();
            for (int x = 0; x < precursorCount; x++)
            {
                Precursors.Add(new Precursor()
                {
                    Target = reader.ReadUInt32(),
                    Tool = reader.ReadUInt32(),
                    RecipeID = reader.ReadUInt32()
                });
            }
            return this;
        }
    }
}
