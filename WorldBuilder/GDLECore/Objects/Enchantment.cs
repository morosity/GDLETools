﻿using System.IO;
using GDLECore.Interfaces;

namespace GDLECore.Objects
{
    public class Enchantment : IPackable<Enchantment>
    {
        public uint ID { get; private set; }
        public uint SpellCategory { get; private set; }
        public uint PowerLevel { get; private set; }
        public double StartTime { get; private set; }
        public double Duration { get; private set; }
        public uint CasterId { get; private set; }
        public float DegradeMod { get; private set; }
        public float DegradeLimit { get; private set; }
        public double LastTimeDegraded { get; private set; }
        public uint SpellSet { get; private set; }
        public StatMod StatModifiers { get; private set; }

        public Enchantment()
        { }

        public Enchantment Unpack(BinaryReader reader)
        {
            ID = reader.ReadUInt32();
            SpellCategory = (reader.ReadUInt32() & 0xFFFF);
            PowerLevel = reader.ReadUInt32();
            StartTime = reader.ReadDouble();
            Duration = reader.ReadDouble();
            CasterId = reader.ReadUInt32();
            DegradeMod = reader.ReadSingle();
            DegradeLimit = reader.ReadSingle();
            LastTimeDegraded = reader.ReadDouble();
            StatModifiers = new StatMod().Unpack(reader);
            long currPos = reader.BaseStream.Position;
            uint spellSetPeek = reader.ReadUInt32();
            if (spellSetPeek < 6)
                SpellSet = spellSetPeek;
            else
                reader.BaseStream.Position = currPos;

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write(ID);
            writer.Write(SpellCategory);
            writer.Write(PowerLevel);
            writer.Write(StartTime);
            writer.Write(Duration);
            writer.Write(CasterId);
            writer.Write(DegradeMod);
            writer.Write(DegradeLimit);
            writer.Write(LastTimeDegraded);
            StatModifiers.Pack(writer);
            writer.Write(SpellSet);

        }
    }
}
