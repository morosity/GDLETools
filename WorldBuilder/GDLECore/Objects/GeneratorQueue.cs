﻿using GDLECore.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore.Objects
{
    public class GeneratorQueue
    {
        public List<GeneratorQueueNode> Nodes { get; private set; }

        public GeneratorQueue()
        {
            Nodes = new List<GeneratorQueueNode>();
        }

        public GeneratorQueue Unpack(BinaryReader reader)
        {
            ushort numGenQueue = reader.ReadUInt16();
            ushort sizeGenQueue = reader.ReadUInt16();

            for (int gq = 0; gq < numGenQueue; gq++)
            {
                Nodes.Add(new GeneratorQueueNode().Unpack(reader));
            }

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write((ushort)Nodes.Count);
            writer.Write((ushort)16);

            foreach (GeneratorQueueNode gqn in Nodes)
                gqn.Pack(writer);
        }

    }
}
