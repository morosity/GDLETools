﻿using GDLECore.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore.Objects
{
    public class Position : IPackable<Position>
    {
        public uint ObjectCellId { get; private set; }
        public Frame Frame { get; private set; }

        public Position()
        { }

        public Position Unpack(BinaryReader reader)
        {
            ObjectCellId = reader.ReadUInt32();
            Frame = new Frame().Unpack(reader);

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write(ObjectCellId);
            Frame.Pack(writer);
        }
    }
}
