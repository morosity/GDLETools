﻿using System.Collections.Generic;
using System.IO;
using GDLECore.Interfaces;

namespace GDLECore.Objects
{
    public class EventFilter : IPackable<EventFilter>
    {
        public List<uint> Events { get; private set; }

        public EventFilter()
        {
            Events = new List<uint>();
        }

        public EventFilter Unpack(BinaryReader reader)
        {
            uint numEvents = reader.ReadUInt32();

            for (int ne = 0; ne < numEvents; ne++)
            {
                Events.Add(reader.ReadUInt32());
            }
            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write(Events.Count);
            foreach (uint eve in Events)
            {
                writer.Write(eve);
            }
        }
    }
}
