﻿using GDLECore.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore.Objects
{
    public class Weenie : IPackable<Weenie>
    {
        public uint WeenieID { get; private set; }
        public uint SaveVersion { get; private set; }
        public uint SaveTimestamp { get; private set; }
        public uint SaveInstance { get; private set; }

        public Qualities Qualities { get; private set; }

        public ObjDesc ObjectDescription { get; private set; }

        public ObjDesc WornObjectDescription { get; private set; }

        public uint PlayerFlags { get; private set; }

        public PlayerModule PlayerModule { get; private set; }

        private List<uint> equipmentIds { get; set; }
        public Dictionary<uint, Weenie> Equipment { get; private set; }

        private List<uint> inventoryIds { get; set; }
        public Dictionary<uint, Weenie> Inventory { get; private set; }

        private List<uint> packIds { get; set; }
        public Dictionary<uint, Weenie> Packs { get; private set; }

        public List<Quest> Quests { get; private set; }

        public Weenie(uint id)
        {
            WeenieID = id;
            equipmentIds = new List<uint>();
            packIds = new List<uint>();
            inventoryIds = new List<uint>();
            Quests = new List<Quest>();
            Inventory = new Dictionary<uint, Weenie>();
            Equipment = new Dictionary<uint, Weenie>();
            Packs = new Dictionary<uint, Weenie>();
        }

        public void LoadInventory(uint invId, BinaryReader invReader)
        {
            if (equipmentIds.Contains(invId))
            {
                if (!Equipment.ContainsKey(invId))
                    Equipment.Add(invId, new Weenie(invId).Unpack(invReader));
            }
            if (inventoryIds.Contains(invId))
            {
                if (!Inventory.ContainsKey(invId))
                    Inventory.Add(invId, new Weenie(invId).Unpack(invReader));
            }
            if (packIds.Contains(invId))
            {
                if(!Packs.ContainsKey(invId))
                    Packs.Add(invId, new Weenie(invId).Unpack(invReader));
            }
        }

        public void LoadPacks(DataTable dataTable)
        {
            foreach (uint pId in packIds)
            {
                Weenie currentPack = Packs[pId];
                foreach (DataRow dr in dataTable.Rows)
                {
                    byte[] invItem = (byte[])dr[1];
                    using (BinaryReader ibr = new BinaryReader(new MemoryStream(invItem)))
                    {
                        currentPack.LoadInventory(Convert.ToUInt32(dr[0].ToString().Trim()), ibr);
                    }
                }
            }
        }

        public Weenie Unpack(BinaryReader reader)
        {
            SaveVersion = reader.ReadUInt32();
            SaveTimestamp = reader.ReadUInt32();
            SaveInstance = reader.ReadUInt32();

            Qualities = new Qualities().Unpack(reader);
            ObjectDescription = new ObjDesc().Unpack(reader);
            WornObjectDescription = new ObjDesc().Unpack(reader);

            uint weenieFlags = reader.ReadUInt32();
            if ((weenieFlags & 1) != 0)
            {
                PlayerModule = new PlayerModule().Unpack(reader);
            }

            if ((weenieFlags & 2) != 0)
            {
                // Read equipids
                uint numItemsEquiped = reader.ReadUInt32();
                for (int eq = 0; eq < numItemsEquiped; eq++)
                {
                    equipmentIds.Add(reader.ReadUInt32());
                }
                // Read inventoryids
                uint numItemsInventory = reader.ReadUInt32();
                for (int i = 0; i < numItemsInventory; i++)
                {
                    inventoryIds.Add(reader.ReadUInt32());
                }
                // Read packids
                uint numPacks = reader.ReadUInt32();
                for (int p = 0; p < numPacks; p++)
                {
                    packIds.Add(reader.ReadUInt32());
                }
            }

            if ((weenieFlags & 4) != 0)
            {
                // Read Quests
                ushort numQuests = reader.ReadUInt16();
                ushort sizeQuests = reader.ReadUInt16();

                for (int q = 0; q < numQuests; q++)
                {
                    Quests.Add(new Quest().Unpack(reader));
                }
            }

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write(SaveVersion);
            writer.Write(SaveTimestamp);
            writer.Write(SaveInstance);

            Qualities.Pack(writer);
            ObjectDescription.Pack(writer);
            WornObjectDescription.Pack(writer);

            int weenieFlags = 0;
            if (PlayerModule != null)
                weenieFlags |= 1;
            if (Equipment.Count > 0 || Inventory.Count > 0 || Packs.Count > 0)
                weenieFlags |= 2;
            if (Quests != null)
                weenieFlags |= 4;

            writer.Write(weenieFlags);

            if (PlayerModule != null)
            {
                PlayerModule.Pack(writer);
            }

            if (Equipment.Count > 0)
            {
                writer.Write(Equipment.Count);
                foreach (KeyValuePair<uint, Weenie> eqi in Equipment)
                    writer.Write(eqi.Key);
            }

            if (Inventory.Count > 0)
            {
                writer.Write(Inventory.Count);
                foreach (KeyValuePair<uint, Weenie> inv in Inventory)
                    writer.Write(inv.Key);
            }

            if (Packs.Count > 0)
            {
                writer.Write(Packs.Count);
                foreach (KeyValuePair<uint, Weenie> pac in Packs)
                    writer.Write(pac.Key);
            }

            if (Quests != null)
            {
                writer.Write((ushort)Quests.Count);
                writer.Write((ushort)16);

                foreach (Quest q in Quests)
                    q.Pack(writer);
            }
        }
    }
}
