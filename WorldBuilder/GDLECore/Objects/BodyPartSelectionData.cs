﻿using System.IO;
using GDLECore.Interfaces;

namespace GDLECore.Objects
{
    public class BodyPartSelectionData : IPackable<BodyPartSelectionData>
    {
        public float HLF { get; private set; }
        public float MLF { get; private set; }
        public float LLF { get; private set; }
        public float HRF { get; private set; }
        public float MRF { get; private set; }
        public float LRF { get; private set; }
        public float HLB { get; private set; }
        public float MLB { get; private set; }
        public float LLB { get; private set; }
        public float HRB { get; private set; }
        public float MRB { get; private set; }
        public float LRB { get; private set; }

        public BodyPartSelectionData()
        { }

        public BodyPartSelectionData Unpack(BinaryReader reader)
        {
            HLF = reader.ReadSingle();
            MLF = reader.ReadSingle();
            LLF = reader.ReadSingle();
            HRF = reader.ReadSingle();
            MRF = reader.ReadSingle();
            LRF = reader.ReadSingle();
            HLB = reader.ReadSingle();
            MLB = reader.ReadSingle();
            LLB = reader.ReadSingle();
            HRB = reader.ReadSingle();
            MRB = reader.ReadSingle();
            LRB = reader.ReadSingle();
            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write(HLF);
            writer.Write(MLF);
            writer.Write(LLF);
            writer.Write(HRF);
            writer.Write(MRF);
            writer.Write(LRF);
            writer.Write(HLB);
            writer.Write(MLB);
            writer.Write(LLB);
            writer.Write(HRB);
            writer.Write(MRB);
            writer.Write(LRB);
        }
    }
}
