﻿using GDLECore.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore.Objects
{
    public class ShortCutManager : IPackable<ShortCutManager>
    {
        private int MaxShortCuts = 18;
        public List<ShortCutData> ShortCuts { get; private set; }

        public ShortCutManager()
        { }

        public ShortCutManager Unpack(BinaryReader reader)
        {
            uint numShortCuts = reader.ReadUInt32();

            ShortCuts = new List<ShortCutData>(MaxShortCuts);

            if (numShortCuts > MaxShortCuts)
                numShortCuts = Convert.ToUInt32(MaxShortCuts);

            for (int sc = 0; sc < numShortCuts; sc++)
            {
                ShortCutData shortCutData = new ShortCutData().Unpack(reader);

                if (shortCutData.Index > 0 && shortCutData.Index < MaxShortCuts)
                {
                    if (!ShortCuts.Contains(shortCutData))
                        ShortCuts.Add(shortCutData);
                }
            }

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write(ShortCuts.Count);
            foreach (ShortCutData scd in ShortCuts)
                scd.Pack(writer);
        }
    }
}
