﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerrainToEncounter
{
    public class LandBlock
    {

        const float half_square_length_174 = 24.0f * 0.5f;
        public uint __thiscall get_terrain(uint cell_id, Vector3 vc)
        {
            LandBlock v3; // esi
            int v4; // ecx
            int v5; // edx
            double v6; // st6
            byte v8; // c0
            byte v9; // c3
            uint result; // eax
            int v11; // eax
            ushort[] v12; // edx
            int x = 0; // [esp+4h] [ebp-4h]
            int y = 0;

            v3 = this;
            LandDefs.gid_to_lcoord(cell_id, ref x, ref y);
            v4 = x % 8;
            v5 = (int)cell_id % 8;
            v6 = vc.y;
            if (((double)v4 + (double)v4 + 1.0) * half_square_length_174 >= vc.x)))
            {
                v11 = v5 + 8 * v4;
                v12 = v3.terrain;
                if ((v8 | v9) != 0)
                {
                    result = ((uint)LOBYTE(v12[v11 + v4]) >> 2) & 0x1F;
                }
                else
                {
                    result = ((uint)LOBYTE(v12[v11 + 1 + v4]) >> 2) & 0x1F;
                }
            }
            else if (v8 | v9)
            {
                result = ((uint)LOBYTE(v3.terrain[v5 + 8 * (v4 + 1) + v4 + 1]) >> 2) & 0x1F;
            }
            else
            {
                result = ((uint)LOBYTE(v3.terrain[8 * v4 + 10 + v5 + v4]) >> 2) & 0x1F;
            }
            return result;
        }

    }
}
