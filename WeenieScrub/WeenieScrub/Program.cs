﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.IO;

namespace WeenieScrub
{
    class Program
    {
        static void Main(string[] args)
        {
            ConnectTest();
        }

        static void ConnectTest()
        {
            SQLiteConnection conn;
            SQLiteCommand cmd;
            SQLiteDataReader dataReader;

            conn = new SQLiteConnection("Data Source=_solclaim.db;Version=3;New=True;");
            conn.Open();

            cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT ObjectName, ObjectClass, SerializedData FROM ObjectData";

            dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
            {
                Console.WriteLine(dataReader[0].ToString());
                uint objClass = UInt32.Parse(dataReader[1].ToString());
                byte[] objData = new byte[dataReader.GetBytes(2, 0, null, 0, int.MaxValue) - 1];
                dataReader.GetBytes(2, 0, objData, 0, objData.Length);
                vgiweenie wtojson = MakeVgiWeenie(objData);
                wtojson.ObjectClass = objClass;
            }


        }

        private static vgiweenie MakeVgiWeenie(byte[] objData)
        {
            BinaryReader reader = new BinaryReader(new MemoryStream(objData));

            vgiweenie weenie = new vgiweenie();
            int intp = reader.ReadInt32();
            for (int index = 0; index < intp; ++index)
                weenie.intProperties[(uint)reader.ReadInt32()] = (uint)reader.ReadInt32();
            int numStringProp = reader.ReadInt32();
            for (int index = 0; index < numStringProp; ++index)
                weenie.stringProperties[(uint)reader.ReadInt32()] = GetArrayAsString(reader);
            int numBoolProp = reader.ReadInt32();
            for (int index = 0; index < numBoolProp; ++index)
                weenie.boolProperties[(uint)reader.ReadInt32()] = reader.ReadByte() != 0;
            int numDoubleProp = reader.ReadInt32();
            for (int index = 0; index < numDoubleProp; ++index)
                weenie.doubleProperties[(uint)reader.ReadInt32()] = reader.ReadDouble();
            int numInt64Prop = reader.ReadInt32();
            for (int index = 0; index < numInt64Prop; ++index)
                weenie.int64Properties[(uint)reader.ReadInt32()] = (ulong)reader.ReadInt64();
            int numSpells = reader.ReadInt32();
            for (int index = 0; index < numSpells; ++index)
                weenie.Spells.Add((uint)reader.ReadInt32());
            try
            {
                weenie.unk = reader.ReadUInt32();
            }
            catch
            { }

            return weenie;
        }

        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        public static string GetArrayAsString(BinaryReader reader)
        {
            int count = (int)GetStringLengthFromStream(reader);
            byte[] numArray = new byte[count];
            reader.Read(numArray, 0, count);
            return new ASCIIEncoding().GetString(numArray);
        }

        public static uint GetStringLengthFromStream(BinaryReader reader)
        {
            byte firstByte = reader.ReadByte();
            byte secondByte = 0;
            byte thirdByte = 0;
            byte fourthByte = 0;
            bool flag1 = false;
            bool flag2 = false;
            bool flag3 = false;
            if (((int)firstByte & 128) > 0)
            {
                secondByte = reader.ReadByte();
                flag1 = true;
            }
            if (((int)secondByte & 128) > 0)
            {
                thirdByte = reader.ReadByte();
                flag2 = true;
            }
            if (((int)thirdByte & 128) > 0)
            {
                fourthByte = reader.ReadByte();
                flag3 = true;
            }
            byte num5 = (byte)((uint)firstByte & (uint)sbyte.MaxValue);
            byte num6 = (byte)((uint)secondByte & (uint)sbyte.MaxValue);
            byte num7 = (byte)((uint)thirdByte & (uint)sbyte.MaxValue);
            uint num8 = 0;
            if (flag3)
                num8 = (uint)fourthByte << 7;
            if (flag2)
                num8 = (num8 | (uint)num7) << 7;
            if (flag1)
                num8 = (num8 | (uint)num6) << 7;
            return num8 | (uint)num5;
        }

    }
}
