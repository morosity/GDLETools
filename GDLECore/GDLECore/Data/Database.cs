﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore.Data
{
    public class Database
    {
        public static class DBUtils
        {
            private static MySqlConnection _conn = null;
            private static string dbserver = string.Empty;
            private static string dbport = string.Empty;
            private static string dbuser = string.Empty;
            private static string dbpass = string.Empty;
            private static string dbname = string.Empty;
            private static string connString = string.Empty;
            private static bool isSetup = false;

            public static void SetupConnection(string server, string port, string user, string pass, string databasename)
            {
                if (string.IsNullOrEmpty(server) || string.IsNullOrEmpty(port) || string.IsNullOrEmpty(user) || string.IsNullOrEmpty(databasename))
                    throw new ArgumentNullException("Check server name, port, user, pass, and name");

                dbserver = server.Trim();
                dbport = port.Trim();
                dbuser = user.Trim();
                dbpass = pass.Trim();
                dbname = databasename.Trim();

                CreateConnectionString();

                _conn = new MySqlConnection(connString);
                _conn.Open();
                isSetup = true;
            }

            public static void CloseDB()
            {
                if (_conn.State == ConnectionState.Open)
                    _conn.Close();
            }

            public static void OpenDB()
            {
                if (_conn.State != ConnectionState.Open)
                    _conn.Open();
            }

            public static Dictionary<uint, string> GetCharIdsAndNames(string connectionString = null)
            {
                Dictionary<uint, string> retVal = new Dictionary<uint, string>();

                if (!isSetup)
                    return retVal;
                
                DataTable dt = new DataTable();
                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    conn.Open();
                    using (MySqlCommand command = new MySqlCommand(Globals.GetIdAndNamesOfChars, conn))
                    {
                        dt.Load(command.ExecuteReader());
                    }
                }
                foreach (DataRow dr in dt.Rows)
                {
                    uint rowId = UInt32.Parse(dr[0].ToString().Trim());
                    string charName = dr[1].ToString().Trim();
                    retVal.Add(rowId, charName);
                }
                return retVal;
            }

            public static List<UInt32> GetHouses(string connectionString = null)
            {
                DataTable charData = new DataTable();

                if (!isSetup)
                    return new List<UInt32>();

                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    conn.Open();

                    using (MySqlCommand command = new MySqlCommand(Globals.GetHouses, conn))
                    {
                        charData.Load(command.ExecuteReader(CommandBehavior.SequentialAccess));
                    }
                }
                return charData.GetListFromColumn<UInt32>("house_id", true);
            }

            public static List<UInt32> GetCharRange(uint start, uint end, string connectionString = null)
            {
                DataTable charData = new DataTable();

                if (!isSetup)
                    return new List<UInt32>();

                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    conn.Open();

                    using (MySqlCommand command = new MySqlCommand(string.Format(Globals.GetCharsInRange, start, end), conn))
                    {
                        charData.Load(command.ExecuteReader(CommandBehavior.SequentialAccess));
                    }
                }
                return charData.GetListFromColumn<UInt32>("weenie_id", false);
            }

            public static uint GetLatestLogin(uint accountId, string connectionString = null)
            {
                if (!isSetup)
                    return 0;

                uint retval = 0;

                DataTable tempDt = new DataTable();
                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    conn.Open();
                    using (MySqlCommand command = new MySqlCommand(string.Format(Globals.GetLatestLoginByAccount, accountId), conn))
                    {
                        tempDt.Load(command.ExecuteReader(CommandBehavior.SequentialAccess));
                    }
                }

                if(tempDt.Rows.Count > 0)
                    retval = Convert.ToUInt32(tempDt.Rows[0][0].ToString().Trim());
                
                return retval;
            }

            public static HouseData GetHouseData(UInt32 houseId, string connectionString = null)
            {
                if (!isSetup)
                    return null;

                DataTable tempDT = new DataTable();
                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    conn.Open();

                    using (MySqlCommand command = new MySqlCommand(string.Format(Globals.GetHouseData, houseId), conn))
                    {
                        tempDT.Load(command.ExecuteReader(CommandBehavior.SequentialAccess));
                    }
                }

                if (tempDT.Rows.Count == 0)
                    return null;

                Byte[] charData = (byte[])tempDT.Rows[0][0];

                HouseData weenie;
                try
                {
                    using (BinaryReader br = new BinaryReader(new MemoryStream(charData)))
                    {
                        weenie = new HouseData().Unpack(br);
                    }
                }
                catch
                {
                    return null;
                }

                return weenie;
            }

            public static List<uint> GetThemAll(uint character, string connectionString = null)
            {
                DataTable charData = new DataTable();
                charData.Columns.Add("id", typeof(UInt32));
                if (!isSetup)
                    return new List<UInt32>();

                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    conn.Open();

                    using (MySqlCommand command = new MySqlCommand(string.Format(Globals.GetTheWholeShebang, character), conn))
                    {
                        charData.Load(command.ExecuteReader(CommandBehavior.SequentialAccess));
                    }
                }
                return charData.GetListFromColumn<UInt32>("id", false);
            }

            public static DataTable GetCharacterData(uint characterid, string connectionString = null)
            {
                DataTable charData = new DataTable();
                charData.Columns.Add("Id", typeof(UInt32));
                charData.Columns.Add("Data", typeof(byte[]));
                if (!isSetup)
                    return charData;
                
                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    conn.Open();

                    using (MySqlCommand command = new MySqlCommand(string.Format(Globals.GetCharacterData, characterid), conn))
                    {
                        charData.Load(command.ExecuteReader(CommandBehavior.SequentialAccess));
                    }
                }
                return charData;
            }

            public static DataTable GetCharInventory(uint characterid, string connectionString = null)
            {
                DataTable inventory = new DataTable();

                inventory.Columns.Add("Id", typeof(UInt32));
                inventory.Columns.Add("Data", typeof(byte[]));

                if (!isSetup)
                    return inventory;

                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    conn.Open();
                    using (MySqlCommand command = new MySqlCommand(string.Format(Globals.GetCharInventory, characterid), conn))
                    {
                        inventory.Load(command.ExecuteReader(CommandBehavior.SequentialAccess));
                    }
                }

                return inventory;
            }

            public static List<uint> GetCharInventoryIds(uint characterid, string connectionString = null)
            {
                List<uint> retVal = new List<uint>();
                DataTable inventory = new DataTable();

                inventory.Columns.Add("Id", typeof(UInt32));

                if (!isSetup)
                    return retVal;

                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    conn.Open();
                    using (MySqlCommand command = new MySqlCommand(string.Format(Globals.GetCharInventoryIDs, characterid), conn))
                    {
                        inventory.Load(command.ExecuteReader(CommandBehavior.SequentialAccess));
                        retVal = inventory.GetListFromColumn<uint>("id", true);
                    }
                }

                return retVal;
            }

            public static void DeleteWeenieById(uint weenieId, string connectionString = null)
            {
                if (!isSetup)
                    return;

                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    conn.Open();
                    using (MySqlCommand command = new MySqlCommand(string.Format(Globals.DeleteCharacterWeenie, weenieId), conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }

            public static void DeleteWeeniesByRange(string range, string connectionString = null)
            {
                if (!isSetup)
                    return;

                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    conn.Open();
                    using (MySqlCommand command = new MySqlCommand(string.Format(Globals.DeleteWeenieByRange, range), conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }

            public static void CopyToNewTable(string table, uint id, string connectionString = null)
            {
                if (!isSetup)
                    return;
                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    if(conn.State != ConnectionState.Open)
                        conn.Open();
                    using (MySqlCommand command = new MySqlCommand(string.Format(Globals.CopyWeenieRow, table, id), conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }

            public static List<uint> GetNonPlayerContainerWeenieList(string connectionString = null)
            {
                List<uint> retVal = new List<uint>();

                if (!isSetup)
                    return retVal;

                DataTable tempDt = new DataTable();
                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    conn.Open();
                    using (MySqlCommand command = new MySqlCommand(Globals.GetNonPlayerTopLevelWeenies, conn))
                    {
                        tempDt.Load(command.ExecuteReader(CommandBehavior.SequentialAccess));
                    }
                }

                foreach (DataRow dr in tempDt.Rows)
                {
                    retVal.Add(Convert.ToUInt32(dr[0].ToString().Trim()));
                }

                return retVal;
            }

            public static List<uint> GetNonPlayerTopLevelWeenieList(string connectionString = null)
            {
                List<uint> retVal = new List<uint>();

                if (!isSetup)
                    return retVal;

                DataTable tempDt = new DataTable();
                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    conn.Open();
                    using (MySqlCommand command = new MySqlCommand(Globals.GetNonPlayerTopLevelWeenie, conn))
                    {
                        tempDt.Load(command.ExecuteReader(CommandBehavior.SequentialAccess));
                    }
                }

                foreach (DataRow dr in tempDt.Rows)
                {
                    retVal.Add(Convert.ToUInt32(dr[0].ToString().Trim()));
                }

                return retVal;
            }

            public static List<uint> GetCurrentCorpses(string connectionString = null)
            {
                List<uint> retVal = new List<uint>();

                if (!isSetup)
                    return retVal;

                DataTable tempDT = new DataTable("corpsedata");
                DataColumn colInt32 = new DataColumn("id");
                colInt32.DataType = System.Type.GetType("System.UInt32");
                tempDT.Columns.Add(colInt32);
                
                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    conn.Open();
                    using (MySqlCommand command = new MySqlCommand(Globals.GetCorpseData, conn))
                    {
                        tempDT.Load(command.ExecuteReader(CommandBehavior.SequentialAccess));
                    }
                }

                retVal = tempDT.GetListFromColumn<uint>("id", true);
                return retVal;
            }

            public static List<uint> GetCurrentContainers(string connectionString = null)
            {
                List<uint> retVal = new List<uint>();

                if (!isSetup)
                    return retVal;

                DataTable tempDT = new DataTable("corpsedata");
                DataColumn colInt32 = new DataColumn("id");
                colInt32.DataType = System.Type.GetType("System.UInt32");
                tempDT.Columns.Add(colInt32);

                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    conn.Open();
                    using (MySqlCommand command = new MySqlCommand(Globals.GetNonCorpseContainers, conn))
                    {
                        tempDT.Load(command.ExecuteReader(CommandBehavior.SequentialAccess));
                    }
                }

                retVal = tempDT.GetListFromColumn<uint>("id", false);
                return retVal;
            }


            public static Byte[] GetWeenieDataBits(uint characterid, string connectionString = null)
            {
                if (!isSetup)
                    return null;
                                
                DataTable tempDT = new DataTable();
                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    conn.Open();

                    using (MySqlCommand command = new MySqlCommand(string.Format(Globals.GetWeenieData, characterid), conn))
                    {
                        tempDT.Load(command.ExecuteReader(CommandBehavior.SequentialAccess));
                    }
                }

                if (tempDT.Rows.Count == 0)
                    return null;

                 return (byte[])tempDT.Rows[0][0];
            }

            public static Weenie GetWeenieData(uint characterid, string connectionString = null)
            {
                if (!isSetup)
                    return null;

                DataTable tempDT = new DataTable();
                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    conn.Open();

                    using (MySqlCommand command = new MySqlCommand(string.Format(Globals.GetWeenieData, characterid), conn))
                    {
                        tempDT.Load(command.ExecuteReader(CommandBehavior.SequentialAccess));
                    }
                }

                if (tempDT.Rows.Count == 0)
                    return null;

                Byte[] charData = (byte[])tempDT.Rows[0][0];

                Weenie weenie;
                try
                {
                    using (BinaryReader br = new BinaryReader(new MemoryStream(charData)))
                    {
                        weenie = new Weenie(characterid).Unpack(br);
                    }
                }
                catch
                {
                    return null;
                }

                return weenie;
            }

            public static string GetPlayerName(uint characterid, string connectionString = null)
            {
                if (!isSetup)
                    return null;

                DataTable tempDT = new DataTable();
                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    conn.Open();

                    using (MySqlCommand command = new MySqlCommand(string.Format(Globals.GetPlayerNameById, characterid), conn))
                    {
                        tempDT.Load(command.ExecuteReader(CommandBehavior.SequentialAccess));
                    }
                }

                if (tempDT.Rows.Count == 0)
                    return null;

                return tempDT.Rows[0][0].ToString().Trim();
            }

            public static string GetAccountNameNyCharName(string charName, string connectionString = null)
            {
                if (!isSetup)
                    return null;

                DataTable tempDT = new DataTable();
                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    conn.Open();

                    using (MySqlCommand command = new MySqlCommand(string.Format(Globals.GetAccountNameByCharName, charName), conn))
                    {
                        tempDT.Load(command.ExecuteReader(CommandBehavior.SequentialAccess));
                    }
                }

                if (tempDT.Rows.Count == 0)
                    return null;

                return tempDT.Rows[0][0].ToString().Trim();
            }

            public static string GetAccountIPByCharName(string charName, string connectionString = null)
            {
                if (!isSetup)
                    return null;

                DataTable tempDT = new DataTable();
                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    conn.Open();

                    using (MySqlCommand command = new MySqlCommand(string.Format(Globals.GetAccountIPByCharName, charName), conn))
                    {
                        tempDT.Load(command.ExecuteReader(CommandBehavior.SequentialAccess));
                    }
                }

                if (tempDT.Rows.Count == 0)
                    return null;

                return tempDT.Rows[0][0].ToString().Trim();
            }



            public static string GetAccountNameById(uint accountId, string connectionString = null)
            {
                if (!isSetup)
                    return null;

                DataTable tempDT = new DataTable();
                using (MySqlConnection conn = new MySqlConnection(connectionString ?? connString))
                {
                    conn.Open();

                    using (MySqlCommand command = new MySqlCommand(string.Format(Globals.GetAccountNameById, accountId), conn))
                    {
                        tempDT.Load(command.ExecuteReader(CommandBehavior.SequentialAccess));
                    }
                }

                if (tempDT.Rows.Count == 0)
                    return null;

                return tempDT.Rows[0][0].ToString().Trim();
            }


            private static void CreateConnectionString()
            {
                connString = new MySqlConnectionStringBuilder()
                {
                    Server = dbserver,
                    Port = Convert.ToUInt32(dbport),
                    UserID = dbuser,
                    Password = dbpass,
                    Database = dbname,
                    IgnorePrepare = false,
                    Pooling = true,
                    AllowUserVariables = true,
                    AllowZeroDateTime = true
                }.ToString();
            }
        }
    }
}
