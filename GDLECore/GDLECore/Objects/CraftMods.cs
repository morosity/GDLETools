﻿using GDLECore;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public class CraftMods : IPackable<CraftMods>
    {
        public List<IntCraftMod> IntMods = new List<IntCraftMod>();
        public List<DidCraftMod> DIDMods = new List<DidCraftMod>();
        public List<IidCraftMod> IIDMods = new List<IidCraftMod>();
        public List<FloatCraftMod> FloatMods = new List<FloatCraftMod>();
        public List<StringCraftMod> StringMods = new List<StringCraftMod>();
        public List<BoolCraftMod> BoolMods = new List<BoolCraftMod>();

        public int ModifyHealth;
        public int ModifyStamina;
        public int ModifyMana;
        public int RequiresHealth;
        public int RequiresStamina;
        public int RequiresMana;

        public bool UnknownBool7;
        public uint ModificationScriptID;

        public int UnknownInt9;
        public uint UnknownUint10;

        public void Pack(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public CraftMods Unpack(BinaryReader reader)
        {
            int intRqCount = reader.ReadUInt16();
            int dummyValue = reader.ReadUInt16();

            for (int a = 0; a < intRqCount; a++)
            {
                IntMods.Add(new IntCraftMod().Unpack(reader));
            }

            int didRqCount = reader.ReadUInt16();
            dummyValue = reader.ReadUInt16();

            for (int a = 0; a < didRqCount; a++)
            {
                DIDMods.Add(new DidCraftMod().Unpack(reader));
            }


            int iidRqCount = reader.ReadUInt16();
            dummyValue = reader.ReadUInt16();

            for (int a = 0; a < iidRqCount; a++)
            {
                IIDMods.Add(new IidCraftMod().Unpack(reader));
            }


            int floatRqCount = reader.ReadUInt16();
            dummyValue = reader.ReadUInt16();

            for (int a = 0; a < floatRqCount; a++)
            {
                FloatMods.Add(new FloatCraftMod().Unpack(reader));
            }


            int stringRqCount = reader.ReadUInt16();
            dummyValue = reader.ReadUInt16();

            for (int a = 0; a < stringRqCount; a++)
            {
                StringMods.Add(new StringCraftMod().Unpack(reader));
            }


            int boolRqCount = reader.ReadUInt16();
            dummyValue = reader.ReadUInt16();

            for (int a = 0; a < boolRqCount; a++)
            {
                BoolMods.Add(new BoolCraftMod().Unpack(reader));
            }

            ModifyHealth = reader.ReadInt32();
            ModifyStamina = reader.ReadInt32();
            ModifyMana = reader.ReadInt32();
            RequiresHealth = reader.ReadInt32();
            RequiresStamina = reader.ReadInt32();
            RequiresMana = reader.ReadInt32();

            UnknownBool7 = Convert.ToBoolean(reader.ReadInt32());
            ModificationScriptID = reader.ReadUInt32();

            UnknownInt9 = reader.ReadInt32();
            UnknownUint10 = reader.ReadUInt32();



            return this;
        }
    }
}
