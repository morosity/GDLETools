﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public class HouseData : IPackable<HouseData>
    {
        public UInt32 SlumLordId = 0;
        public UInt32 OwnerId = 0;
        public UInt32 OwnerAccountId = 0;
        public UInt32 PurchaseTimeStamp = 0;
        public UInt32 CurrentMaintPeriodTimeStamp = 0;
        public UInt32 HouseType = 0;
        public Position Location;
        public List<HousePayment> BuyItems;
        public List<HousePayment> RentItems;
        public List<UInt32> HouseAccessList;
        public List<UInt32> StorageAccessList;
        public bool AllegianceAccess = false;
        public bool AllegianceStorageAccess = false;
        public bool EveryoneAccess = false;
        public bool EveryoneStorageAccess = false;
        public bool HooksVisible = false;

        public HouseData()
        {
            Location = new Position();
            BuyItems = new List<HousePayment>();
            RentItems = new List<HousePayment>();
            HouseAccessList = new List<uint>();
            StorageAccessList = new List<uint>();
        }

        public void Pack(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public HouseData Unpack(BinaryReader reader)
        {
            SlumLordId = reader.ReadUInt32();
            OwnerId = reader.ReadUInt32();
            OwnerAccountId = reader.ReadUInt32();
            PurchaseTimeStamp = reader.ReadUInt32();
            CurrentMaintPeriodTimeStamp = reader.ReadUInt32();
            HouseType = reader.ReadUInt32();
            Location.Unpack(reader);

            Int16 houseBuyItems = reader.ReadInt16();
            Int16 dummyhbi = reader.ReadInt16();
            for (int x = 0; x < houseBuyItems; x++)
            {
                HousePayment hp = new HousePayment();
                hp.Unpack(reader);
                BuyItems.Add(hp);
            }

            Int16 houseRentItems = reader.ReadInt16();
            Int16 dummyhri = reader.ReadInt16();
            for (int x = 0; x < houseRentItems; x++)
            {
                HousePayment hp = new HousePayment();
                hp.Unpack(reader);
                RentItems.Add(hp);
            }

            Int16 houseAccessCount = reader.ReadInt16();
            Int16 dummyV1 = reader.ReadInt16();
            for (int x = 0; x < houseAccessCount; x++)
            {
                HouseAccessList.Add(reader.ReadUInt32());
            }

            Int16 houseStorageCount = reader.ReadInt16();
            Int16 dummyV2 = reader.ReadInt16();
            for (int x = 0; x < houseStorageCount; x++)
            {
                StorageAccessList.Add(reader.ReadUInt32());
            }

            AllegianceAccess = reader.ReadBoolean();
            AllegianceStorageAccess = reader.ReadBoolean();
            EveryoneAccess = reader.ReadBoolean();
            EveryoneStorageAccess = reader.ReadBoolean();
            HooksVisible = reader.ReadBoolean();

            return this as HouseData;
        }
    }
}
