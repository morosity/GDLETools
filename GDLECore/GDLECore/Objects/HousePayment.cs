﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public class HousePayment : IPackable<HousePayment>
    {
        public UInt32 WeenieClassID = 0;
        public int Number = 0;
        public int Paid = 0;
        public string Name;
        public string PluralName;

        public void Pack(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public HousePayment Unpack(BinaryReader reader)
        {
            Number = reader.ReadInt32();
            Paid = reader.ReadInt32();
            WeenieClassID = reader.ReadUInt32();
            Name = reader.ReadGDLEString();
            PluralName = reader.ReadGDLEString();

            return this as HousePayment;
        }
    }
}
