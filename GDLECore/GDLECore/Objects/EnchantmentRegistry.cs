﻿using System.Collections.Generic;
using System.IO;


namespace GDLECore
{
    public class EnchantmentRegistry : IPackable<EnchantmentRegistry>
    {
        public List<Enchantment> MultiEnchantments { get; private set; }
        public List<Enchantment> Additions { get; private set; }
        public List<Enchantment> Cooldowns { get; private set; }
        public Enchantment Vitae { get; private set; }
        public uint HelpfulEnchantmentCount { get; private set; }
        public uint HarmfulEnchantmentCount { get; private set; }

        public EnchantmentRegistry()
        {
            MultiEnchantments = new List<Enchantment>();
            Additions = new List<Enchantment>();
            Cooldowns = new List<Enchantment>();
        }

        public EnchantmentRegistry Unpack(BinaryReader reader)
        {
            uint enchFlags = reader.ReadUInt32();

            if ((enchFlags & 1) != 0)
            {
                uint numMultiEnchants = reader.ReadUInt32();
                for (int me = 0; me < numMultiEnchants; me++)
                {
                    MultiEnchantments.Add(new Enchantment().Unpack(reader));
                }
            }

            if ((enchFlags & 2) != 0)
            {
                uint numAddEnchants = reader.ReadUInt32();

                for (int ae = 0; ae < numAddEnchants; ae++)
                {
                    Additions.Add(new Enchantment().Unpack(reader));
                }
            }

            if ((enchFlags & 8) != 0)
            {
                uint numCooldowns = reader.ReadUInt32();
                for (int c = 0; c < numCooldowns; c++)
                {
                    Cooldowns.Add(new Enchantment().Unpack(reader));
                }
            }

            if ((enchFlags & 4) != 0)
            {
                Vitae = new Enchantment().Unpack(reader);
            }

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            uint enchFlags = 0;

            if (MultiEnchantments.Count > 0)
                enchFlags |= 1;
            if (Additions.Count > 0)
                enchFlags |= 2;
            if (Cooldowns.Count > 0)
                enchFlags |= 8;
            if (Vitae != null)
                enchFlags |= 4;

            writer.Write(enchFlags);

            if (MultiEnchantments.Count > 0)
            {
                writer.Write(MultiEnchantments.Count);
                foreach (Enchantment en in MultiEnchantments)
                {
                    en.Pack(writer);
                }
            }

            if (Additions.Count > 0)
            {
                writer.Write(Additions.Count);
                foreach (Enchantment en in Additions)
                {
                    en.Pack(writer);
                }
            }

            if (Cooldowns.Count > 0)
            {
                writer.Write(Cooldowns.Count);
                foreach (Enchantment en in Cooldowns)
                {
                    en.Pack(writer);
                }
            }

            if (Vitae != null)
                Vitae.Pack(writer);

        }
    }
}
