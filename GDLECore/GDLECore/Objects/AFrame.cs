﻿using System.IO;



namespace GDLECore
{
    public class AFrame : IPackable<AFrame>
    {
        public Vector Origin { get; private set; }
        public Quaternion Quaternion { get; private set; }

        public AFrame()
        { }

        public AFrame Unpack(BinaryReader reader)
        {
            Origin = new Vector().Unpack(reader);
            Quaternion = new Quaternion().Unpack(reader);
            return this as AFrame;
        }

        public void Pack(BinaryWriter writer)
        {
            Origin.Pack(writer);
            Quaternion.Pack(writer);
        }

        public override string ToString()
        {
            return Origin.X.ToString("N5") + " " + Origin.Y.ToString("N5") + " " + Origin.Z.ToString("N5") + " " +
                Quaternion.W.ToString("N5") + " " + Quaternion.X.ToString("N5") + " " + Quaternion.Y.ToString("N5") + " " + 
                Quaternion.Z.ToString("N5");
        }
    }
}
