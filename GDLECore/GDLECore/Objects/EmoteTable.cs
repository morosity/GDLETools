﻿using System.Collections.Generic;
using System.IO;


namespace GDLECore
{
    public class EmoteTable : IPackable<EmoteTable>
    {
        public Dictionary<uint, List<EmoteSet>> EmoteSets { get; private set; }


        public EmoteTable()
        {
            EmoteSets = new Dictionary<uint, List<EmoteSet>>();
        }

        public EmoteTable Unpack(BinaryReader reader)
        {
            ushort numEmotes = reader.ReadUInt16();
            ushort siteEmotes = reader.ReadUInt16();

            for (int ne = 0; ne < numEmotes; ne++)
            {
                uint emoteKey = reader.ReadUInt32();
                uint numEmoteSets = reader.ReadUInt32();
                List<EmoteSet> tempSets = new List<EmoteSet>();
                for (int es = 0; es < numEmoteSets; es++)
                {
                    tempSets.Add((new EmoteSet().Unpack(reader)));
                }
                EmoteSets.Add(emoteKey, tempSets);
            }

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write((ushort)1);
            writer.Write((ushort)16);

            //writer.Write(EmoteSets.First().Value.Count);

            foreach (KeyValuePair<uint, List<EmoteSet>> emts in EmoteSets)
            {
                writer.Write(emts.Key);
                writer.Write(emts.Value.Count);
                foreach (EmoteSet es in emts.Value)
                {
                    es.Pack(writer);
                }
            }
        }
    }
}
