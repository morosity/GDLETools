﻿using GDLECore;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public class CraftOperation : IPackable<CraftOperation>
    {
        public uint UnknownUint;
        public SkillList Skill = SkillList.UNDEF_SKILL;
        public uint Difficulty;
        public uint SkillCheckFormula;
        public uint SuccessWCID;
        public uint SuccessAmount;
        public string SuccessMessage;
        public uint FailureWCID;
        public uint FailureAmount;
        public string FailureMessage;

        public double SuccessConsumeTargetChance;
        public uint SuccessConsumeTargetAmount;
        public string SuccessConsumeTargetMessage;

        public double SuccessConsumeToolChance;
        public uint SuccessConsumeToolAmount;
        public string SuccessConsumeToolMessage;

        public double FailureConsumeTargetChance;
        public uint FailureConsumeTargetAmount;
        public string FailureConsumeTargetMessage;

        public double FailureConsumeToolChance;
        public uint FailureConsumeToolAmount;
        public string FailureConsumeToolMessage;

        public CraftRequirements[] CraftRequirements = new CraftRequirements[3];
        public CraftMods[] CraftMods = new CraftMods[8];

        public uint DataID;

        public void Pack(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public CraftOperation Unpack(BinaryReader reader)
        {
            UnknownUint = reader.ReadUInt32();
            Skill = (SkillList)reader.ReadUInt32();
            Difficulty = reader.ReadUInt32();
            SkillCheckFormula = reader.ReadUInt32(); // Make to enum eventually
            SuccessWCID = reader.ReadUInt32();
            SuccessAmount = reader.ReadUInt32();
            SuccessMessage = reader.ReadGDLEString();
            FailureWCID = reader.ReadUInt32();
            FailureAmount = reader.ReadUInt32();
            FailureMessage = reader.ReadGDLEString();
            SuccessConsumeTargetChance = reader.ReadDouble();
            SuccessConsumeTargetAmount = reader.ReadUInt32();
            SuccessConsumeTargetMessage = reader.ReadGDLEString();
            SuccessConsumeToolChance = reader.ReadDouble();
            SuccessConsumeToolAmount = reader.ReadUInt32();
            SuccessConsumeToolMessage = reader.ReadGDLEString();
            FailureConsumeTargetChance = reader.ReadDouble();
            FailureConsumeTargetAmount = reader.ReadUInt32();
            FailureConsumeTargetMessage = reader.ReadGDLEString();
            FailureConsumeToolChance = reader.ReadDouble();
            FailureConsumeToolAmount = reader.ReadUInt32();
            FailureConsumeToolMessage = reader.ReadGDLEString();

            for (int r = 0; r < CraftRequirements.Count(); r++)
            {
                CraftRequirements[r] = new CraftRequirements().Unpack(reader);
            }

            for (int m = 0; m < CraftMods.Count(); m++)
            {
                CraftMods[m] = new CraftMods().Unpack(reader);
            }

            DataID = reader.ReadUInt32();

            return this;
        }
    }
}
