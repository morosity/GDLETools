﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public class ObjDesc : IPackable<ObjDesc>
    {
        private uint defaulMask = 0x04000000;

        public uint PaletteID { get; private set; }
        public List<SubPalette> SubPalettes { get; private set; }
        public int SubPaletteCount { get; private set; }
        public List<TextureMapChange> TextureMapChanges { get; private set; }
        public int TextureMapChangeCount { get; private set; }
        public List<AnimPartChange> AnimPartChanges { get; private set; }
        public int AnimationPartChangCount { get; private set; }

        public ObjDesc()
        {
            SubPalettes = new List<SubPalette>();
            TextureMapChanges = new List<TextureMapChange>();
            AnimPartChanges = new List<AnimPartChange>();
        }

        public ObjDesc Unpack(BinaryReader reader)
        {
            try
            {
                if (reader.ReadByte() != 0x11)
                    return null;

                SubPaletteCount = reader.ReadByte();
                TextureMapChangeCount = reader.ReadByte();
                AnimationPartChangCount = reader.ReadByte();

                if (SubPaletteCount > 0)
                {
                    PaletteID = reader.UnpackFromUnknown(defaulMask);

                    for (int sp = 0; sp < SubPaletteCount; sp++)
                    {
                        SubPalette subPalette = new SubPalette().Unpack(reader);
                        if (subPalette == null)
                            return null;
                        SubPalettes.Add(subPalette);
                    }
                }

                for (int tm = 0; tm < TextureMapChangeCount; tm++)
                {
                    TextureMapChange tmc = new TextureMapChange().Unpack(reader);
                    if (tmc == null)
                        return null;
                    TextureMapChanges.Add(tmc);
                }

                for (int am = 0; am < AnimationPartChangCount; am++)
                {
                    AnimPartChange apc = new AnimPartChange().Unpack(reader);
                    if (apc == null)
                        return null;
                    AnimPartChanges.Add(apc);
                }

                reader.Align();
            }
            catch (EndOfStreamException eos)
            {
                // add logging later
            }

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write((byte)0x11);
            writer.Write((byte)SubPaletteCount);
            writer.Write((byte)TextureMapChangeCount);
            writer.Write((byte)AnimationPartChangCount);

            if (SubPaletteCount > 0)
            {
                writer.PackToUnknown(defaulMask, PaletteID);

                foreach (SubPalette sp in SubPalettes)
                    sp.Pack(writer);
            }

            if (TextureMapChangeCount > 0)
            {
                foreach (TextureMapChange tmc in TextureMapChanges)
                    tmc.Pack(writer);
            }

            if (AnimationPartChangCount > 0)
            {
                foreach (AnimPartChange apc in AnimPartChanges)
                    apc.Pack(writer);
            }

            int alignBytesTotal = (2 * SubPaletteCount) + (3 * TextureMapChangeCount) + (2 * AnimationPartChangCount);
            int alignBytesMod = alignBytesTotal % 4;
            int alignBytesNeeded = 0;
            if (alignBytesMod == 1)
                alignBytesNeeded = 3;
            if (alignBytesMod == 2)
                alignBytesNeeded = 2;
            if (alignBytesMod == 3)
                alignBytesNeeded = 1;

            for (int a = 0; a < alignBytesNeeded; a++)
            {
                writer.Write((byte)0x0);
            }
        }
    }
}
