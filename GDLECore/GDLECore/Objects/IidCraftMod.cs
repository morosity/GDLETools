﻿using GDLECore;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public class IidCraftMod : IPackable<IidCraftMod>
    {
        public int UnknownInt;
        public int Operation; // TODO: Make enum
        public PropertyIID Property;
        public int Value;

        public void Pack(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public IidCraftMod Unpack(BinaryReader reader)
        {
            UnknownInt = reader.ReadInt32();
            Operation = reader.ReadInt32();
            Property = (PropertyIID)reader.ReadInt32();
            Value = reader.ReadInt32();
            return this;
        }
    }
}
