﻿
using GDLECore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public class PlayerModule : IPackable<PlayerModule>
    {
        public ShortCutManager ShortCuts { get; private set; }
        public Dictionary<uint, List<uint>> FavoriteSpells { get; private set; }
        public Dictionary<uint, uint> DesiredSpellComponents { get; private set; }

        [DefaultValue(0x50C41542)] // 0x50C4A54A will set Ingore fellowships to true
        public uint Options { get; private set; }

        [DefaultValue(0x948710)]
        public uint Options2 { get; private set; }

        [DefaultValue(0x3FFF)]
        public uint SpellFilters { get; private set; }

        public GenericQualitiesData GenericQualitiesData { get; private set; }

        [DefaultValue("%#H:%M:%S ")]
        public string TimeStampFormat { get; private set; }

        public PlayerModule()
        {
            FavoriteSpells = new Dictionary<uint, List<uint>>();
            DesiredSpellComponents = new Dictionary<uint, uint>();
        }

        public PlayerModule Unpack(BinaryReader reader)
        {
            uint playerFlags = reader.ReadUInt32();
            Options = reader.ReadUInt32();

            if ((playerFlags & 1) != 0)
            {
                ShortCuts = new ShortCutManager().Unpack(reader);
            }

            // First shortcut always saved even if no spells
            ushort numFirstBar = reader.ReadUInt16();
            ushort sizeFirstBar = reader.ReadUInt16();

            List<uint> firstBar = new List<uint>();
            for (int b1 = 0; b1 < numFirstBar; b1++)
            {
                firstBar.Add(reader.ReadUInt32());
            }
            FavoriteSpells.Add(0, firstBar);


            if ((playerFlags & 4) != 0)
            {
                for (int b = 1; b < 5; b++)
                {
                    ushort numOnBar = reader.ReadUInt16();
                    ushort sizeOnBar = reader.ReadUInt16();

                    List<uint> Bar = new List<uint>();
                    for (int b1 = 0; b1 < numOnBar; b1++)
                    {
                        firstBar.Add(reader.ReadUInt32());
                    }
                    if (Bar.Count > 0)
                        FavoriteSpells.Add(Convert.ToUInt32(b), Bar);
                }
            }
            else if ((playerFlags & 0x10) != 0)
            {
                for (int b = 1; b < 7; b++)
                {
                    ushort numOnBar = reader.ReadUInt16();
                    ushort sizeOnBar = reader.ReadUInt16();

                    List<uint> Bar = new List<uint>();
                    for (int b1 = 0; b1 < numOnBar; b1++)
                    {
                        firstBar.Add(reader.ReadUInt32());
                    }
                    if (Bar.Count > 0)
                        FavoriteSpells.Add(Convert.ToUInt32(b), Bar);
                }
            }
            else if ((playerFlags & 0x400) != 0)
            {
                for (int b = 1; b < 8; b++)
                {
                    ushort numOnBar = reader.ReadUInt16();
                    ushort sizeOnBar = reader.ReadUInt16();

                    List<uint> Bar = new List<uint>();
                    for (int b1 = 0; b1 < numOnBar; b1++)
                    {
                        firstBar.Add(reader.ReadUInt32());
                    }
                    FavoriteSpells.Add(Convert.ToUInt32(b), Bar);
                }
            }

            if ((playerFlags & 8) != 0)
            {
                DesiredSpellComponents = new Dictionary<uint, uint>();
                ushort numComps = reader.ReadUInt16();
                ushort sizeComps = reader.ReadUInt16();

                for (int c = 0; c < numComps; c++)
                {
                    DesiredSpellComponents.Add(reader.ReadUInt32(), reader.ReadUInt32());
                }
            }

            if ((playerFlags & 0x20) != 0)
            {
                SpellFilters = reader.ReadUInt32();
            }

            if ((playerFlags & 0x40) != 0)
            {
                Options2 = reader.ReadUInt32();
            }

            if ((playerFlags & 0x80) != 0)
            {
                TimeStampFormat = reader.ReadString();
            }

            if ((playerFlags & 0x100) != 0)
            {
                GenericQualitiesData = new GenericQualitiesData().Unpack(reader);
            }

            if ((playerFlags & 0x200) != 0)
            {
                uint windowLen = reader.ReadUInt32();
                byte[] wdata = new byte[windowLen];
                wdata = reader.ReadBytes((int)windowLen);
            }

            // Is an align needed here?

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            uint pmFlags = 0;

            pmFlags |= 0x400;

            if (ShortCuts != null)
            {
                pmFlags |= 1;
            }

            if (DesiredSpellComponents.Count > 0)
            {
                pmFlags |= 8;
            }

            pmFlags |= 0x60; // Spell filter and Options2

            if (GenericQualitiesData != null)
            {
                pmFlags |= 0x100;
            }

            writer.Write(pmFlags);
            writer.Write(Options);

            if (ShortCuts != null)
                ShortCuts.Pack(writer);

            // favorites bar
            for (int fb = 0; fb < 8; fb++)
            {
                var cb = FavoriteSpells.ElementAt(fb);
                writer.Write(cb.Value.Count);
                foreach (uint sid in cb.Value)
                    writer.Write(sid);
            }

            if (DesiredSpellComponents.Count > 0)
            {
                writer.Write((ushort)DesiredSpellComponents.Count);
                writer.Write((ushort)16);

                foreach (KeyValuePair<uint, uint> sc in DesiredSpellComponents)
                {
                    writer.Write(sc.Key);
                    writer.Write(sc.Value);
                }
            }

            writer.Write(SpellFilters);
            writer.Write(Options2);

            if (GenericQualitiesData != null)
                GenericQualitiesData.Pack(writer);



        }

    }
}
