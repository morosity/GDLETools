﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public class CraftRequirements : IPackable<CraftRequirements>
    {
        public List<IntCraftRequirement> IntRequirements = new List<IntCraftRequirement>();
        public List<DidCraftRequirement> DIDRequirements = new List<DidCraftRequirement>();
        public List<IidCraftRequirement> IIDRequirements = new List<IidCraftRequirement>();
        public List<FloatCraftRequirement> FloatRequirements = new List<FloatCraftRequirement>();
        public List<StringCraftRequirement> StringRequirements = new List<StringCraftRequirement>();
        public List<BoolCraftRequirement> BoolRequirements = new List<BoolCraftRequirement>();

        public void Pack(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public CraftRequirements Unpack(BinaryReader reader)
        {
            int intRqCount = reader.ReadUInt16();
            int dummyValue = reader.ReadUInt16();

            for (int a = 0; a < intRqCount; a++)
            {
                IntRequirements.Add(new IntCraftRequirement().Unpack(reader));
            }

            int didRqCount = reader.ReadUInt16();
            dummyValue = reader.ReadUInt16();

            for (int a = 0; a < didRqCount; a++)
            {
                DIDRequirements.Add(new DidCraftRequirement().Unpack(reader));
            }


            int iidRqCount = reader.ReadUInt16();
            dummyValue = reader.ReadUInt16();

            for (int a = 0; a < iidRqCount; a++)
            {
                IIDRequirements.Add(new IidCraftRequirement().Unpack(reader));
            }


            int floatRqCount = reader.ReadUInt16();
            dummyValue = reader.ReadUInt16();

            for (int a = 0; a < floatRqCount; a++)
            {
                FloatRequirements.Add(new FloatCraftRequirement().Unpack(reader));
            }


            int stringRqCount = reader.ReadUInt16();
            dummyValue = reader.ReadUInt16();

            for (int a = 0; a < stringRqCount; a++)
            {
                StringRequirements.Add(new StringCraftRequirement().Unpack(reader));
            }


            int boolRqCount = reader.ReadUInt16();
            dummyValue = reader.ReadUInt16();

            for (int a = 0; a < boolRqCount; a++)
            {
                BoolRequirements.Add(new BoolCraftRequirement().Unpack(reader));
            }


            return this;
        }
    }
}
