﻿using GDLECore;
using System;
using System.IO;


namespace GDLECore
{
    public class Emote : IPackable<Emote>
    {
        public EmoteType EmoteType { get; private set; }
        public float Delay { get; private set; }
        public float Extent { get; private set; }
        public uint Amount { get; private set; }
        public UInt64 Amount64 { get; private set; }
        public UInt64 HeroXp64 { get; private set; }
        public ulong Min64 { get; private set; }
        public ulong Max64 { get; private set; }
        public uint Min { get; private set; }
        public uint Max { get; private set; }
        public double FMin { get; private set; }
        public double FMax { get; private set; }
        public uint Stat { get; private set; }
        public uint Motion { get; private set; }

        public PScriptType PScript { get; private set; }
        public SoundType Sound { get; private set; }

        public CreationProfile CreationProfile { get; private set; }

        public Frame Frame { get; private set; }
        public uint SpellID { get; private set; }
        public string TestString { get; private set; }
        public string Message { get; private set; }
        public double Percent { get; private set; }
        public int Display { get; private set; }
        public uint WealthRating { get; private set; }
        public uint TreasureClass { get; private set; }
        public int TreasureType { get; private set; }
        public Position Position { get; private set; }

        public Emote()
        { }

        public Emote Unpack(BinaryReader reader)
        {
            EmoteType = (EmoteType)reader.ReadUInt32();
            Delay = reader.ReadSingle();
            Extent = reader.ReadSingle();

            switch (EmoteType)
            {
	            case EmoteType.Act_EmoteType:
	            case EmoteType.Say_EmoteType:
	            case EmoteType.Tell_EmoteType:
                case EmoteType.TextDirect_EmoteType:
                case EmoteType.WorldBroadcast_EmoteType:
                case EmoteType.LocalBroadcast_EmoteType:
                case EmoteType.DirectBroadcast_EmoteType:
                case EmoteType.UpdateQuest_EmoteType:
                case EmoteType.InqQuest_EmoteType:
                case EmoteType.StampQuest_EmoteType:
                case EmoteType.StartEvent_EmoteType:
                case EmoteType.StopEvent_EmoteType:
                case EmoteType.BLog_EmoteType:
                case EmoteType.AdminSpam_EmoteType:
                case EmoteType.EraseQuest_EmoteType:
                case EmoteType.InqEvent_EmoteType:
                case EmoteType.InqFellowQuest_EmoteType:
                case EmoteType.UpdateFellowQuest_EmoteType:
                case EmoteType.StampFellowQuest_EmoteType:
                case EmoteType.TellFellow_EmoteType:
                case EmoteType.FellowBroadcast_EmoteType:
                case EmoteType.Goto_EmoteType:
                case EmoteType.PopUp_EmoteType:
                case EmoteType.UpdateMyQuest_EmoteType:
                case EmoteType.InqMyQuest_EmoteType:
                case EmoteType.StampMyQuest_EmoteType:
                case EmoteType.EraseMyQuest_EmoteType:
                case EmoteType.LocalSignal_EmoteType:
                case EmoteType.InqContractsFull_EmoteType:
		            Message = reader.ReadGDLEString();
                            break;
                case EmoteType.DecrementQuest_EmoteType:
	            case EmoteType.IncrementQuest_EmoteType:
	            case EmoteType.SetQuestCompletions_EmoteType:
	            case EmoteType.DecrementMyQuest_EmoteType:
	            case EmoteType.IncrementMyQuest_EmoteType:
	            case EmoteType.SetMyQuestCompletions_EmoteType:
	            case EmoteType.InqPackSpace_EmoteType:
	            case EmoteType.InqQuestBitsOn_EmoteType:
	            case EmoteType.InqQuestBitsOff_EmoteType:
	            case EmoteType.InqMyQuestBitsOn_EmoteType:
	            case EmoteType.InqMyQuestBitsOff_EmoteType:
	            case EmoteType.SetQuestBitsOn_EmoteType:
	            case EmoteType.SetQuestBitsOff_EmoteType:
	            case EmoteType.SetMyQuestBitsOn_EmoteType:
	            case EmoteType.SetMyQuestBitsOff_EmoteType:
                    Message = reader.ReadGDLEString();
                    Amount = reader.ReadUInt32();
                    break;
                case EmoteType.SetIntStat_EmoteType:
	            case EmoteType.IncrementIntStat_EmoteType:
	            case EmoteType.DecrementIntStat_EmoteType:
	            case EmoteType.SetBoolStat_EmoteType:
		            Stat = reader.ReadUInt32();
                    Amount = reader.ReadUInt32();
                    break;
                case EmoteType.SetInt64Stat_EmoteType:
		            Stat = reader.ReadUInt32();
                    Amount64 = reader.ReadUInt64();
                    break;
                case EmoteType.SetFloatStat_EmoteType:
		            Stat = reader.ReadUInt32();
                    Percent = reader.ReadDouble();
                    break;
                case EmoteType.InqQuestSolves_EmoteType:
                case EmoteType.InqFellowNum_EmoteType:
	            case EmoteType.InqNumCharacterTitles_EmoteType:
	            case EmoteType.InqMyQuestSolves_EmoteType:
		            Message = reader.ReadGDLEString();
                    Min = reader.ReadUInt32();
                    Max = reader.ReadUInt32();
                    break;
                case EmoteType.AwardXP_EmoteType: // 2
                case EmoteType.AwardNoShareXP_EmoteType: // 62
                    Amount64 = reader.ReadUInt64();
                    HeroXp64 = reader.ReadUInt64(); 
                    break;
                case EmoteType.SpendLuminance_EmoteType:
	            case EmoteType.AwardLuminance_EmoteType:
		            Amount64 = reader.ReadUInt64();
                    HeroXp64 = reader.ReadUInt64();
                    break;
                case EmoteType.AddCharacterTitle_EmoteType:
                    Amount = reader.ReadUInt32(); // in ToD at some point, an integer AddCharacterTitle
                    break;
                case EmoteType.AwardTrainingCredits_EmoteType:
	            case EmoteType.InflictVitaePenalty_EmoteType:
	            case EmoteType.RemoveVitaePenalty_EmoteType:
	            case EmoteType.AddContract_EmoteType:
	            case EmoteType.RemoveContract_EmoteType:
		            Amount = reader.ReadUInt32();
                    break;
                case EmoteType.CastSpell_EmoteType:
	            case EmoteType.CastSpellInstant_EmoteType:
	            case EmoteType.TeachSpell_EmoteType:
	            case EmoteType.PetCastSpellOnOwner_EmoteType:
		            SpellID = reader.ReadUInt32();
                    break;
                case EmoteType.Give_EmoteType:
                case EmoteType.TakeItems_EmoteType:
                    CreationProfile = new CreationProfile().Unpack(reader);
                    break;

                case EmoteType.InqOwnsItems_EmoteType:
                    Message = reader.ReadString();
                    CreationProfile = new CreationProfile().Unpack(reader);
                    break;
                case EmoteType.CreateTreasure_EmoteType:
                    WealthRating = reader.ReadUInt32();
                    TreasureClass = reader.ReadUInt32();
                    TreasureType = reader.ReadInt32();
                    break;
                case EmoteType.Motion_EmoteType:
                case EmoteType.ForceMotion_EmoteType:
                    Motion = reader.ReadUInt32();
                    break;
                case EmoteType.MoveHome_EmoteType:
	            case EmoteType.Move_EmoteType:
	            case EmoteType.Turn_EmoteType:
	            case EmoteType.MoveToPos_EmoteType:
		            Frame = new Frame().Unpack(reader);
                    break;
                case EmoteType.PhysScript_EmoteType:
		            PScript = (PScriptType)reader.ReadUInt32();
                    break;
                case EmoteType.Sound_EmoteType:
		            Sound = (SoundType)reader.ReadUInt32();
                    break;
                case EmoteType.AwardSkillXP_EmoteType:
	            case EmoteType.AwardSkillPoints_EmoteType:
		            Amount = reader.ReadUInt32();
                    Stat = reader.ReadUInt32();
                    break;
                case EmoteType.UntrainSkill_EmoteType:
		            Stat = reader.ReadUInt32();
                    break;
                case EmoteType.SetAltRacialSkills_EmoteType:
		            Amount = reader.ReadUInt32();
                    break;
                case EmoteType.InqBoolStat_EmoteType:
	            case EmoteType.InqSkillTrained_EmoteType:
	            case EmoteType.InqSkillSpecialized_EmoteType:
		            Message = reader.ReadGDLEString();
                    Stat = reader.ReadUInt32();
                    break;
                case EmoteType.InqStringStat_EmoteType:
	            case EmoteType.InqYesNo_EmoteType:
                    Message = reader.ReadGDLEString();
                    TestString = reader.ReadGDLEString();
                    Stat = reader.ReadUInt32();
                    break;
                case EmoteType.InqIntStat_EmoteType:
	            case EmoteType.InqAttributeStat_EmoteType:
	            case EmoteType.InqRawAttributeStat_EmoteType:
	            case EmoteType.InqSecondaryAttributeStat_EmoteType:
	            case EmoteType.InqRawSecondaryAttributeStat_EmoteType:
	            case EmoteType.InqSkillStat_EmoteType:
	            case EmoteType.InqRawSkillStat_EmoteType:
		            Message = reader.ReadGDLEString();
                    Min = reader.ReadUInt32();
                    Max = reader.ReadUInt32();
                    Stat = reader.ReadUInt32();
                    break;
                case EmoteType.InqInt64Stat_EmoteType:
                    Message = reader.ReadGDLEString();
                    Min64 = reader.ReadUInt64();
                    Max64 = reader.ReadUInt64();
                    Stat = reader.ReadUInt32();
                    break;
                case EmoteType.InqFloatStat_EmoteType:
                    Message = reader.ReadGDLEString();
                    FMin = reader.ReadDouble();
                    FMax = reader.ReadDouble();
                    Stat = reader.ReadUInt32();
                    break;
                case EmoteType.AwardLevelProportionalXP_EmoteType:
                    Percent = reader.ReadDouble();
                    Min64 = reader.ReadUInt64();
                    Max64 = reader.ReadUInt64();
                    Display = reader.ReadInt32();
                    break;
                case EmoteType.AwardLevelProportionalSkillXP_EmoteType:
		            Stat = reader.ReadUInt32();
                    Percent = reader.ReadDouble();
                    Min = reader.ReadUInt32();
                    Max = reader.ReadUInt32();
                    Display = reader.ReadInt32();
                    break;
                case EmoteType.SetSanctuaryPosition_EmoteType:
		            Position = new Position().Unpack(reader);
                    break;
                case EmoteType.TeleportTarget_EmoteType:
	            case EmoteType.TeleportSelf_EmoteType:
                    Position = new Position().Unpack(reader);
                    break;
            }

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write((uint)EmoteType);
            writer.Write(Delay);
            writer.Write(Extent);

            switch (EmoteType)
            {
                case EmoteType.Act_EmoteType:
                case EmoteType.Say_EmoteType:
                case EmoteType.Tell_EmoteType:
                case EmoteType.TextDirect_EmoteType:
                case EmoteType.WorldBroadcast_EmoteType:
                case EmoteType.LocalBroadcast_EmoteType:
                case EmoteType.DirectBroadcast_EmoteType:
                case EmoteType.UpdateQuest_EmoteType:
                case EmoteType.InqQuest_EmoteType:
                case EmoteType.StampQuest_EmoteType:
                case EmoteType.StartEvent_EmoteType:
                case EmoteType.StopEvent_EmoteType:
                case EmoteType.BLog_EmoteType:
                case EmoteType.AdminSpam_EmoteType:
                case EmoteType.EraseQuest_EmoteType:
                case EmoteType.InqEvent_EmoteType:
                case EmoteType.InqFellowQuest_EmoteType:
                case EmoteType.UpdateFellowQuest_EmoteType:
                case EmoteType.StampFellowQuest_EmoteType:
                case EmoteType.TellFellow_EmoteType:
                case EmoteType.FellowBroadcast_EmoteType:
                case EmoteType.Goto_EmoteType:
                case EmoteType.PopUp_EmoteType:
                case EmoteType.UpdateMyQuest_EmoteType:
                case EmoteType.InqMyQuest_EmoteType:
                case EmoteType.StampMyQuest_EmoteType:
                case EmoteType.EraseMyQuest_EmoteType:
                case EmoteType.LocalSignal_EmoteType:
                case EmoteType.InqContractsFull_EmoteType:
                    ushort length = (ushort)Message.Length;
                    byte[] strAsBytes = Message.ToByteArray();
                    int alignBytes = Globals.ByteAlign(sizeof(ushort) + strAsBytes.Length);
                    writer.Write(length);
                    writer.Write(strAsBytes);
                    for (int ab = 0; ab < alignBytes; ab++)
                    {
                        writer.Write((byte)0);
                    }
                    break;
                case EmoteType.DecrementQuest_EmoteType:
                case EmoteType.IncrementQuest_EmoteType:
                case EmoteType.SetQuestCompletions_EmoteType:
                case EmoteType.DecrementMyQuest_EmoteType:
                case EmoteType.IncrementMyQuest_EmoteType:
                case EmoteType.SetMyQuestCompletions_EmoteType:
                case EmoteType.InqPackSpace_EmoteType:
                case EmoteType.InqQuestBitsOn_EmoteType:
                case EmoteType.InqQuestBitsOff_EmoteType:
                case EmoteType.InqMyQuestBitsOn_EmoteType:
                case EmoteType.InqMyQuestBitsOff_EmoteType:
                case EmoteType.SetQuestBitsOn_EmoteType:
                case EmoteType.SetQuestBitsOff_EmoteType:
                case EmoteType.SetMyQuestBitsOn_EmoteType:
                case EmoteType.SetMyQuestBitsOff_EmoteType:
                    length = (ushort)Message.Length;
                    strAsBytes = Message.ToByteArray();
                    alignBytes = Globals.ByteAlign(sizeof(ushort) + strAsBytes.Length);
                    writer.Write(length);
                    writer.Write(strAsBytes);
                    for (int ab = 0; ab < alignBytes; ab++)
                    {
                        writer.Write((byte)0);
                    }
                    writer.Write(Amount);
                    break;
                case EmoteType.SetIntStat_EmoteType:
                case EmoteType.IncrementIntStat_EmoteType:
                case EmoteType.DecrementIntStat_EmoteType:
                case EmoteType.SetBoolStat_EmoteType:
                    writer.Write(Stat);
                    writer.Write(Amount);
                    break;
                case EmoteType.SetInt64Stat_EmoteType:
                    writer.Write(Stat);
                    writer.Write(Amount64);
                    break;
                case EmoteType.SetFloatStat_EmoteType:
                    writer.Write(Stat);
                    writer.Write(Percent);
                    break;
                case EmoteType.InqQuestSolves_EmoteType:
                case EmoteType.InqFellowNum_EmoteType:
                case EmoteType.InqNumCharacterTitles_EmoteType:
                case EmoteType.InqMyQuestSolves_EmoteType:
                    length = (ushort)Message.Length;
                    strAsBytes = Message.ToByteArray();
                    alignBytes = Globals.ByteAlign(sizeof(ushort) + strAsBytes.Length);
                    writer.Write(length);
                    writer.Write(strAsBytes);
                    for (int ab = 0; ab < alignBytes; ab++)
                    {
                        writer.Write((byte)0);
                    }
                    writer.Write(Min);
                    writer.Write(Max);
                    break;
                case EmoteType.AwardXP_EmoteType: // 2
                case EmoteType.AwardNoShareXP_EmoteType: // 62
                    writer.Write(Amount64);
                    writer.Write(HeroXp64);
                    break;
                case EmoteType.SpendLuminance_EmoteType:
                case EmoteType.AwardLuminance_EmoteType:
                    writer.Write(Amount64);
                    writer.Write(HeroXp64);
                    break;
                case EmoteType.AddCharacterTitle_EmoteType:
                    writer.Write(Amount); // in ToD at some point, an integer AddCharacterTitle
                    break;
                case EmoteType.AwardTrainingCredits_EmoteType:
                case EmoteType.InflictVitaePenalty_EmoteType:
                case EmoteType.RemoveVitaePenalty_EmoteType:
                case EmoteType.AddContract_EmoteType:
                case EmoteType.RemoveContract_EmoteType:
                    writer.Write(Amount);
                    break;
                case EmoteType.CastSpell_EmoteType:
                case EmoteType.CastSpellInstant_EmoteType:
                case EmoteType.TeachSpell_EmoteType:
                case EmoteType.PetCastSpellOnOwner_EmoteType:
                    writer.Write(SpellID);
                    break;
                case EmoteType.Give_EmoteType:
                case EmoteType.TakeItems_EmoteType:
                    CreationProfile.Pack(writer);
                    break;

                case EmoteType.InqOwnsItems_EmoteType:
                    length = (ushort)Message.Length;
                    strAsBytes = Message.ToByteArray();
                     alignBytes = Globals.ByteAlign(sizeof(ushort) + strAsBytes.Length);
                    writer.Write(length);
                    writer.Write(strAsBytes);
                    for (int ab = 0; ab < alignBytes; ab++)
                    {
                        writer.Write((byte)0);
                    }
                    CreationProfile.Pack(writer);
                    break;
                case EmoteType.CreateTreasure_EmoteType:
                    writer.Write(WealthRating);
                    writer.Write(TreasureClass);
                    writer.Write(TreasureType);
                    break;
                case EmoteType.Motion_EmoteType:
                case EmoteType.ForceMotion_EmoteType:
                    writer.Write(Motion);
                    break;
                case EmoteType.MoveHome_EmoteType:
                case EmoteType.Move_EmoteType:
                case EmoteType.Turn_EmoteType:
                case EmoteType.MoveToPos_EmoteType:
                    Frame.Pack(writer);
                    break;
                case EmoteType.PhysScript_EmoteType:
                    writer.Write((uint)PScript);
                    break;
                case EmoteType.Sound_EmoteType:
                    writer.Write((uint)Sound);
                    break;
                case EmoteType.AwardSkillXP_EmoteType:
                case EmoteType.AwardSkillPoints_EmoteType:
                    writer.Write(Amount);
                    writer.Write(Stat);
                    break;
                case EmoteType.UntrainSkill_EmoteType:
                    writer.Write(Stat);
                    break;
                case EmoteType.SetAltRacialSkills_EmoteType:
                    writer.Write(Amount);
                    break;
                case EmoteType.InqBoolStat_EmoteType:
                case EmoteType.InqSkillTrained_EmoteType:
                case EmoteType.InqSkillSpecialized_EmoteType:
                    length = (ushort)Message.Length;
                    strAsBytes = Message.ToByteArray();
                    alignBytes = Globals.ByteAlign(sizeof(ushort) + strAsBytes.Length);
                    writer.Write(length);
                    writer.Write(strAsBytes);
                    for (int ab = 0; ab < alignBytes; ab++)
                    {
                        writer.Write((byte)0);
                    }
                    writer.Write(Stat);
                    break;
                case EmoteType.InqStringStat_EmoteType:
                case EmoteType.InqYesNo_EmoteType:
                    length = (ushort)Message.Length;
                    strAsBytes = Message.ToByteArray();
                    alignBytes = Globals.ByteAlign(sizeof(ushort) + strAsBytes.Length);
                    writer.Write(length);
                    writer.Write(strAsBytes);
                    for (int ab = 0; ab < alignBytes; ab++)
                    {
                        writer.Write((byte)0);
                    }
                    length = (ushort)TestString.Length;
                    strAsBytes = TestString.ToByteArray();
                    alignBytes = Globals.ByteAlign(sizeof(ushort) + strAsBytes.Length);
                    writer.Write(length);
                    writer.Write(strAsBytes);
                    for (int ab = 0; ab < alignBytes; ab++)
                    {
                        writer.Write((byte)0);
                    }
                    writer.Write(Stat);
                    break;
                case EmoteType.InqIntStat_EmoteType:
                case EmoteType.InqAttributeStat_EmoteType:
                case EmoteType.InqRawAttributeStat_EmoteType:
                case EmoteType.InqSecondaryAttributeStat_EmoteType:
                case EmoteType.InqRawSecondaryAttributeStat_EmoteType:
                case EmoteType.InqSkillStat_EmoteType:
                case EmoteType.InqRawSkillStat_EmoteType:
                    length = (ushort)Message.Length;
                    strAsBytes = Message.ToByteArray();
                    alignBytes = Globals.ByteAlign(sizeof(ushort) + strAsBytes.Length);
                    writer.Write(length);
                    writer.Write(strAsBytes);
                    for (int ab = 0; ab < alignBytes; ab++)
                    {
                        writer.Write((byte)0);
                    }
                    writer.Write(Min);
                    writer.Write(Max);
                    writer.Write(Stat);
                    break;
                case EmoteType.InqInt64Stat_EmoteType:
                    length = (ushort)Message.Length;
                    strAsBytes = Message.ToByteArray();
                    alignBytes = Globals.ByteAlign(sizeof(ushort) + strAsBytes.Length);
                    writer.Write(length);
                    writer.Write(strAsBytes);
                    for (int ab = 0; ab < alignBytes; ab++)
                    {
                        writer.Write((byte)0);
                    }
                    writer.Write(Min64);
                    writer.Write(Max64);
                    writer.Write(Stat);
                    break;
                case EmoteType.InqFloatStat_EmoteType:
                    length = (ushort)Message.Length;
                    strAsBytes = Message.ToByteArray();
                    alignBytes = Globals.ByteAlign(sizeof(ushort) + strAsBytes.Length);
                    writer.Write(length);
                    writer.Write(strAsBytes);
                    for (int ab = 0; ab < alignBytes; ab++)
                    {
                        writer.Write((byte)0);
                    }
                    writer.Write(FMin);
                    writer.Write(FMax);
                    writer.Write(Stat);
                    break;
                case EmoteType.AwardLevelProportionalXP_EmoteType:
                    writer.Write(Percent);
                    writer.Write(Min64);
                    writer.Write(Max64);
                    writer.Write(Display);
                    break;
                case EmoteType.AwardLevelProportionalSkillXP_EmoteType:
                    writer.Write(Stat);
                    writer.Write(Percent);
                    writer.Write(Min);
                    writer.Write(Max);
                    writer.Write(Display);
                    break;
                case EmoteType.SetSanctuaryPosition_EmoteType:
                    Position.Pack(writer);
                    break;
                case EmoteType.TeleportTarget_EmoteType:
                case EmoteType.TeleportSelf_EmoteType:
                    Position.Pack(writer);
                    break;
            }

        }
    }
}
