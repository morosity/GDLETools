﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public enum RegenerationType
    {
        Undef_RegenerationType = 0,
        Destruction_RegenerationType = 1,
        PickUp_RegenerationType = 2,
        Death_RegenerationType = 4
    };
}
