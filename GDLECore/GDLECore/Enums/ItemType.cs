﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public enum ItemType
    {
        TYPE_UNDEF = 0,
        TYPE_MELEE_WEAPON = (1 << 0), // 1
        TYPE_ARMOR = (1 << 1), // 2
        TYPE_CLOTHING = (1 << 2), // 4
        TYPE_JEWELRY = (1 << 3), // 8
        TYPE_CREATURE = (1 << 4), // 0x10
        TYPE_FOOD = (1 << 5), // 0x20
        TYPE_MONEY = (1 << 6), // x40
        TYPE_MISC = (1 << 7), // 0x80
        TYPE_MISSILE_WEAPON = (1 << 8), // 0x100
        TYPE_CONTAINER = (1 << 9), // 0x200
        TYPE_USELESS = (1 << 10), // 0x400
        TYPE_GEM = (1 << 11), // 0x800
        TYPE_SPELL_COMPONENTS = (1 << 12), // 0x1000
        TYPE_WRITABLE = (1 << 13),
        TYPE_KEY = (1 << 14),
        TYPE_CASTER = (1 << 15),
        TYPE_PORTAL = (1 << 16), // 0x10000
        TYPE_LOCKABLE = (1 << 17),
        TYPE_PROMISSORY_NOTE = (1 << 18),
        TYPE_MANASTONE = (1 << 19),
        TYPE_SERVICE = (1 << 20), // 0x100000
        TYPE_MAGIC_WIELDABLE = (1 << 21),
        TYPE_CRAFT_COOKING_BASE = (1 << 22),
        TYPE_CRAFT_ALCHEMY_BASE = (1 << 23),
        // NOTE: Skip 1
        TYPE_CRAFT_FLETCHING_BASE = (1 << 25), // 0x2000000
        TYPE_CRAFT_ALCHEMY_INTERMEDIATE = (1 << 26),
        TYPE_CRAFT_FLETCHING_INTERMEDIATE = (1 << 27),
        TYPE_LIFESTONE = (1 << 28), // 0x10000000
        TYPE_TINKERING_TOOL = (1 << 29),
        TYPE_TINKERING_MATERIAL = (1 << 30),
        TYPE_GAMEBOARD = (1 << 31),
        TYPE_PORTAL_MAGIC_TARGET = 268500992,
        TYPE_LOCKABLE_MAGIC_TARGET = 640,
        TYPE_VESTEMENTS = 6,
        TYPE_WEAPON = 257,
        TYPE_WEAPON_OR_CASTER = 33025,
        TYPE_ITEM = 3013615,
        TYPE_REDIRECTABLE_ITEM_ENCHANTMENT_TARGET = 33031,
        TYPE_ITEM_ENCHANTABLE_TARGET = 560015,
        TYPE_SELF = 0,
        TYPE_VENDOR_SHOPKEEP = 1208248231,
        TYPE_VENDOR_GROCER = 4481568
    };

}
