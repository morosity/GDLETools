﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public static class Globals
    {
        internal static readonly string GetTheWholeShebang = "SELECT id FROM weenies WHERE top_level_object_id = {0};";
        public static string GetCharacterList = "SELECT name FROM characters ORDER BY name;";
        public static string GetCharacterData = "SELECT id, data FROM weenies WHERE top_level_object_id = {0} and id = top_level_object_id;";
        public static string GetCharInventory = "SELECT id, data FROM weenies WHERE top_level_object_id = {0} and id <> top_level_object_id;";
        public static string GetCharInventoryIDs = "SELECT id FROM weenies WHERE top_level_object_id = {0} and id <> top_level_object_id;";
        public static string GetCharDataLength = "SELECT length(data) FROM weenies WHERE top_level_object_id = (SELECT weenie_id FROM characters WHERE name = '{0}') and id = top_level_object_id;";
        public static string SaveCharacterData = "UPDATE weenies SET data = @data WHERE id = {0}";
        public static string SaveInventoryData = "UPDATE weenies SET data = @data WHERE top_level_object_id = {0} AND id = {1}";
        public static string GetNonCharacterRows = "SELECT COUNT(*) FROM weenies WHERE id >= 2147483648;";
        public static string GetCharacterWeenies = "SELECT * FROM weenies WHERE id < 2147483648;";
        public static string DeleteCorpses = "DELETE FROM weenies WHERE id >= 2147483648 and id = top_level_object_id;";
        public static string GetIdAndNamesOfChars = "SELECT weenie_id, name FROM characters ORDER BY name;";
        public static string DeleteCharacterInventory = "DELETE FROM weenies WHERE top_level_object_id = {0};";
        public static string DeleteCharacterWeenie = "DELETE FROM weenies WHERE id = {0};";
        public static string DeleteCharacter = "DELETE FROM chracters WHERE weenie_id = {0};";
        public static string GetAccountForCharacter = "SELECT account_id FROM characters WHERE weenie_id = {0};";
        public static string UpdateAccountPassword = "UPDATE accounts SET password = 'a953d45cfd9680887dc6c6f1a00e5372bc93a32a358a1f6106f3db134b9a988b', password_salt = 'dc5079d09a1f4681' WHERE id = {0}; ";
        public static string GetNonPlayerTopLevelWeenies = "select top_level_object_id, count(*) from weenies where top_level_object_id > 2147483648 and id <> top_level_object_id group by top_level_object_id having count(*) > 1 order by top_level_object_id;";
        public static string GetCorpseData = "SELECT id FROM weenies WHERE ID = top_level_object_id and ID IN (SELECT weenie_id FROM blocks);";
        public static string GetWeenieData = "select data from weenies where id = {0};";
        public static string GetPlayerNameById = "select name from characters where weenie_id = {0};";
        public static string GetNonPlayerTopLevelWeenie = "select top_level_object_id from weenies where top_level_object_id > 1879048192 and id = top_level_object_id;";
        public static string GetHouses = "SELECT house_id from houses;";
        public static string GetHouseData = "SELECT data FROM houses WHERE house_id = {0};";
        public static string GetAccountNameByCharName = "SELECT username FROM accounts WHERE id = (SELECT account_id FROM characters WHERE name = '{0}');";
        public static string GetAccountNameById = "SELECT username FROM accounts WHERE id = {0};";
        public static string GetAccountIPByCharName = "SELECT created_ip_address FROM accounts WHERE id = (SELECT account_id FROM characters WHERE name = '{0}');";
        public static string GetLatestLoginByAccount = "select COALESCE(max(c.ts_login),0) from accounts a join characters c on c.account_id = a.id where a.id = {0};";
        public static string DeleteWeenieByRange = "DELETE FROM weenies WHERE id in ({0});";
        public static string GetNonCorpseContainers = "SELECT distinct id FROM weenies WHERE ID = top_level_object_id and top_level_object_id > 1879048191 AND ID NOT IN (SELECT weenie_id FROM blocks);";
        public static string CopyWeenieRow = "INSERT INTO {0} (id) VALUES ({1});";
        public static string GetCharsInRange = "SELECT weenie_id FROM characters WHERE weenie_id >={0} and weenie_id <={1};";


        public static int ByteAlign(int len)
        {
            int extra = len % 4;
            if (extra == 1)
                return 3;
            if (extra == 2)
                return 2;
            if (extra == 3)
                return 1;
            return extra;
        }
    }
}
